﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Prototype_JetPackFuelMeter : MonoBehaviour {

    public CharJetPack jetpack;
    public Text fuelText;
    Rigidbody rb;
    public CharController charController;
    public Text speedText;

	// Use this for initialization
	void Start () {
        if (charController)
        {
            rb = charController.gameObject.GetComponent<Rigidbody>();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (jetpack && fuelText)
        {
            fuelText.text = Mathf.Round(jetpack.GetCurrentFuelPercentage() * 100).ToString() + "%\nFuel"; 
        }
        if (rb && speedText)
        {
            Vector2 speed = new Vector2(rb.velocity.x, rb.velocity.z);
            speedText.text = (Mathf.Round(speed.magnitude * 36f) * 0.1f).ToString() + "Kph";// mps * 3.6 is kph
        }
    }
}
