﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharController))]
public class CharJetPack : MonoBehaviour {
    CharController charController;
    Rigidbody rb;

    public float force;
    public float fuelTime;
    [SerializeField]
    float curFuelTime;
    public float recoveryTime;
    public float inputThreshhold;
    float curInputThreshhold;
    public float maxVelLimit;

    public float recoveryStepAngle;

    bool waitToRepressJump = true;

	// Use this for initialization
	void Start () {
        charController = GetComponent<CharController>();
        rb = GetComponent<Rigidbody>();
        curFuelTime = fuelTime;
        charController.onJump += OnJump;
    }

    private void OnDestroy()
    {
        charController.onJump -= OnJump;
    }

    // Update is called once per frame
    void Update () {
		if(!charController.isGrounded)
        {
            if(waitToRepressJump && Input.GetButtonUp("Jump"))
            {
                waitToRepressJump = false;
            }

            if (waitToRepressJump == false && curFuelTime > 0 && Input.GetButton("Jump"))
            {
                curInputThreshhold += Time.deltaTime;
                if (curInputThreshhold >= inputThreshhold)
                {
                    curFuelTime -= Time.deltaTime;
                    if (rb.velocity.y < maxVelLimit)
                    {
                        rb.AddForce(transform.up * force * Time.deltaTime, ForceMode.Force);
                    }
                }
            }
            else {
                curInputThreshhold = 0;
            }
        }else
        {
            if (charController.GetCurrentStepAngle() < recoveryStepAngle)
            {
                if (curFuelTime < fuelTime)
                    curFuelTime += Time.deltaTime * fuelTime / recoveryTime;
                else
                {
                    curFuelTime = fuelTime;
                }
            }

            curInputThreshhold = 0;
        }


	}

    void OnJump()
    {
        waitToRepressJump = true;
    }

    public float GetCurrentFuelPercentage()
    {
        return curFuelTime / fuelTime;
    }
}
