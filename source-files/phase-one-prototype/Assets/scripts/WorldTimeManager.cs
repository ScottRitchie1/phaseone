﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class WorldTimeManager : MonoBehaviour {
    public static WorldTimeManager instance;

    public enum _time { seconds, minutes, hours, days, months, years};

    public double startTime;
    public float timeScale = 1;
    public static double currentTime;

    public float hoursInADay = 0.5f;
    public float daysInAMonth = 30;
    public float MonthsInAYear = 12;

    
    [Range(0, 1)]
    public static float minuteWeight;
    [Range(0, 1)]
    public static float hourWeight;
    [Range(0, 1)]
    public static float dayWeight;
    [Range(0, 1)]
    public static float monthWeight;
    [Range(0, 1)]
    public static float yearsWeight;

    private void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("WorldTimeManager instance already set."); }
    }

    private void Start()
    {
        ChangeTime(12, _time.hours);
    }

    // Update is called once per frame
    void Update () {
        currentTime += Time.deltaTime * timeScale * (Input.GetKey(KeyCode.Alpha3)? 600 : 1) * (Input.GetKey(KeyCode.Alpha0) ? 10 : 1);

        yearsWeight = GetWeight(_time.years);
        minuteWeight = GetWeight(_time.minutes);
        hourWeight = GetWeight(_time.hours);
        dayWeight = GetWeight(_time.days);
        monthWeight = GetWeight(_time.months);
    }

    public string TimeString(bool timeInTotals)
    {
        float seconds;
        double mintues;
        double hours;
        double days;
        double months;
        double years;

        if (timeInTotals)
        {
            seconds = (float)GetTotalTime(_time.seconds);
            mintues = GetTotalTime(_time.minutes);
            hours = GetTotalTime(_time.hours);
            days = GetTotalTime(_time.days);
            months = GetTotalTime(_time.months);
            years = GetTotalTime(_time.years);
        }
        else
        {
            seconds = GetTime(_time.seconds);
            mintues = GetTime(_time.minutes);
            hours = GetTime(_time.hours);
            days = GetTime(_time.days);
            months = GetTime(_time.months);
            years = GetTime(_time.years);
        }
        return ("YEARS: " + years.ToString() + "\nMONTHS: " + months.ToString() + "\nDAYS:" + days.ToString() + "\nHOURS:" + hours.ToString() + "\nMINUTES:" + mintues.ToString() + "\nSECONDS:" + seconds.ToString());
    }

    public float GetTime(_time t)
    {
        switch (t)
        {
            case _time.seconds:
                return (float)(currentTime) % 60;
            case _time.minutes:
                return Mathf.Floor(((float)currentTime / 60) % 60);
            case _time.hours:
                return Mathf.Floor(((float)currentTime / 3600) % hoursInADay);
            case _time.days:
                return Mathf.Floor(((float)currentTime / (3600 * hoursInADay)) % daysInAMonth);
            case _time.months:
                return Mathf.Floor(((float)currentTime / (3600 * hoursInADay * daysInAMonth)) % MonthsInAYear);
            case _time.years:
                return Mathf.Floor(((float)currentTime / (3600 * hoursInADay * daysInAMonth * MonthsInAYear)));
            default: return 0;
        }
    }

    public double GetTotalTime(_time t)
    {
        switch (t)
        {
            case _time.seconds:
                return currentTime;
            case _time.minutes:
                return currentTime / 60;
            case _time.hours:
                return currentTime / 3600;
            case _time.days:
                return currentTime / (3600 * hoursInADay);
            case _time.months:
                return currentTime / (3600 * hoursInADay * daysInAMonth);
            case _time.years:
                return currentTime / (3600 * hoursInADay * daysInAMonth * MonthsInAYear);
            default: return 0;
        }
    }

    public float GetWeight(_time t)
    {
        switch (t)
        {
            case _time.minutes:
                return Mathf.InverseLerp(0, 60, (float)(GetTotalTime(_time.seconds) % 60));
            case _time.hours:
                return Mathf.InverseLerp(0, 60, (float)(GetTotalTime(_time.minutes) % 60));
            case _time.days:
                return Mathf.InverseLerp(0, hoursInADay, (float)(GetTotalTime(_time.hours) % hoursInADay));
            case _time.months:
                return Mathf.InverseLerp(0, daysInAMonth, (float)(GetTotalTime(_time.days) % daysInAMonth));
            case _time.years:
                return Mathf.InverseLerp(0, MonthsInAYear, (float)(GetTotalTime(_time.months) % MonthsInAYear));

            default:
                {
                    Debug.LogError("Seconds doesnt have a weight.");
                    return 0;
                }
        }
    }

    public void ChangeTime(double change)
    {
        currentTime += change;
    }

    public void ChangeTime(int change, _time t)
    {
        switch (t)
        {
            case _time.minutes:{
                    currentTime += change * 60;
                    break;};
            case _time.hours:{
                    currentTime += change * 3600;
                    break;};
            case _time.days:{
                    currentTime += change * 3600 * hoursInADay;
                    break;};
            case _time.months:{
                    currentTime += change * 3600 * hoursInADay * daysInAMonth;
                    break;};
            case _time.years:{
                    currentTime += change * 3600 * hoursInADay * daysInAMonth * MonthsInAYear;
                    break;};
            default:{
                    currentTime += change;
                    break;}
        }
    }
}
