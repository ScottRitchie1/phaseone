﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Prototype_ScanUI : MonoBehaviour {

    public static Prototype_ScanUI instance;

    public Canvas scanUI;
    public Text scanName;
    public Text scanOrganics;
    public Text scanH20;
    public Text scanMineral;
    public Text scanEnergy;
    public Text scanCompound;
    public float displayTime = 1;

    void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("Prototype_ScanUI instance already set."); }
    }

    private void Start()
    {
        scanUI = GetComponentInChildren<Canvas>();
        scanUI.enabled = false;
    }

    public void ScanItem(Prototype_Interactable item)
    {
        //Debug.Log("SCANNING: " + item.currentStage.name);

        if(scanUI.enabled == true)
            StopCoroutine("ScanCoroutine");

        if (item.currentStage.name != "")
        {
            scanName.text = item.currentStage.name;
        }
        else
        {
            scanName.text = item.currentStage.stage.gameObject.name;
        }

        scanOrganics.gameObject.SetActive(item.currentStage.organicsRange.y > 0);
        scanCompound.gameObject.SetActive(item.currentStage.compoundRange.y > 0);
        scanH20.gameObject.SetActive(item.currentStage.h20Range.y > 0);
        scanMineral.gameObject.SetActive(item.currentStage.mineralRange.y > 0);
        scanEnergy.gameObject.SetActive(item.currentStage.energyRange.y > 0);

        scanOrganics.text = item.currentStage.organicsRange.x.ToString() + "-" + item.currentStage.organicsRange.y.ToString() + " Organics";
        scanH20.text = item.currentStage.h20Range.x.ToString() + "-" + item.currentStage.h20Range.y.ToString() + " H20";
        scanMineral.text = item.currentStage.mineralRange.x.ToString() + "-" + item.currentStage.mineralRange.y.ToString() + " Minerals";
        scanEnergy.text = item.currentStage.energyRange.x.ToString() + "-" + item.currentStage.energyRange.y.ToString() + " Energy";
        scanCompound.text = item.currentStage.compoundRange.x.ToString() + "-" + item.currentStage.compoundRange.y.ToString() + " Compound";

        StartCoroutine("ScanCoroutine", displayTime);
    }

    IEnumerator ScanCoroutine(float time)
    {
        float curTime = time;
        scanUI.enabled = true;
        yield return new WaitForSeconds(time);
        scanUI.enabled = false;
    }
}
