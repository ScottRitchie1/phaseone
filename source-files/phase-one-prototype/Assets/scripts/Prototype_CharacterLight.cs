﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Prototype_CharacterLight : MonoBehaviour {


    public GameObject lights;

	
	// Update is called once per frame
	void Update () {
        lights.SetActive(DayCycleManager.instance.isNight());
	}
    
}
