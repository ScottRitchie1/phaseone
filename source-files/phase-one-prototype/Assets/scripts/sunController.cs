﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sunController : MonoBehaviour {

    public static sunController instance;

    public Light lightSource;
    public Light hueLight;

    //public Color dayTimeColour;
    //public Color nightTimeColour;
    public Color currentColour;
    public float current;
    public float timeScale = 5f;
	[Range(0,1)]
	public float nightScale = 0.5f;
    float hueIn;
    float mainIn;
    // Use this for initialization

    private void Awake()
    {
        instance = this;
    }

    void Start () {
        hueIn = hueLight.intensity;
        mainIn = lightSource.intensity;
        RenderSettings.sun = lightSource;
    }

    // Update is called once per frame
    void Update () {
        float dot = Vector3.Dot(-lightSource.transform.forward, Vector3.up);
        current = Mathf.Clamp01(1-((1-dot) * (1-dot)));
        lightSource.intensity = current * mainIn;
        hueLight.intensity = current * hueIn;
        //currentColour = Color.Lerp(nightTimeColour, dayTimeColour, current);
        //RenderSettings.ambientLight = currentColour;
        RenderSettings.reflectionIntensity = current;
        RenderSettings.fogDensity = current * 0.01f;
        currentColour = RenderSettings.ambientEquatorColor;
		RenderSettings.ambientIntensity = Mathf.Max(current, nightScale);

        float inputMultiply = 1;
        if (Input.GetKey(KeyCode.Alpha3))
        {
            inputMultiply = 50;
        }
        lightSource.transform.parent.Rotate(Vector3.right * timeScale * Time.deltaTime * inputMultiply);
    }

    public void RandomizeSun()
    {
        lightSource.transform.parent.Rotate(Vector3.right * Random.Range(0, 360));
    }
}
