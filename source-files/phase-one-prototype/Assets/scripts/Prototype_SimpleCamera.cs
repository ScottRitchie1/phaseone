﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Prototype_SimpleCamera : MonoBehaviour {

    public static Prototype_SimpleCamera cameraInsance;
    public Camera actualCamera;

    public Transform target;
    Transform cameraPivot;
    [SerializeField]
    Vector3 cameraOriginLocalPos;
    [SerializeField]
    Quaternion cameraOriginLocalRot;
    Transform pivot;
    [SerializeField]
    public float positionSmoothing = 2, cameraSmoothing = 5;
    private Vector3 smoothDampPosRef;

    [SerializeField]
    float sensitivity = 2;

    [SerializeField]
    int angleMin = -80, angleMax = 80;

    private float curAngleX, curAngleY;
    private Quaternion pivotRot, baseRot;
    // Use this for initialization\

    private void Awake()
    {
        cameraInsance = this;
    }

    void Start () {
        pivot = transform.GetChild(0);
        cameraPivot = pivot.GetChild(0);
        cameraOriginLocalPos = cameraPivot.localPosition;
        cameraOriginLocalRot = cameraPivot.localRotation;
        actualCamera = cameraPivot.GetComponent<Camera>();

    }

    // Update is called once per frame
    private void LateUpdate()
    {
        
        float xInput, yInput;
        xInput = -Input.GetAxis("Mouse Y");
        yInput = Input.GetAxis("Mouse X");

        curAngleX += xInput * sensitivity;
        curAngleX = Mathf.Clamp(curAngleX, angleMin, angleMax);
        pivotRot = Quaternion.Euler(curAngleX, 0, 0);

        curAngleY += yInput * sensitivity;
        baseRot = Quaternion.Euler(0, curAngleY, 0);

        pivot.localRotation = Quaternion.Slerp(pivot.localRotation, pivotRot, cameraSmoothing * Time.deltaTime);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, baseRot, cameraSmoothing * Time.deltaTime);

        

        cameraPivot.localPosition = Vector3.MoveTowards(cameraPivot.localPosition, cameraOriginLocalPos, Time.deltaTime * positionSmoothing);
        cameraPivot.localRotation = Quaternion.Slerp(cameraPivot.localRotation, cameraOriginLocalRot, cameraSmoothing * Time.deltaTime);

        transform.position = target.position;//Vector3.SmoothDamp(transform.position, , ref smoothDampPosRef, Time.deltaTime * positionSmoothing);

    }

    private void OnDrawGizmosSelected()
    {
        if (cameraPivot && pivot)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, pivot.position);
            Gizmos.DrawLine(pivot.position, cameraPivot.position);
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(cameraPivot.position, 1);
        }

        if (target)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, target.position);
        }
    }
}
