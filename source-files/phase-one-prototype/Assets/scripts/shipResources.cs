﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shipResources : MonoBehaviour {

    public static shipResources instance;

    public int organics = 0;
    public int h20 = 0;
    public int compound = 0;
    public int mineral = 0;
    public int energy = 0;

    public Text organicsText;
    public Text h20Text;
    public Text compoundText;
    public Text mineralText;
    public Text energyText;

    public GameObject lights;


    // Use this for initialization
    void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        organicsText.text = "Organics: " + organics.ToString();
        h20Text.text = "H20: " + h20.ToString();
        compoundText.text = "Compound: " + compound.ToString();
        mineralText.text = "Mineral: " + mineral.ToString();
        energyText.text = "Energy: " + energy.ToString();

        lights.SetActive(DayCycleManager.instance.isNight());
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            compound += 10;
            organics += 10;
            h20 += 10;
            energy += 10;
            mineral += 10;
        }

    }

    public bool hasResources(int _organics, int _compound, int _h20, int _mineral, int _energy)
    {
        if (organics - _organics < 0)
            return false;
        if (compound - _compound < 0)
            return false;
        if (h20 - _h20 < 0)
            return false;
        if (mineral - _mineral < 0)
            return false;
        if (energy - _energy < 0)
            return false;


        return true;
    }



}
