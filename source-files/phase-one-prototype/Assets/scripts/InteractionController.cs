﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour {

    public static InteractionController instance;

    CamController cam;

    public float eyeHeight;

    public float interactionRange;
    public float interactionAngle;

    int layerMask;
    // Use this for initialization
    void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("InteractionController instance already set."); }
    }

    void Start () {
        layerMask = LayerMask.GetMask("InteractableEntity");
        cam = CamController.instance;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetMouseButtonDown(0))
        {
            InteractableEntityBase target = GetInteractionTarget(cam.transform.forward, InteractionPosition());

            if(target != null)
            {
                target.OnHarvest();
            }
        }else if (Input.GetKeyDown(KeyCode.E) || Input.GetMouseButtonDown(1))
        {
            InteractableEntityBase target = GetInteractionTarget(cam.transform.forward, InteractionPosition());

            if (target != null)
            {
                target.OnScan();
            }
        }

    }

    public InteractableEntityBase GetInteractionTarget(Vector3 targetingDir, Vector3 targetingPos)
    {
        Collider[] hitInteractables;

        hitInteractables = GetHitInteractablesInInteractionRange();

        if(hitInteractables.Length > 0)
        {
            int targetIndex = -1;
            float targetAngle = 0;

            for(int i = 0; i < hitInteractables.Length; i++)
            {
                float tempAngle = Vector3.Angle(hitInteractables[i].transform.position - targetingPos, targetingDir);

                if(targetIndex == -1 || tempAngle < targetAngle)
                {
                    targetIndex = i;
                    targetAngle = tempAngle;
                }
            }

            if(targetAngle <= interactionAngle)
            {
                return hitInteractables[targetIndex].GetComponent<InteractableEntityBase>();
            }
        }

        return null;
    }

    public Collider[] GetHitInteractablesInInteractionRange()
    {
        return Physics.OverlapSphere(transform.position, interactionRange, layerMask, QueryTriggerInteraction.Ignore);
    }

    Vector3 InteractionPosition()
    {
        return transform.position + transform.up * eyeHeight;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(InteractionPosition(), (cam? cam.transform.forward : transform.forward) * interactionRange);

        Gizmos.DrawWireSphere(InteractionPosition(), interactionRange);

        if (cam)
        {
            Collider[] inRange = GetHitInteractablesInInteractionRange();
            InteractableEntityBase target = GetInteractionTarget(cam.transform.forward, InteractionPosition());

            foreach (Collider col in inRange)
            {
                Gizmos.color = Color.green;
                if (target && col.gameObject == target.gameObject)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawLine(col.transform.position, InteractionPosition());

                }
                Gizmos.DrawRay(col.transform.position, Vector3.up * 10);
            }

        }

    }
#endif
}
