﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prototype_CharacterController : MonoBehaviour {

    Rigidbody rigidbody;
    public static Prototype_CharacterController instance;
    Prototype_SimpleCamera cam;
    private Transform ourTrans;

    public float moveSpeed = 350f;
    public float jumpForce = 200;

    float cameraDirection;
    float characterDirection;
    float targetDirection;

    float targetDeltaAngle;
    float cameraDeltaAngle;

    [Space(1)]
    [Header("Input")]
    [SerializeField]
    Vector2 moveInput;
    float speed;//0 is idle, 1 is walk, 2 is run, 3 is sprint
    float targetSpeed = 0;

    [Space(1)]
    [Header("Movement Smoothing")]
    public float turnLerp = 2;
    public float speedLerp = 2;


    [Space(1)]
    [Header("Grounded Check")]
    RaycastHit groundedHit;
    public bool isGrounded = true;
    public float groundedCheckDistance = 1f;
    public float groundLerp = 1f;
    
    float groundedCheckRadius;

    
    [Space(2)]
    [Header("DEBUG")]

    [Space(2)]
    [Header("Grounded check")]
    [SerializeField]
    bool DisableInputs = false;
    [SerializeField]
    bool OverlayText = false;
    [SerializeField]
    bool DrawGismos = false;

    private void Awake()
    {
        instance = this;

    }

    void Start () {
        //QualitySettings.vSyncCount = 0;
        //Application.targetFrameRate = 10;
        //Grab Reference to Self Transform
        ourTrans = this.transform;
        cam = Prototype_SimpleCamera.cameraInsance;
        groundedCheckRadius = GetComponent<CapsuleCollider>().radius;
        rigidbody = GetComponent<Rigidbody>();
    }


    void Update() {

        //INPUTS
        if(!DisableInputs)
        moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        cameraDirection = cam.transform.eulerAngles.y;
        characterDirection = ourTrans.eulerAngles.y;
        targetDirection = ourTrans.eulerAngles.y;

        if (isGrounded)
        {
            isGrounded = Physics.SphereCast(ourTrans.position + new Vector3(0, groundedCheckRadius * 1.1f + 0.5f, 0), groundedCheckRadius * 1.1f, -ourTrans.up, out groundedHit, groundedCheckDistance+0.5f);
        }
        else
        {
            if (rigidbody.velocity.y < 0)
            {
                isGrounded = Physics.SphereCast(ourTrans.position + new Vector3(0, groundedCheckRadius, 0), groundedCheckRadius * 0.9f, -ourTrans.up, out groundedHit, groundedCheckRadius);
            }
        }
        

        if (moveInput.magnitude > 0)
        {
            targetDirection = cam.transform.eulerAngles.y;

        }


        if (Input.GetKey(KeyCode.W))
        {
            if (Input.GetKey(KeyCode.D))
            {
                targetDirection += 45;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                targetDirection -= 45;
            }

        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (Input.GetKey(KeyCode.D))
            {
                targetDirection += 135;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                targetDirection -= 135;
            }
            else
            {
                targetDirection += 180;
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.D))
            {
                targetDirection += 90;
            }
            if (Input.GetKey(KeyCode.A))
            {
                targetDirection -= 90;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            isGrounded = false;
            rigidbody.AddForce(transform.up * jumpForce);
        }

        targetDirection = targetDirection % 360;

        targetDeltaAngle = Mathf.DeltaAngle(ourTrans.eulerAngles.y, targetDirection);
        cameraDeltaAngle = Mathf.DeltaAngle(ourTrans.eulerAngles.y, cameraDirection);

        if (Mathf.Approximately(targetDeltaAngle, 0))
        {
            targetDeltaAngle = 0;
        }
        if (Mathf.Approximately(cameraDeltaAngle, 0))
        {
            cameraDeltaAngle = 0;
        }



        targetSpeed = Mathf.Max(Mathf.Abs(moveInput.x), Mathf.Abs(moveInput.y));
        speed = Mathf.MoveTowards(speed, targetSpeed, Time.deltaTime * speedLerp);
        float calTurnLerp = turnLerp;
        if(isGrounded == false)
        {
            calTurnLerp *= 0.1f;
        }
        ourTrans.eulerAngles = new Vector3(ourTrans.eulerAngles.x, Mathf.LerpAngle(ourTrans.eulerAngles.y, targetDirection, calTurnLerp * Time.deltaTime), ourTrans.eulerAngles.z);


        if (isGrounded)
        {
            rigidbody.velocity = transform.forward * (speed * moveSpeed * Time.deltaTime); //new Vector3(anim.deltaPosition.x * 50, rigidbody.velocity.y, anim.deltaPosition.z * 50);
            ourTrans.position = new Vector3(ourTrans.position.x, Mathf.Lerp(ourTrans.position.y, groundedHit.point.y, Time.deltaTime * groundLerp), ourTrans.position.z);
        }
    }

    private void OnGUI()
    {
        if (OverlayText)
        {
            GUIStyle blackText = new GUIStyle();
            blackText.normal.textColor = Color.black;

            GUIStyle blueText = new GUIStyle();
            blueText.normal.textColor = Color.blue;

            GUIStyle redText = new GUIStyle();
            redText.normal.textColor = Color.red;

            GUI.Label(new Rect(10, 10, 200, 20), ("MoveInput: " + moveInput.ToString()), blackText);
            GUI.Label(new Rect(10, 30, 200, 20), ("Speed:" + speed.ToString()), blackText);
            GUI.Label(new Rect(10, 70, 200, 20), ("Target Delta Angle: " + targetDeltaAngle.ToString()), blueText);
            GUI.Label(new Rect(10, 90, 200, 20), ("Camera Delta Angle: " + cameraDeltaAngle.ToString()), blueText);

            GUI.Label(new Rect(10, 140, 200, 20), ("Is Grounded: " + isGrounded.ToString()), redText);
        }

    }

    private void OnDrawGizmos()
    {
        if (DrawGismos)
        {
            float lineLenght = 10;
            if (cam)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(ourTrans.position, ourTrans.position + (cam.transform.forward * lineLenght));
            }

            if (Mathf.Approximately(characterDirection, targetDirection))
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(ourTrans.position, ourTrans.position + (ourTrans.forward * lineLenght));
            }
            else
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(ourTrans.position, ourTrans.position + (ourTrans.forward * lineLenght));
                Gizmos.color = Color.red;
                Gizmos.DrawLine(ourTrans.position, ourTrans.position + ((Quaternion.Euler(new Vector3(ourTrans.eulerAngles.x, targetDirection, ourTrans.eulerAngles.z)) * Vector3.forward) * 10));
            }
            Gizmos.color = Color.yellow;

            Gizmos.color = Color.grey;
            Gizmos.DrawRay(ourTrans.position, -ourTrans.up * groundedCheckDistance);
        }
    }
}
