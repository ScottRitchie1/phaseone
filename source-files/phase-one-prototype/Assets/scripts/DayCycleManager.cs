﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycleManager : MonoBehaviour {
    public static DayCycleManager instance;


    public Transform rotater;
    public Light dayLight;

    [Header("Settings")]
    public Gradient colourCurve;
    public AnimationCurve dayLightIntensity;
    public AnimationCurve ambientLightIntensityCurve;
    public AnimationCurve nightFogWeight;
    [Space]
    [Range(0.1f, 0.9f)]
    public float dayNightWeight;

    [Range(0, 1)]

    public float nightTimeDarknessCap;

    [Space]
    public bool useWorldTime = true;
    public float cycleTime;

    [Range(0, 1)]
    public float weight;

    private void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("DayCycleController instance already set."); }
    }
    void Start () {
        if (cycleTime <= 0)
            cycleTime = 1;

        RenderSettings.sun = dayLight;
        RenderSettings.fog = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (useWorldTime)//only calculate our own weight if we dont want to use the worldTimeCycle
        {
            weight = WorldTimeManager.dayWeight;
        }else {
            weight = (weight + ((Time.deltaTime / cycleTime) * (Input.GetKey(KeyCode.Alpha3) ? 60 : 1))) % 1;
        }


        rotater.localEulerAngles = Vector3.right * (Mathf.Lerp(-90, 270, weight));


        dayLight.intensity = dayLightIntensity.Evaluate(weight);
        dayLight.color = colourCurve.Evaluate(weight);
        RenderSettings.ambientIntensity = Mathf.Max(ambientLightIntensityCurve.Evaluate(weight), nightTimeDarknessCap);
        RenderSettings.ambientLight = dayLight.color;
        RenderSettings.reflectionIntensity = RenderSettings.ambientIntensity;
        RenderSettings.fogDensity = nightFogWeight.Evaluate(weight);
    }

    public bool isNight()
    {
        return (weight < 0.25f || weight > 0.75f);
    }

    public void RandomizeCycle()
    {
        weight = Random.Range(0, 1);
    }
}
