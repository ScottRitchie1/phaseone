﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanLookAt : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.LookAt(CamController.instance.transform);
	}
}
