﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameQuit : MonoBehaviour {

    float t = 0;
	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        

        if (Input.GetKey(KeyCode.Escape))
        {
            t += Time.deltaTime;

            if(t > 2)
            {
                Application.Quit();
            }
        }else if (Input.GetKeyUp(KeyCode.Escape))
        {
            t = 0;
        }
	}
}
