﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prototype_Builder : MonoBehaviour {

    public static Prototype_Builder instance;

    [System.Serializable]
    public class Buildable
    {
        public string structureName;
        public GameObject holoGram;
        public GameObject Structure;
        public float yearsToBuild;
        public int organics = 0;
        public int h20 = 0;
        public int compound = 0;
        public int mineral = 0;
        public int energy = 0;
    }

    public List<Buildable> buildables = new List<Buildable>();
    public GameObject currentHoloGram;
    public int currentBuilding = 0;
    public bool hasResources = false;

    public Color cantBuild;
    public Color canBuild;

    public bool inBuildMode = false;
    // Use this for initialization
    private void Awake()
    {
        instance = this;
    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(2))
        {
            inBuildMode = !inBuildMode;
            if(inBuildMode == false)
            {
                Destroy(currentHoloGram);
            }
        }

        if(CharController.instance.isGrounded == false)
        {
            inBuildMode = false;
            if (currentHoloGram)
            {
                Destroy(currentHoloGram);
            }
        }

        if (inBuildMode)
        {
            if(Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                currentBuilding++;
            }else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                currentBuilding--;
            }

            currentBuilding = currentBuilding % buildables.Count;
            if(currentBuilding < 0)
            {
                currentBuilding = buildables.Count - 1;
            }


            if(currentHoloGram == null || currentHoloGram.name != buildables[currentBuilding].holoGram.name)
            {
                Destroy(currentHoloGram);
                currentHoloGram = Instantiate(buildables[currentBuilding].holoGram, Vector3.zero, Quaternion.identity);
                currentHoloGram.name = buildables[currentBuilding].holoGram.name;
                Debug.Log("aaaa");
                if (shipResources.instance.hasResources(
                    buildables[currentBuilding].organics,
                    buildables[currentBuilding].compound,
                    buildables[currentBuilding].h20,
                    buildables[currentBuilding].mineral,
                    buildables[currentBuilding].energy) == false)
                {
                    hasResources = false;
                    foreach(Renderer child in currentHoloGram.GetComponentsInChildren<Renderer>())
                    {
                        child.material.color = cantBuild;
                    }
                }
                else
                {
                    hasResources = true;
                    foreach (Renderer child in currentHoloGram.GetComponentsInChildren<Renderer>())
                    {
                        child.material.color = canBuild;
                    }
                }
            }

            Vector3 point = transform.position + CamController.instance.GetCameraDirection() * 2;
            RaycastHit hit;
            if(Physics.Raycast(point + Vector3.up * 5, Vector3.down, out hit, 10)) {
                point = new Vector3(point.x, hit.point.y, point.z);
            }

            currentHoloGram.transform.position = point;

            if (Input.GetMouseButton(0))
            {
                currentHoloGram.transform.eulerAngles += Vector3.up * 50 * Time.deltaTime;
            }

            if (hasResources && Input.GetMouseButtonDown(1))
            {
                Instantiate(buildables[currentBuilding].Structure, currentHoloGram.transform.position, currentHoloGram.transform.rotation);
                shipResources.instance.organics -= buildables[currentBuilding].organics;
                shipResources.instance.h20 -= buildables[currentBuilding].h20;
                shipResources.instance.mineral -= buildables[currentBuilding].mineral;
                shipResources.instance.energy -= buildables[currentBuilding].energy;
                shipResources.instance.compound -= buildables[currentBuilding].compound;
                Destroy(currentHoloGram);
                currentHoloGram = null;

            }
        }
	}
}
