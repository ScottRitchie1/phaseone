﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class respawn : MonoBehaviour {

    public static respawn instance;
    public static int NextCloneInYears = 35;
    public Text nextCloneText;
    public Transform respawnPoint;

    // Use this for initialization
    private void Awake()
    {
        instance = this;
    }

    void Start () {
        NextCloneInYears = Random.Range(20, 60);
        CharController.instance.transform.position = respawnPoint.transform.position;
        CharController.instance.transform.rotation = respawnPoint.transform.rotation;
    }
	
	// Update is called once per frame
	void Update () {
        nextCloneText.text = NextCloneInYears.ToString() + " years till new clone is born";

    }

    public void Respawn()
    {
        yearManager.currentYear += NextCloneInYears;
        NextCloneInYears = Random.Range(20, 60);
        WorldTimeManager.instance.ChangeTime(NextCloneInYears, WorldTimeManager._time.years);
        CharController.instance.transform.position = respawnPoint.transform.position;
        CharController.instance.transform.rotation = respawnPoint.transform.rotation;
        float f = Random.Range(0.0f, WorldTimeManager.instance.hoursInADay) * 60.0f;
        WorldTimeManager.instance.ChangeTime((int)f, WorldTimeManager._time.minutes);
    }
}
