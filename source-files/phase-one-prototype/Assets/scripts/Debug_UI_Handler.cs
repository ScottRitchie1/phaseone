﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Debug_UI_Handler : MonoBehaviour {

    public InputField changeTimeInput;
    public Text timeScaleText;
    public float timeScale = 1;
    public Text timeText;
    bool timeDisplayType;
    public Toggle timeDisplayTypeToggle;
    public Text timeCycleDisplay;
    bool isActive;
	// Use this for initialization
	void Start () {
#if !UNITY_EDITOR
        //Destroy(gameObject);
#endif
    }
	
	// Update is called once per frame
	void Update () {
        transform.GetChild(0).gameObject.SetActive(isActive);
        if (Input.GetKeyDown(KeyCode.Escape)) isActive = !isActive;
		if(timeScale > 1 || timeScale  < 0)
        {
            WorldTimeManager.currentTime += Time.deltaTime * (timeScale - 1);
        }

        if (timeScaleText) timeScaleText.text = "     TimeScale: " + timeScale.ToString();
        if (timeText) timeText.text = WorldTimeManager.instance.TimeString(timeDisplayType);
        if(timeDisplayTypeToggle) timeDisplayType = timeDisplayTypeToggle.isOn;
        if (timeCycleDisplay) timeCycleDisplay.text = "HOURS IN DAY: " + WorldTimeManager.instance.hoursInADay.ToString() + "\nDAYS IN MONTHS: " + WorldTimeManager.instance.daysInAMonth.ToString() + "\nMONTHS IN YEAR: " + WorldTimeManager.instance.MonthsInAYear.ToString();

    }

    public void ChangeTimeYears()
    {
        int result;
        if(changeTimeInput)WorldTimeManager.instance.ChangeTime((int.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.years);
    }

    public void ChangeTimeMonths()
    {
        int result;
        if (changeTimeInput) WorldTimeManager.instance.ChangeTime((int.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.months);
    }

    public void ChangeTimeDays()
    {
        int result;
        if (changeTimeInput) WorldTimeManager.instance.ChangeTime((int.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.days);
    }

    public void ChangeTimeHours()
    {
        int result;
        if (changeTimeInput) WorldTimeManager.instance.ChangeTime((int.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.hours);
    }

    public void ChangeTimeMinutes()
    {
        int result;
        if (changeTimeInput) WorldTimeManager.instance.ChangeTime((int.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.minutes);
    }

    public void ChangeTimeSeconds()
    {
        int result;
        if (changeTimeInput) WorldTimeManager.instance.ChangeTime((int.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.seconds);
    }

    public void ChangeTimeScale(float t)
    {
        timeScale = t;
    }

    public void Respawn()
    {
        respawn.instance.Respawn();
    }
    public void ResetGame()
    {
        SceneManager.LoadScene(0);
    }

}
