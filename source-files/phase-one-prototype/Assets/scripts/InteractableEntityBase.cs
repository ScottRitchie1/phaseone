﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class InteractableEntityBase : MonoBehaviour {

    private void Awake()
    {
#if UNITY_EDITOR
        //gameObject.layer = LayerMask.NameToLayer("InteractableEntity");

        if (gameObject.layer != LayerMask.NameToLayer("InteractableEntity"))
            Debug.LogError(gameObject.name + " is an entity but doesnt have the 'InteractableEntity' Layer Set.");
#endif
    }

    public virtual void OnScan()
    {
        Debug.Log(gameObject.name + " was scanned.");
    }

    public virtual void OnHarvest()
    {
        Debug.Log(gameObject.name + " was harvested.");
    }
}
