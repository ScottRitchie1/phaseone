﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class yearManager : MonoBehaviour {
    public static int currentYear = 0;
    public Text currentYearDisplay;
	// Use this for initialization
	void Start () {
        currentYear = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentYear++;
            if(respawn.NextCloneInYears > 0)
            respawn.NextCloneInYears--;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentYear--;
            respawn.NextCloneInYears++;

        }

        currentYearDisplay.text = "year: " + currentYear.ToString();

    }
}
