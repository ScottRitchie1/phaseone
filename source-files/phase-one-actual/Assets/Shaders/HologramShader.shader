// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:33269,y:32452,varname:node_2865,prsc:2|diff-7662-RGB,spec-7584-OUT,emission-131-OUT,alpha-4656-OUT;n:type:ShaderForge.SFN_Tex2d,id:4370,x:32587,y:32005,ptovrint:False,ptlb:Hologram Texure,ptin:_HologramTexure,varname:node_4370,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9b122435a588c0f4ca41a832cacfdf01,ntxv:2,isnm:False|UVIN-8247-OUT;n:type:ShaderForge.SFN_Slider,id:7453,x:32068,y:32320,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_7453,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.111111,max:10;n:type:ShaderForge.SFN_Fresnel,id:6522,x:32387,y:32441,varname:node_6522,prsc:2|EXP-7453-OUT;n:type:ShaderForge.SFN_Multiply,id:8569,x:32556,y:32441,varname:node_8569,prsc:2|A-1362-OUT,B-6522-OUT;n:type:ShaderForge.SFN_Time,id:8167,x:31459,y:32129,varname:node_8167,prsc:2;n:type:ShaderForge.SFN_Add,id:8247,x:32175,y:31853,varname:node_8247,prsc:2|A-3748-UVOUT,B-5008-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1046,x:31658,y:31964,ptovrint:False,ptlb:Scroll Speed,ptin:_ScrollSpeed,varname:node_1046,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:5008,x:31849,y:31964,varname:node_5008,prsc:2|A-1046-OUT,B-8167-T;n:type:ShaderForge.SFN_Color,id:7662,x:32387,y:32595,ptovrint:False,ptlb:Colour,ptin:_Colour,varname:node_7662,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.1302326,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:6794,x:32556,y:32575,varname:node_6794,prsc:2|A-8569-OUT,B-7662-RGB;n:type:ShaderForge.SFN_Slider,id:7584,x:33098,y:32190,ptovrint:False,ptlb:Metalic,ptin:_Metalic,varname:node_7584,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:4226,x:32794,y:32022,varname:node_4226,prsc:2|A-4370-R,B-4370-G;n:type:ShaderForge.SFN_Multiply,id:1362,x:32794,y:32139,varname:node_1362,prsc:2|A-4226-OUT,B-4370-B;n:type:ShaderForge.SFN_Color,id:2198,x:32718,y:33066,ptovrint:False,ptlb:Glow Fresnel Colour,ptin:_GlowFresnelColour,varname:node_2198,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.9706273,c3:1,c4:1;n:type:ShaderForge.SFN_Fresnel,id:3473,x:32570,y:32978,varname:node_3473,prsc:2|EXP-6568-OUT;n:type:ShaderForge.SFN_Slider,id:6568,x:32220,y:32999,ptovrint:False,ptlb:Glow Fresnel Size,ptin:_GlowFresnelSize,varname:node_6568,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:6.728374,max:10;n:type:ShaderForge.SFN_Multiply,id:9662,x:32838,y:33213,varname:node_9662,prsc:2|A-3473-OUT,B-2198-RGB;n:type:ShaderForge.SFN_ValueProperty,id:8281,x:32346,y:32913,ptovrint:False,ptlb:Glow Intensity,ptin:_GlowIntensity,varname:node_8281,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Multiply,id:5085,x:32558,y:32826,varname:node_5085,prsc:2|A-8281-OUT,B-9662-OUT;n:type:ShaderForge.SFN_Add,id:6633,x:32903,y:32503,varname:node_6633,prsc:2|A-6794-OUT,B-5085-OUT;n:type:ShaderForge.SFN_Noise,id:3085,x:31869,y:32258,varname:node_3085,prsc:2|XY-6405-OUT;n:type:ShaderForge.SFN_Multiply,id:6405,x:31695,y:32242,varname:node_6405,prsc:2|A-8479-OUT,B-8167-T;n:type:ShaderForge.SFN_Vector1,id:8479,x:31865,y:32756,varname:node_8479,prsc:2,v1:1;n:type:ShaderForge.SFN_ScreenPos,id:3748,x:31847,y:31826,varname:node_3748,prsc:2,sctp:2;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:3385,x:32139,y:32639,varname:node_3385,prsc:2|IN-3085-OUT,IMIN-8479-OUT,IMAX-2695-OUT,OMIN-8479-OUT,OMAX-3094-OUT;n:type:ShaderForge.SFN_Multiply,id:4762,x:32796,y:32695,varname:node_4762,prsc:2|A-8569-OUT,B-3028-OUT;n:type:ShaderForge.SFN_Add,id:9775,x:32817,y:32825,varname:node_9775,prsc:2|A-4762-OUT,B-9263-OUT;n:type:ShaderForge.SFN_Multiply,id:9263,x:33132,y:32953,varname:node_9263,prsc:2|A-431-OUT,B-3473-OUT;n:type:ShaderForge.SFN_Slider,id:2695,x:31708,y:32443,ptovrint:False,ptlb:Flicker Rate,ptin:_FlickerRate,varname:node_2695,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.540171,max:0.95;n:type:ShaderForge.SFN_Slider,id:6000,x:31712,y:32537,ptovrint:False,ptlb:Flicker Intensity,ptin:_FlickerIntensity,varname:node_6000,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.65,max:1;n:type:ShaderForge.SFN_OneMinus,id:3094,x:31865,y:32618,varname:node_3094,prsc:2|IN-6000-OUT;n:type:ShaderForge.SFN_Slider,id:431,x:33064,y:33204,ptovrint:False,ptlb:Fresnel Blend,ptin:_FresnelBlend,varname:node_431,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Abs,id:3028,x:32377,y:32744,varname:node_3028,prsc:2|IN-5183-OUT;n:type:ShaderForge.SFN_Clamp01,id:5183,x:32263,y:32705,varname:node_5183,prsc:2|IN-3385-OUT;n:type:ShaderForge.SFN_Multiply,id:131,x:33090,y:32543,varname:node_131,prsc:2|A-6633-OUT,B-5520-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5520,x:33090,y:32681,ptovrint:False,ptlb:Emmision,ptin:_Emmision,varname:node_5520,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Slider,id:4275,x:32817,y:32953,ptovrint:False,ptlb:Minimum Opacity,ptin:_MinimumOpacity,varname:node_4275,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1623932,max:1;n:type:ShaderForge.SFN_Clamp,id:4656,x:33111,y:32766,varname:node_4656,prsc:2|IN-9775-OUT,MIN-4275-OUT,MAX-8857-OUT;n:type:ShaderForge.SFN_Vector1,id:8857,x:33111,y:32895,varname:node_8857,prsc:2,v1:1;proporder:4370-7453-1046-7662-7584-2198-6568-8281-2695-6000-431-5520-4275;pass:END;sub:END;*/

Shader "Shader Forge/HologramShader" {
    Properties {
        _HologramTexure ("Hologram Texure", 2D) = "black" {}
        _Fresnel ("Fresnel", Range(0, 10)) = 1.111111
        _ScrollSpeed ("Scroll Speed", Float ) = 0.1
        _Colour ("Colour", Color) = (0,0.1302326,1,1)
        _Metalic ("Metalic", Range(0, 1)) = 0
        _GlowFresnelColour ("Glow Fresnel Colour", Color) = (0,0.9706273,1,1)
        _GlowFresnelSize ("Glow Fresnel Size", Range(0, 10)) = 6.728374
        _GlowIntensity ("Glow Intensity", Float ) = 5
        _FlickerRate ("Flicker Rate", Range(0, 0.95)) = 0.540171
        _FlickerIntensity ("Flicker Intensity", Range(0, 1)) = 0.65
        _FresnelBlend ("Fresnel Blend", Range(0, 1)) = 0
        _Emmision ("Emmision", Float ) = 1
        _MinimumOpacity ("Minimum Opacity", Range(0, 1)) = 0.1623932
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _HologramTexure; uniform float4 _HologramTexure_ST;
            uniform float _Fresnel;
            uniform float _ScrollSpeed;
            uniform float4 _Colour;
            uniform float _Metalic;
            uniform float4 _GlowFresnelColour;
            uniform float _GlowFresnelSize;
            uniform float _GlowIntensity;
            uniform float _FlickerRate;
            uniform float _FlickerIntensity;
            uniform float _FresnelBlend;
            uniform float _Emmision;
            uniform float _MinimumOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float3 tangentDir : TEXCOORD4;
                float3 bitangentDir : TEXCOORD5;
                float4 projPos : TEXCOORD6;
                UNITY_FOG_COORDS(7)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD8;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float2 sceneUVs = (i.projPos.xy / i.projPos.w);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = 0.5;
                float perceptualRoughness = 1.0 - 0.5;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metalic;
                float specularMonochrome;
                float3 diffuseColor = _Colour.rgb; // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                half surfaceReduction;
                #ifdef UNITY_COLORSPACE_GAMMA
                    surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;
                #else
                    surfaceReduction = 1.0/(roughness*roughness + 1.0);
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                indirectSpecular *= surfaceReduction;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 node_8167 = _Time;
                float2 node_8247 = (sceneUVs.rg+(_ScrollSpeed*node_8167.g));
                float4 _HologramTexure_var = tex2D(_HologramTexure,TRANSFORM_TEX(node_8247, _HologramTexure));
                float node_8569 = (((_HologramTexure_var.r*_HologramTexure_var.g)*_HologramTexure_var.b)*pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel));
                float node_3473 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_GlowFresnelSize);
                float3 emissive = (((node_8569*_Colour.rgb)+(_GlowIntensity*(node_3473*_GlowFresnelColour.rgb)))*_Emmision);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                float node_8479 = 1.0;
                float node_6405 = (node_8479*node_8167.g);
                float2 node_3085_skew = float2(node_6405,node_6405) + 0.2127+float2(node_6405,node_6405).x*0.3713*float2(node_6405,node_6405).y;
                float2 node_3085_rnd = 4.789*sin(489.123*(node_3085_skew));
                float node_3085 = frac(node_3085_rnd.x*node_3085_rnd.y*(1+node_3085_skew.x));
                fixed4 finalRGBA = fixed4(finalColor,clamp(((node_8569*abs(saturate((node_8479 + ( (node_3085 - node_8479) * ((1.0 - _FlickerIntensity) - node_8479) ) / (_FlickerRate - node_8479)))))+(_FresnelBlend*node_3473)),_MinimumOpacity,1.0));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _HologramTexure; uniform float4 _HologramTexure_ST;
            uniform float _Fresnel;
            uniform float _ScrollSpeed;
            uniform float4 _Colour;
            uniform float _Metalic;
            uniform float4 _GlowFresnelColour;
            uniform float _GlowFresnelSize;
            uniform float _GlowIntensity;
            uniform float _FlickerRate;
            uniform float _FlickerIntensity;
            uniform float _FresnelBlend;
            uniform float _Emmision;
            uniform float _MinimumOpacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float3 tangentDir : TEXCOORD4;
                float3 bitangentDir : TEXCOORD5;
                float4 projPos : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float2 sceneUVs = (i.projPos.xy / i.projPos.w);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                UNITY_LIGHT_ATTENUATION(attenuation,i, i.posWorld.xyz);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = 0.5;
                float perceptualRoughness = 1.0 - 0.5;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metalic;
                float specularMonochrome;
                float3 diffuseColor = _Colour.rgb; // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                float4 node_8167 = _Time;
                float2 node_8247 = (sceneUVs.rg+(_ScrollSpeed*node_8167.g));
                float4 _HologramTexure_var = tex2D(_HologramTexure,TRANSFORM_TEX(node_8247, _HologramTexure));
                float node_8569 = (((_HologramTexure_var.r*_HologramTexure_var.g)*_HologramTexure_var.b)*pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel));
                float node_8479 = 1.0;
                float node_6405 = (node_8479*node_8167.g);
                float2 node_3085_skew = float2(node_6405,node_6405) + 0.2127+float2(node_6405,node_6405).x*0.3713*float2(node_6405,node_6405).y;
                float2 node_3085_rnd = 4.789*sin(489.123*(node_3085_skew));
                float node_3085 = frac(node_3085_rnd.x*node_3085_rnd.y*(1+node_3085_skew.x));
                float node_3473 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_GlowFresnelSize);
                fixed4 finalRGBA = fixed4(finalColor * clamp(((node_8569*abs(saturate((node_8479 + ( (node_3085 - node_8479) * ((1.0 - _FlickerIntensity) - node_8479) ) / (_FlickerRate - node_8479)))))+(_FresnelBlend*node_3473)),_MinimumOpacity,1.0),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _HologramTexure; uniform float4 _HologramTexure_ST;
            uniform float _Fresnel;
            uniform float _ScrollSpeed;
            uniform float4 _Colour;
            uniform float _Metalic;
            uniform float4 _GlowFresnelColour;
            uniform float _GlowFresnelSize;
            uniform float _GlowIntensity;
            uniform float _Emmision;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 projPos : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float2 sceneUVs = (i.projPos.xy / i.projPos.w);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 node_8167 = _Time;
                float2 node_8247 = (sceneUVs.rg+(_ScrollSpeed*node_8167.g));
                float4 _HologramTexure_var = tex2D(_HologramTexure,TRANSFORM_TEX(node_8247, _HologramTexure));
                float node_8569 = (((_HologramTexure_var.r*_HologramTexure_var.g)*_HologramTexure_var.b)*pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel));
                float node_3473 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_GlowFresnelSize);
                o.Emission = (((node_8569*_Colour.rgb)+(_GlowIntensity*(node_3473*_GlowFresnelColour.rgb)))*_Emmision);
                
                float3 diffColor = _Colour.rgb;
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, _Metalic, specColor, specularMonochrome );
                o.Albedo = diffColor + specColor * 0.125; // No gloss connected. Assume it's 0.5
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
