﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour {

    public static TutorialManager instance;
    public AudioSource voiceAudio;
    public AudioClip[] clips;
    // Use this for initialization
    public bool allowDoorsToOpen = false;
    public bool allowBuild = false;
    public bool allowHarvest = false;
    public InteractableStructure_Seed seed;
    private void Awake()
    {
        instance = this;
        StartCoroutine("tutorialCoroutine");
    }
   


    IEnumerator tutorialCoroutine()
    {
        voiceAudio.PlayOneShot(clips[0]);
        while (voiceAudio.isPlaying)
        {
            yield return null;
        }
        allowDoorsToOpen = true;
        while(ScanUI.instance.isScanning == false)
        {
            yield return null;
        }
        voiceAudio.PlayOneShot(clips[1]);
        while (voiceAudio.isPlaying)
        {
            yield return null;
        }
        allowHarvest = true;
        while(CharacterInstanceManager.instance.interactionController.isHarvesting == false)
        {
            yield return null;
        }
        voiceAudio.PlayOneShot(clips[2]);
        while (Vector3.Distance(CharacterInstanceManager.instance.transform.position, ShipInstanceManager.instance.centerPivot.position) > 10)
        {
            yield return null;
        }
        voiceAudio.PlayOneShot(clips[3]);
        while (voiceAudio.isPlaying)
        {
            yield return null;
        }
        allowBuild = true;
        while(seed == null)
        {
            yield return null;
        }
        PowerComponent pc = seed.GetComponent<PowerComponent>();
        if (seed.GetComponent<PowerComponent>().IsPowered == false)
        {
            voiceAudio.PlayOneShot(clips[4]);
            while (voiceAudio.isPlaying)
            {
                yield return null;
            }
        }

        voiceAudio.PlayOneShot(clips[5]);
        while (CharacterInstanceManager.instance.animatorController.activeRagdoll == false)
        {
            yield return null;
        }
        voiceAudio.PlayOneShot(clips[6]);
        while (voiceAudio.isPlaying)
        {
            yield return null;
        }
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(0);

    }
}
