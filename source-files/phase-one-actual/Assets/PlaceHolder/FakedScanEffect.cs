﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakedScanEffect : InteractableEntityBase
{

    public List<ScanUI.ResourceElementArgs> resources = new List<ScanUI.ResourceElementArgs>();
    public string displayName = "Unknown";
    public float growthPercentage = -.53f;
    public string cantHarvestReason = "Requires appropriate tools.";
    bool requiresScan = true;
    public GameObject Effect;
    public bool NonHarvest;
    public MeshFilter scanMesh;
    // Use this for initialization
    public override void OnScan()
    {
        if (ScanUI.instance)
        {
            ScanUI.instance.ScanObject(transform, false, 2.5f);
            ScanUI.instance.UpdateIcon();
            ScanUI.instance.UpdateDisplayNameTitle(displayName);
            if (NonHarvest)
                ScanUI.instance.UpdateErrorMessage(cantHarvestReason);
            else
            {
                ScanUI.instance.UpdateErrorMessage();

            }
            ScanUI.instance.UpdateConnectionStatus();
            ScanUI.instance.UpdateOutput();
            if (resources.Count > 0)
            {
                ScanUI.instance.UpdateResourceDisplay("Available Resources", resources.ToArray());

            }
            else
            {
                ScanUI.instance.UpdateResourceDisplay();
            }
            if (growthPercentage != 0)
            {
                ScanUI.instance.UpdateProgress("Growth Cycle Complete", growthPercentage);
            }
            else
            {
                ScanUI.instance.UpdateProgress();
            }
            ScanUI.instance.UpdateScanEffect(scanMesh);

            if (Effect)
            {
                Effect.SetActive(true);
            }
        }
    }
}
