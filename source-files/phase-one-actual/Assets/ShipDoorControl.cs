﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipDoorControl : MonoBehaviour {

    public AudioClip doorOpenSound;
    public AudioClip doorCloseSound;

    private void Start()
    {
        GetComponent<Animator>().SetBool("isOpen", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (TutorialManager.instance.allowDoorsToOpen && GetComponent<Animator>().GetBool("isOpen") == false && other.gameObject.GetComponent<CharController>())
        {
            GetComponent<Animator>().SetBool("isOpen", true);
            GetComponentInChildren<AudioSource>().PlayOneShot(doorOpenSound);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (GetComponent<Animator>().GetBool("isOpen") && other.gameObject.GetComponent<CharController>())
        {
            GetComponent<Animator>().SetBool("isOpen", false);
            GetComponentInChildren<AudioSource>().PlayOneShot(doorCloseSound);
        }
    }
}
