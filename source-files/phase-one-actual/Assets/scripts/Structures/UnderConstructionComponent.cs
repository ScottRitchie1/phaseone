﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
//public class resourceDepletionTracker
//{
//    public ResourceItem resource;
//    public int amount;
//    public int currentDepleted = 0;
//}

public class UnderConstructionComponent : MonoBehaviour {

    public GameObject UnderConstructionObject;
    public LineRenderer underConstructionLine;
    public BoxCollider underConstructionNoBuildArea;

    public PowerComponent powerComponent;

    [SerializeField]
    double creationTime;
    [SerializeField]
    public double finishTime;
    [SerializeField]
    double ConstructionTime;
    [Range(0,1)]
    public float buildCompletionPercentage = 0;
    //[Space]

    //public List<resourceDepletionTracker> resourceDepletion = new List<resourceDepletionTracker>();
    // Use this for initialization
    void Awake () {
        UnderConstructionObject = new GameObject("UnderConstructionObject");
        UnderConstructionObject.transform.SetParent(this.transform);
        UnderConstructionObject.transform.localPosition = Vector3.zero;
        UnderConstructionObject.transform.localRotation = Quaternion.identity;
        UnderConstructionObject.layer = LayerMask.NameToLayer("BlockBuilding");
        underConstructionLine = UnderConstructionObject.AddComponent<LineRenderer>();
        underConstructionNoBuildArea = UnderConstructionObject.AddComponent<BoxCollider>();
        powerComponent = GetComponent<PowerComponent>();
	}
	
	// Update is called once per frame
	void Update () {

        buildCompletionPercentage = WorldTimeManager.TimeInverseLerp(creationTime, finishTime);
       // Debug.Log("                                                                   ");
       // Debug.Log("                                                                   ");
       //
       // Debug.Log("//////////////////////// -- NEW FRAME -- \\\\\\\\\\\\\\\\\\\\\\\\\\");
       // if(WorldTimeManager.deltaTime > 99999)
       // {
       //     donecheck = true;
       //
       // }
       //
       // if(donecheck)
       //     Debug.Log("here--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
       //
       // 
       //
       // buildingHasProgressed = true;
       // lastFrameBuildPrecentage = 0;
       //
       // for (int i = 0; i < resourceDepletion.Count; i++)
       // {
       //     Debug.Log("                                            ");
       //
       //     Debug.Log("+++++++++++++++++++++++" + resourceDepletion[i].resource.itemName + "+++++++++++++++++++++++");
       //
       //     float targetDepletedfloat = resourceDepletion[i].amount * (float)((cantBuildWaitTime + WorldTimeManager.deltaTime) / ConstructionTime);
       //     Debug.Log((float)((cantBuildWaitTime + WorldTimeManager.deltaTime) / ConstructionTime) + " * " + resourceDepletion[i].amount + " = " + targetDepletedfloat);
       //     int targetDepleted = Mathf.Clamp(targetDepletedfloat < 0 ? Mathf.CeilToInt(targetDepletedfloat) : Mathf.FloorToInt(targetDepletedfloat), 0, resourceDepletion[i].amount);
       //
       //     if (targetDepleted > resourceDepletion[i].currentDepleted)
       //     {
       //         int resourcesRemaining = ShipInstanceManager.instance.shipResourceContainer.RemoveResourcesOneByOne(resourceDepletion[i].resource, targetDepleted - resourceDepletion[i].currentDepleted);
       //         if (resourcesRemaining == 0)
       //         {
       //             Debug.Log("Removing " + resourceDepletion[i].resource.itemName + " with no remainder");
       //     
       //             resourceDepletion[i].currentDepleted = targetDepleted;
       //         }
       //         else
       //         {
       //             Debug.Log("Removing " + resourceDepletion[i].resource.itemName + " with remainder: " + resourcesRemaining);
       //     
       //             resourceDepletion[i].currentDepleted = targetDepleted - resourcesRemaining;
       //             buildingHasProgressed = false;
       //         }
       //     }
       //     else if(targetDepleted < resourceDepletion[i].currentDepleted)
       //     {
       //         Debug.Log("Adding resources " + (resourceDepletion[i].currentDepleted - targetDepleted) + " " + resourceDepletion[i].resource.itemName);
       //     
       //         ShipInstanceManager.instance.shipResourceContainer.AddResources(resourceDepletion[i].resource, resourceDepletion[i].currentDepleted - targetDepleted);
       //         resourceDepletion[i].currentDepleted = targetDepleted;
       //     }
       //     Debug.Log("TargetDepletion: " + targetDepleted + "    actually depleted: " + resourceDepletion[i].currentDepleted + "    depletion % :" + ((float)resourceDepletion[i].currentDepleted / resourceDepletion[i].amount));
       //     lastFrameBuildPrecentage += (float)resourceDepletion[i].currentDepleted / resourceDepletion[i].amount;
       // }
       // lastFrameBuildPrecentage /= resourceDepletion.Count;
       // double buildTimeElapsedThisFrame = (ConstructionTime * (lastFrameBuildPrecentage - buildCompletionPercentage));//TIME IT TOOK TO BUILD AS MUCH AS IT COULD LAST FRAME
       //
       // Debug.Log("lastFrameBuildPrecentage: " + lastFrameBuildPrecentage + "          buildCompletionPercentage: " + buildCompletionPercentage + "      buildDelta: " + (lastFrameBuildPrecentage - buildCompletionPercentage));
       // double remainingBuildTime = (ConstructionTime * (1 - buildCompletionPercentage));
       // Debug.Log("DELTA TIME: " + WorldTimeManager.deltaTime + "    WORLD TIME: " + WorldTimeManager.currentTime + "       ELAPSED TIME - (LAST FRAME): " + cantBuildWaitTime + "       ELAPSED TIME - (THIS FRAME): " + (cantBuildWaitTime + WorldTimeManager.deltaTime));
       //
       // //if (buildingHasProgressed) {
       // buildCompletionPercentage = (float)lastFrameBuildPrecentage;
       // //Debug.Log("AAAAAAAA buildTimeeeeee: " + buildTimeeeeee + "      max: " + max + "    DT: " + WorldTimeManager.deltaTime);
       // if (buildingHasProgressed)
       // {
       //     if (remainingBuildTime - buildTimeElapsedThisFrame < WorldTimeManager.deltaTime)
       //     {
       //         cantBuildWaitTime += buildTimeElapsedThisFrame;
       //     }
       //     else
       //     {
       //         cantBuildWaitTime += WorldTimeManager.deltaTime;// System.Math.Min(buildTimeeeeee, lastDelta);
       //
       //     }
       // }
       // //}
       // Debug.Log("buildTimeElapsedThisFrame: " + buildTimeElapsedThisFrame);
       // Debug.Log("remainingBuildTime: " + remainingBuildTime);
       // Debug.Log("BUILD DELTA: " + (1 - (lastFrameBuildPrecentage - buildCompletionPercentage)));
       //
       // lastDelta = WorldTimeManager.deltaTime;
       //
       //
       //
        if (buildCompletionPercentage >= 1)
        {
            if (powerComponent)
            {
                powerComponent.isOn = true;
            }
            if (UnderConstructionObject.activeSelf == true)
            {
                UnderConstructionObject.SetActive(false);
                
            }
        }
        else 
        {
            if (powerComponent)
            {
                powerComponent.isOn = false;
            }
            if (UnderConstructionObject.activeSelf == false)
            {
                UnderConstructionObject.SetActive(true);
                
            }
        }
    }

    public void SetBlockBuildingArea(Vector3 position, Vector3 size)
    {
        underConstructionNoBuildArea.size = size;
        underConstructionNoBuildArea.center = position;
    }

    public void SetZonePoints(Vector3[] points, float width, Material zoneMaterial = null)
    {
        underConstructionLine.material = zoneMaterial;
        underConstructionLine.textureMode = LineTextureMode.Tile;
        underConstructionLine.loop = true;
        underConstructionLine.endWidth = width;
        underConstructionLine.startWidth = width;
        underConstructionLine.positionCount = points.Length;
        underConstructionLine.SetPositions(points);
    }

    public void SetBuildTime(double buildTime, WorldTimeManager._time timeType)
    {
        creationTime = WorldTimeManager.currentTime;
        ConstructionTime = WorldTimeManager.ConvertTime(timeType, WorldTimeManager._time.seconds, buildTime);
        finishTime = creationTime + WorldTimeManager.ConvertTime(timeType, WorldTimeManager._time.seconds, buildTime);
    }
}
