﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newResource", menuName = "ResourceItem")]
public class ResourceItem : ScriptableObject
{
    

    public string itemName = "resources short name";
    public string description = "the description for this resource";
    public Sprite icon = null;// the icon for the resuorce
    public List<constructionRequirementsClass> constructionRequirements;
    public double constructionTime;
    public WorldTimeManager._time constructionTimeType;
}
[System.Serializable]
public class constructionRequirementsClass
{
    public ResourceItem requiredResource;
    public int requiredAmount;
}
