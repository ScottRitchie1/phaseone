﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour {

    public SkinnedMeshRenderer skin;

    public List<Transform> ragDollBones;

    public List<Transform> destroyOnRagDoll;

    private void Start()
    {
        if(skin == null)
        {
            skin = GetComponent<SkinnedMeshRenderer>();
        }
    }

    public void InitilizeRagdoll()
    {
        foreach (Transform t in ragDollBones)
        {
            t.GetComponent<Rigidbody>().isKinematic = true;
            t.GetComponent<Collider>().enabled = false;
            t.gameObject.layer = LayerMask.NameToLayer("RagDollBones");
        }
    }

    public void EnableRagDoll(Vector3 force)
    {
        foreach (Transform t in ragDollBones)
        {
            Rigidbody trb = t.GetComponent<Rigidbody>();
            trb.isKinematic = false;
            trb.velocity = force;
            t.GetComponent<Collider>().enabled = true;
        }

        foreach(Transform t in destroyOnRagDoll)
        {
            Destroy(t.gameObject);
        }
    }
}
