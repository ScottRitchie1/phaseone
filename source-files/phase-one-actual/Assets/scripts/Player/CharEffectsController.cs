﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharEffectsController : MonoBehaviour {

    CharJetPack jetpack;
    public ParticleSystem[] smokeTrailParticles = new ParticleSystem[2];
    public ParticleSystem heatParticle;
    public List<ParticleSystem> mainEngineParticles;
    public Light[] engineLights = new Light[2];
    public float engineLightFlickerRate = 1;
    [Range(0,1)]
    public float enginelightFlickerAmount = 1;
    float engineLightOriginalIntensity;
    float startEngineLightIntensity;
    float targetEngineLightIntensity;
    bool mainEnginesOn = false;
    bool smokeTrailsOn = false;
	void Start () {
        jetpack = CharacterInstanceManager.instance.jetPack;
        foreach (ParticleSystem ps in mainEngineParticles)
        {
            ps.Stop();
        }
        mainEnginesOn = false;
        foreach(ParticleSystem ps in smokeTrailParticles)
        {
            ps.Stop();
        }

        if (engineLights.Length > 0)
        {
            engineLightOriginalIntensity = engineLights[0].intensity;
            startEngineLightIntensity = engineLightOriginalIntensity;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (jetpack)
        {
            if (jetpack.isThrusting)
            {
                if (smokeTrailsOn == false)
                {
                    foreach (ParticleSystem ps in smokeTrailParticles)
                    {
                        ps.Play();
                    }
                    smokeTrailsOn = true;
                }

                if (mainEnginesOn == false)
                {
                    foreach(ParticleSystem ps in mainEngineParticles)
                    {
                        ps.Play();
                    }
                    mainEnginesOn = true;
                }

                if (engineLights.Length > 0)
                {

                    if(engineLights[0].intensity == targetEngineLightIntensity)
                    {
                        targetEngineLightIntensity = Random.Range(engineLightOriginalIntensity * enginelightFlickerAmount, engineLightOriginalIntensity);
                        startEngineLightIntensity = engineLights[0].intensity;
                        if(startEngineLightIntensity == 0)
                        {
                            startEngineLightIntensity = engineLightOriginalIntensity;
                        }
                    }

                    float curIntensity = Mathf.MoveTowards(engineLights[0].intensity, targetEngineLightIntensity, startEngineLightIntensity / engineLightFlickerRate * Time.deltaTime);
                    foreach(Light l in engineLights)
                    {
                        l.intensity = curIntensity;
                    }
                }
            }
            else if(jetpack.isThrusting == false)
            {
                if (smokeTrailsOn == true)
                {
                    foreach (ParticleSystem ps in smokeTrailParticles)
                    {
                        ps.Stop();
                    }
                    smokeTrailsOn = false;
                }

                if (mainEnginesOn)
                {
                    foreach (ParticleSystem ps in mainEngineParticles)
                    {
                        ps.Stop();
                    }
                    mainEnginesOn = false;
                }

                if(engineLights.Length > 0 && engineLights[0].intensity > 0)
                {
                    targetEngineLightIntensity = 0;
                    foreach (Light l in engineLights)
                    {
                        l.intensity = 0;
                    }
                    
                }
            }
        }
	}
}
