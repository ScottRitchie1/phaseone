﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "newBlueprint", menuName = "StructureBluePrint")]
public class StructureBluePrint : ScriptableObject
{
    [FormerlySerializedAs("Name")]
    [SerializeField]
    public string structureName = "resources short name";
    public string description = "the description for this resource";
    public Sprite icon = null;// the icon for the resuorce
    [Header("REQUIREMENTS")]
    public List<constructionRequirementsClass> constructionRequirements;
    public double constructionTime;
    public WorldTimeManager._time constructionTimeType;
    [Header("CONSTRUCTION")]
    public GameObject constructionPrefab;
    public bool structureSnapsToIncline;
    [Header("GROUND AREA")]
    public Mesh constructionHologram;
    public Vector2 constructiondAreaSize;
    public Vector3 constructionAreaOffset;
    public float maxConstructionIncline;
    [Header("NO COLLIDE VOLUME")]
    public Vector3 construcutionVolumeSize;
    public Vector3 constructionVolumeOffset;
    [Header("UI")]
    public Vector2 buildUIOffSet;
}


