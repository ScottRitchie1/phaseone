﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightFireFlys : MonoBehaviour {

    ParticleSystem ps;

    public Vector2 activeTime;
    public bool palsdasd = false;
    ParticleSystem.MainModule psMain;
    ParticleSystem.MinMaxCurve startLifeTime;
    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
        psMain = ps.main;
        startLifeTime = psMain.startLifetime;
    }

    // Update is called once per frame
    void Update () {
        palsdasd = ps.isEmitting;
        if(DayCycleManager.instance.weight > activeTime.x && DayCycleManager.instance.weight < activeTime.y)
        {
            if (ps.isPlaying)
            {
                ps.Stop();
            }
        }
        else if (ps.isPlaying == false)
        {
            ps.Play();
        }
    }
}
