﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ShipTerminalUI : MonoBehaviour {
    public PowerPlantComponent shipPowerPlant;
    respawn respawnInstance;

    public RectTransform resourceSlotContainter;
    public RectTransform resourceElement;
    [Space]
    public Text nextCloneYearsText;
    public Text nextCloneDaysText;
    public Text NextCloneMinutesText;
    [Space]
    public Slider powerBar;
    Image powerBarFillRect;
    public Text powerCapacity;
    public Text powerstatus;
    public Text powerYearsText;
    public Text powerDaysText;
    public Text powerMinutesText;
    [Space]
    public Text clonePercentage;
    [Space]
    public Color styleRed;
    public Color styleGreen;
    // Use this for initialization
    public class Prototype_ResourceSlot
    {
        public ResourceContainerComponent.ResourceSlot slot;
        public Text nameSlotText;
        public Text amountSlotText;

        public void UpdateSlot()
        {
            nameSlotText.text = slot.Resource.name;
            amountSlotText.text = slot.Amount.ToString();
        }
    }

    [SerializeField]
    List<Prototype_ResourceSlot> slotList = new List<Prototype_ResourceSlot>();

    private void Start()
    {
        ShipInstanceManager.instance.shipResourceContainer.resourceListChanged += UpdateSlots;
        UpdateSlots();
        respawnInstance = FindObjectOfType<respawn>();
        powerBarFillRect = powerBar.fillRect.GetComponent<Image>();
    }

    private void OnDisable()
    {
        ShipInstanceManager.instance.shipResourceContainer.resourceListChanged -= UpdateSlots;
    }

    private void Update()
    {
        string timeString = WorldTimeManager.TimeString(respawnInstance.nextCloneBirthTime - WorldTimeManager.currentTime);
        string[] stringArr = Regex.Split(timeString, @"\{.*?\}");
        //TO FIX REGEX POPULATES AN EMPTY FIRST ENTRY INTO THE LST SO ICREAMENTED EVERTHING BY 1
        if (nextCloneYearsText && stringArr.Length > 0) nextCloneYearsText.text = stringArr[1];
        if (nextCloneDaysText && stringArr.Length > 1) nextCloneDaysText.text = stringArr[2];
        if (NextCloneMinutesText && stringArr.Length > 3) NextCloneMinutesText.text = stringArr[4];


        clonePercentage.text = Mathf.Round(respawnInstance.GetCloneCompletePercentage()*100).ToString() + "%";
        if (shipPowerPlant)
        {
            powerCapacity.text = System.Math.Round(shipPowerPlant.currentCapacity).ToString() + "/" + shipPowerPlant.maxCapacity.ToString();
            powerBar.value = shipPowerPlant.GetShipPowerPercentage();
            if (shipPowerPlant.IsDepleting())
            {
                powerBarFillRect.color = styleRed;
                powerstatus.text = "Depletes In:";
                powerstatus.color = styleRed;
                //text.text += "% \n DEPLETES IN: " + string.Format(WorldTimeManager.TimeString(shipPowerPlant.GetDepletionTime()), "Y:", "D:", " H:", " M:", " S:");
                timeString = WorldTimeManager.TimeString(shipPowerPlant.GetDepletionTime());
                stringArr = Regex.Split(timeString, @"\{.*?\}");
                //TO FIX REGEX POPULATES AN EMPTY FIRST ENTRY INTO THE LST SO ICREAMENTED EVERTHING BY 1
                if (powerYearsText && stringArr.Length > 0) powerYearsText.text = stringArr[1];
                if (powerDaysText && stringArr.Length > 1) powerDaysText.text = stringArr[2];
                if (powerMinutesText && stringArr.Length > 3) powerMinutesText.text = stringArr[4];
            }
            else
            {
                powerBarFillRect.color = styleGreen;
                powerstatus.text = "Charged In:";
                powerstatus.color = styleGreen;
                //text.text += "% \n CHARGED IN: " + string.Format(WorldTimeManager.TimeString(shipPowerPlant.GetChargeTime()), "Y:", "D:", " H:", " M:", " S:");
                timeString = WorldTimeManager.TimeString(shipPowerPlant.GetChargeTime());
                stringArr = Regex.Split(timeString, @"\{.*?\}");
                //TO FIX REGEX POPULATES AN EMPTY FIRST ENTRY INTO THE LST SO ICREAMENTED EVERTHING BY 1
                if (powerYearsText && stringArr.Length > 0) powerYearsText.text = stringArr[1];
                if (powerDaysText && stringArr.Length > 1) powerDaysText.text = stringArr[2];
                if (powerMinutesText && stringArr.Length > 3) powerMinutesText.text = stringArr[4];
            }
        }
    }

    void UpdateSlots()
    {
        List<Prototype_ResourceSlot> toSearch = new List<Prototype_ResourceSlot>(slotList);

        foreach (ResourceContainerComponent.ResourceSlot slot in ShipInstanceManager.instance.shipResourceContainer.ResourceList)
        {
            bool foundUISlot = false;
            foreach (Prototype_ResourceSlot uiSlot in toSearch)
            {
                if (uiSlot.slot == slot)
                {
                    foundUISlot = true;
                    toSearch.Remove(uiSlot);
                }

                if (foundUISlot == true)
                {
                    break;
                }
            }

            if (foundUISlot == false)
            {
                Prototype_ResourceSlot newSlot = new Prototype_ResourceSlot();
                newSlot.slot = slot;
                newSlot.slot.UpdateSlot += newSlot.UpdateSlot;
                RectTransform slotObj = Instantiate(resourceElement, resourceSlotContainter.position, resourceSlotContainter.rotation, resourceSlotContainter);
                newSlot.nameSlotText = slotObj.GetChild(0).GetComponent<Text>();
                newSlot.amountSlotText = slotObj.GetChild(1).GetComponent<Text>();
                newSlot.UpdateSlot();

                slotObj.localScale = Vector3.one;

                slotList.Add(newSlot);

            }
        }

        foreach (Prototype_ResourceSlot uiSlot in toSearch)
        {
            Destroy(uiSlot.nameSlotText.gameObject);
            slotList.Remove(uiSlot);
        }
    }
}
