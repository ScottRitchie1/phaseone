﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInstanceManager : MonoBehaviour {
    public static ShipInstanceManager instance;

    public ResourceContainerComponent shipResourceContainer;
    public PowerPlantComponent shipPowerPlant;
    public PowerComponent shipPower;
    public Transform centerPivot;
	// Use this for initialization
	void Awake () {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("Ship instance already set."); }

        shipResourceContainer = GetComponent<ResourceContainerComponent>();
        shipPowerPlant = GetComponent<PowerPlantComponent>();
        shipPower = GetComponent<PowerComponent>();
    }


}
