﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGlowScript : MonoBehaviour {

    public Vector2 lifeTimeWeights;
    public Vector2 lightNightrange;
    public float lerpWeightAmount = 0.05f;
    public float dayEmmisionCap = 0.1f;
    public Light light;
    public Renderer[] emmisionMat;

    float startIntensity;
    float nightWeight;
    Color startEmmision;
    // Use this for initialization
    void Start()
    {
        if (light)
        {
            startIntensity = light.intensity;
        }

        if (emmisionMat.Length > 0)
        {
            startEmmision = emmisionMat[0].material.GetColor("_EmissionColor");
        }
    }

    // Update is called once per frame
    void Update()
    {
        nightWeight = DayCycleManager.instance.weight > (lightNightrange.x + lightNightrange.y) / 2 ?
            Mathf.InverseLerp(lightNightrange.y, lightNightrange.y + lerpWeightAmount, DayCycleManager.instance.weight) :
            Mathf.InverseLerp(lightNightrange.x, lightNightrange.x - lerpWeightAmount, DayCycleManager.instance.weight);
        float curWeight = nightWeight;
        if(light)
            light.intensity = startIntensity * curWeight;
        for (int i = 0; i < emmisionMat.Length; i++)
        {
            emmisionMat[i].material.SetColor("_EmissionColor", startEmmision * Mathf.Clamp(curWeight, dayEmmisionCap, 1));

        }
    }
}
