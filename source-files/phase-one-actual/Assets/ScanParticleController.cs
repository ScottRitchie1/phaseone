﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanParticleController : MonoBehaviour {

    public ParticleSystem p;
    public ParticleSystem.Particle[] particles;
    public Transform Target;
    public AnimationCurve trackingWeight = new AnimationCurve();


    void Start()
    {
        if (p == null)
        p = GetComponent<ParticleSystem>();
    }


    void LateUpdate()
    {

        particles = new ParticleSystem.Particle[p.particleCount];

        p.GetParticles(particles);
        for (int i = 0; i < particles.GetUpperBound(0); i++)
        {
            float ForceToAdd = (particles[i].startLifetime - particles[i].remainingLifetime) * (Vector3.Distance(Target.position, particles[i].position));
            //Debug.DrawRay (particles [i].position, (Target.position - particles [i].position).normalized * (ForceToAdd/10));

            particles[i].velocity = Vector3.Lerp(particles[i].velocity, (Target.position - particles[i].position).normalized * ForceToAdd, Time.deltaTime * trackingWeight.Evaluate(1-Mathf.InverseLerp(0, particles[i].startLifetime, particles[i].remainingLifetime)));
            //particles[i].velocity = Vector3.Lerp(particles[i].velocity, (transform.InverseTransformPoint(Target.position) - particles[i].position).normalized, Time.deltaTime * velocityChange);

            //particles[i].velocity = Vector3.right;

            //particles [i].position = Vector3.Lerp (particles [i].position, transform.InverseTransformPoint(Target.position), Time.deltaTime / 2.0f);

        }

        p.SetParticles(particles, particles.Length);

    }
}
