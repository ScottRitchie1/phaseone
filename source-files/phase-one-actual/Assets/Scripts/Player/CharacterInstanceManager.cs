﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInstanceManager : MonoBehaviour {

    public static CharacterInstanceManager instance;

    public CharController controller;
    public CharColliderController colliderController;
    public CharJetPack jetPack;
    public CharBuildController buildController;
    public InteractionController interactionController;
    public Rigidbody rb;

    public CharAnimator animatorController;

    private void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("CharacterInstanceManager instance already set."); }

        controller = GetComponent<CharController>();
        colliderController = GetComponent<CharColliderController>();
        buildController = GetComponent<CharBuildController>();
        interactionController = GetComponent<InteractionController>();
        jetPack = GetComponent<CharJetPack>();
        rb = GetComponent<Rigidbody>();
        animatorController = GetComponentInChildren<CharAnimator>();
    }


}
