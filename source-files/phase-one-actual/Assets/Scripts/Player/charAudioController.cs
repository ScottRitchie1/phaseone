﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(Animator))]
public class charAudioController : MonoBehaviour {

    
    [System.Serializable]
    public class footStepDefinition
    {
        public AudioClip stepSound;
        public Vector2 pitchRange;
        [Range(0, 32)]
        public int surfaceTypeIndex = 0;
    }

    public AudioSource mainAudioSource;

    [Header("SUIT")]
    public Vector2 suitRuffleVolumeRange;
    public float suitRufflePitch = 1;
    public AudioClip suitRuffleSound;
    

    [Header("JETPACK")]
    public AudioClip jetpackSound;

    [Header("FOOT STEPS")]
    public List<footStepDefinition> footSteps = new List<footStepDefinition>();
    public Vector2 footStepVolumeRange;
    public AudioSource footStepAudioSource;

    Animator anim;
    CharController charController;

    float prevFootStep;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        charController = CharacterInstanceManager.instance.controller;

        if (footStepAudioSource == null)
            footStepAudioSource = GetComponent<AudioSource>();
        

        if(!(footSteps.Count > 0))
        {
            Debug.LogError("FootSteps must have default entry.");
            this.enabled = false;
        }

        if(footStepAudioSource == null)
        {
            Debug.LogError("no foot step audio source.");
            this.enabled = false;
        }

        if (mainAudioSource == null)
        {
            Debug.LogError("no main audio source.");
        }
        else
        {
            mainAudioSource.pitch = suitRufflePitch;
            mainAudioSource.clip = suitRuffleSound;
            mainAudioSource.loop = true;
            mainAudioSource.Play();
        }

        if (charController == null)
        {
            Debug.LogError("FootSteps controller cant find charController.");
            this.enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (charController.isGrounded)
        {
            if (anim)
            {
                float curFootStep = Mathf.Round(anim.GetFloat("footStep") * 100) / 100;
                if (prevFootStep < 0)
                {
                    if (curFootStep >= 0)
                    {
                        PlayFootStep(0);
                    }
                }
                else if (prevFootStep >= 0)
                {
                    if (curFootStep < 0)
                    {
                        PlayFootStep(0);
                    }
                }
                prevFootStep = curFootStep;
            }

            if (mainAudioSource)
            {
                if (mainAudioSource.clip != suitRuffleSound)
                {
                    mainAudioSource.clip = suitRuffleSound;
                    mainAudioSource.Play();
                }
                mainAudioSource.volume = Mathf.InverseLerp(suitRuffleVolumeRange.x, suitRuffleVolumeRange.y, charController.transform.InverseTransformDirection(charController.rigidbody.velocity).z);
            }
        }
        else
        {

            
            if(mainAudioSource)
            {
                if (mainAudioSource.clip != jetpackSound)
                {
                    mainAudioSource.clip = jetpackSound;
                    mainAudioSource.volume = 0;
                    mainAudioSource.Play();
                }
                if (CharacterInstanceManager.instance.jetPack.isThrusting)
                {
                    mainAudioSource.volume = Mathf.Lerp(mainAudioSource.volume, 1, Time.deltaTime*10);
                    
                }
                else
                {
                    mainAudioSource.volume = Mathf.Lerp(mainAudioSource.volume, 0, Time.deltaTime * 10);
                }
            }
        }

    }

    void PlayFootStep(int index)
    {
        footStepAudioSource.pitch = Random.Range(footSteps[index].pitchRange.x, footSteps[index].pitchRange.y);
        footStepAudioSource.PlayOneShot(footSteps[index].stepSound);
        footStepAudioSource.volume = Mathf.InverseLerp(footStepVolumeRange.x, footStepVolumeRange.y, charController.transform.InverseTransformDirection(charController.rigidbody.velocity).z);
    }
}
