﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour {

    public static InteractionController instance;

    CamController cam;

    public float eyeHeight;

    public float interactionRange;
    public float interactionAngle;

    public bool isHarvesting = false;
    int layerMask;

    public GameObject harvestEffect;
    public Material harvestMaterial;
    public ParticleSystem ps;
    public AudioSource harvestAudio;
    public AudioClip harvestSound;
    public AudioClip scanSound;
    // Use this for initialization
    void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("InteractionController instance already set."); }
    }

    void Start () {
        ps.transform.SetParent(null);
        layerMask = LayerMask.GetMask("InteractableEntity");
        cam = CamController.instance;
        harvestAudio.loop = true;

    }

    // Update is called once per frame
    void Update () {
        if (CharacterInstanceManager.instance.buildController.inBuildMode == false) {
            if (Input.GetKeyDown(KeyCode.Q) || Input.GetMouseButtonDown(0))
            {
                InteractableEntityBase target = GetInteractionTarget(cam.transform.forward, InteractionPosition());

                if (isHarvesting == false && target != null && (TutorialManager.instance.allowHarvest) && ScanUI.instance.isScanning == false)
                {
                    if (target.UseDefaultHarvest())
                    {
                        StartCoroutine("harvestCoroutine", target);
                    }
                    else
                    {
                        target.OnHarvest();
                    }
                }
            } else if (Input.GetKeyDown(KeyCode.E) || Input.GetMouseButtonDown(1))
            {
                InteractableEntityBase target = GetInteractionTarget(cam.transform.forward, InteractionPosition());

                if (target != null)
                {
                    target.OnScan();
                }
            }
        }
    }

    public InteractableEntityBase GetInteractionTarget(Vector3 targetingDir, Vector3 targetingPos)
    {
        Collider[] hitInteractables;

        hitInteractables = GetHitInteractablesInInteractionRange();

        if(hitInteractables.Length > 0)
        {
            int targetIndex = -1;
            float targetAngle = 0;

            for(int i = 0; i < hitInteractables.Length; i++)
            {
                float tempAngle = Vector3.Angle(hitInteractables[i].transform.position - targetingPos, targetingDir);

                if(targetIndex == -1 || tempAngle < targetAngle)
                {
                    targetIndex = i;
                    targetAngle = tempAngle;
                }
            }

            if(targetAngle <= interactionAngle)
            {
                return hitInteractables[targetIndex].GetComponent<InteractableEntityBase>();
            }
        }

        return null;
    }

    public Collider[] GetHitInteractablesInInteractionRange()
    {
        return Physics.OverlapSphere(transform.position, interactionRange, layerMask, QueryTriggerInteraction.Ignore);
    }

    Vector3 InteractionPosition()
    {
        return transform.position + transform.up * eyeHeight;
    }

    IEnumerator harvestCoroutine(InteractableEntityBase target)
    {
        if (target.GetComponent<NonHarvestableEntity>())
        {
            yield break;
        }
        isHarvesting = true;
        harvestAudio.clip = harvestSound;
        harvestAudio.Play();
        transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
        MeshRenderer[] meshRenderers = target.GetComponentsInChildren<MeshRenderer>(false);
        List<Material> originalMats = new List<Material>();
        harvestMaterial.SetTexture("_MainTex", meshRenderers[0].material.mainTexture);
        foreach(MeshRenderer mr in meshRenderers)
        {
            originalMats.Add(mr.material);
            mr.material = harvestMaterial;
        }

        ps.transform.position = meshRenderers[0].transform.position;
        ps.transform.rotation = meshRenderers[0].transform.rotation;

        ps.gameObject.SetActive(true);
        ParticleSystem.ShapeModule shape = ps.shape;
        shape.mesh = meshRenderers[0].GetComponent<MeshFilter>().mesh;
        shape.scale = meshRenderers[0].transform.lossyScale;
        foreach (ParticleSystem ps in ps.GetComponentsInChildren<ParticleSystem>())
        {
            shape = ps.shape;
            shape.mesh = meshRenderers[0].GetComponent<MeshFilter>().mesh;
            shape.scale = meshRenderers[0].transform.lossyScale;
        }

        float finishTime = Time.time + 5;
        harvestEffect.SetActive(true);
        CharacterInstanceManager.instance.controller.canMove = false;
        CharacterInstanceManager.instance.animatorController.anim.SetBool("Harvest", true);
        while (Time.time < finishTime)
        {
            harvestEffect.transform.LookAt(meshRenderers[0].transform.position);
            float value = Mathf.InverseLerp(finishTime - 5, finishTime, Time.time);
            foreach(MeshRenderer mr in meshRenderers)
            {
                mr.material.SetFloat("_cutoff", value);
            }
            yield return null;
        }
        CharacterInstanceManager.instance.controller.canMove = true;
        CharacterInstanceManager.instance.animatorController.anim.SetBool("Harvest", false);
        for(int i = 0; i < meshRenderers.Length; i++)
        {
            meshRenderers[i].material = originalMats[i];
        }
        harvestAudio.Stop();

        harvestEffect.SetActive(false);
        ps.gameObject.SetActive(false);
        target.OnHarvest();
        isHarvesting = false;

    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(InteractionPosition(), (cam? cam.transform.forward : transform.forward) * interactionRange);

        Gizmos.DrawWireSphere(InteractionPosition(), interactionRange);

        if (cam)
        {
            Collider[] inRange = GetHitInteractablesInInteractionRange();
            InteractableEntityBase target = GetInteractionTarget(cam.transform.forward, InteractionPosition());

            foreach (Collider col in inRange)
            {
                Gizmos.color = Color.green;
                if (target && col.gameObject == target.gameObject)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawLine(col.transform.position, InteractionPosition());

                }
                Gizmos.DrawRay(col.transform.position, Vector3.up * 10);
            }

        }

    }
#endif
}
