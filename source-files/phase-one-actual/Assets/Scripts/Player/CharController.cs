﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// FIXXXXXXXX
/// implement step height
/// max walkable angle
/// use jetpack after falling - currently have to press space twice to work
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class CharController : MonoBehaviour {
    [System.Serializable]
    public class controllerState
    {
        public float moveSpeed;// the max rate at which the character moves
        public float turnSpeed;// the max rate at which the character turns
        [Space]
        public float acceleration;// the rate at which the character can reach their max move speed
        public float deacceleration;// the rate at which the character returns to no movement speed
        [Range(0.0f, 1.0f)]
        public float smoothingFactor;// the amount our character's horizontal movement is smoothed - this doesnt effect forwards and backwards movement - change acceleration for this
        [Range(0.0f,1.0f)]
        public float facingToTargetDirectionWeight;// the direction weight our character should move, either towards our facing direction OR towards our inpuit direction OR a blend of both
        [Space]
        public float turnDampingRecovery;// the amount our character can return to max turn speed after initiating a turn
        [Range(0.0f, 1.0f)]
        public float turnDampingChangeIncrease;// the amount our characters turning is damped each time we change direction
    }

    public static CharController instance; // instance to our character
    Rigidbody rb;// holds ourt rigid body
    public Rigidbody rigidbody
    {
        get
        {
            return rb;
        }
    }
    CamController cam;// holds our camera

    [HideInInspector]
    public controllerState currentControllerState;//our current state
    public bool canMove = true;

    [Header("State Controllers")]
    public controllerState groundedControllerState;// movement values for grounded

    [Space]
    public controllerState airTimeControllerState;//movement values while not grounded

    [Header("Jumping")]
    public float jumpForce;// how high we just
    public float velocityDampeningOnLanded;// the amount our velocity is damped when we land

    
    [Header("Grounded")]
    RaycastHit m_groundedHit;// the place we hit the ground
    float m_groundedCheckRadius;// the radious of our grounded check - set to 90% of the cap collider radious at start
    [HideInInspector]
    public float curGroundedYPos; // thbis holds the current lerping y pos for our character to target when grounded
    public float groundedGravity = 20f; // the rate our character moves towards the ground - sicne we disable physics gravity for more control
    const float GROUNDED_VERTICAL_VELOCITY_LERP_TO_ZERO_RATE = 5; // the rate our characters natural vertical velocity returns to 0 - this is to stop the character floating

    float distToGround = 0;
    public float DistToGround{get{return distToGround;}}

    public bool isGrounded = true;// if we are on the ground or not
    public float groundedStepHeight = 1f;// the downwards step height for natural gravity
    [Space(5)]
    public float maxRegroundAngle;// the max angle we can reground while we are in the air
    public float maxWalkAngle;
    [Range (0,1)]
    public float overWalkAngleDamping;
    public AnimationCurve moveAngleSpeedWeightCurve;
    [Space]
    public LayerMask GroundedCheckLayerMask;

    

    Vector3 m_inputs;//wasd inputs

    //public Vector3 Inputs
    //{
    //    get
    //    {
    //        return m_inputs;
    //    }
    //}

    bool m_shouldJumpNextFixedUpdate;//space input to jump in fixed update

    Vector3 m_targetDirection;//the target direction based off our input direction relitive to the camera direction

    float m_curTurnDamping; // our current turn damping - this controls the smoothing of changing turn angles
    float m_curMoveSpeed;// our current movement speed - this controls the acceleration and deacceleration
    float m_prevTargetDeltaAngle = 0;// this stores the previous delta angle between our forward and aur target direction - used to determine if the character changes turn direction

    #region VARS - Delegates
    public delegate void OnJumpDelegate();
    public OnJumpDelegate onJump;

    public delegate void OnGroundedDelegate();
    public OnGroundedDelegate onGrounded;
    #endregion

    private void Awake()
    {
        if (instance == null){instance = this;}// set instance
        else{Debug.LogError("CharController instance already set.");}

        rb = GetComponent<Rigidbody>();// grab rigid body
        if(GetComponent<CharColliderController>() && GetComponent<CharColliderController>().lowerCollider)
            m_groundedCheckRadius = GetComponent<CharColliderController>().lowerCollider.radius;// set grandoed check sphere
        else
            m_groundedCheckRadius = GetComponent<CapsuleCollider>().radius;// set grandoed check sphere

        m_targetDirection = transform.forward;// set forwards
    }

    private void Start () {
        cam = CamController.instance;// grab camera
        OnGrounded();// default to grounded state
    }

    private void OnEnable()
    {
        OnUngrounded();
    }

    private void Update()
    {
        UpdateInputs();// update inputs

        if (m_inputs.magnitude > 0)// only recalculate if we are using inputs
            m_targetDirection = Quaternion.LookRotation(cam.GetCameraDirection()) * m_inputs;// calculates the relative target direction based off our cameras direction and our inputs


        if (isGrounded)// if we are grounded and not preparing to jump
            UpdateGrounded();
        else// if we not grounded OR we are preparing to jump
            UpdateNotGrounded();

        UpdatePositionMovement();// update movement
        UpdateRotationMovement();// update rotation
    }

    private void FixedUpdate()
    {

        //pm.dynamicFriction = Mathf.Abs(groundedFriction * (1-m_inputs.magnitude));//only apply friction when we arnt moving with inputs
        //pm.staticFriction = pm.dynamicFriction;
        //Debug.Log(GetComponent<Collider>().material.dynamicFriction);

        float signedAngle = Vector3.SignedAngle((isGrounded? Vector3.Cross(transform.right, m_groundedHit.normal) : transform.forward), transform.forward, transform.right);


        //Calculate the force required to move our character
        Vector3 ourMoveForce = ((Vector3.Lerp(transform.forward, m_targetDirection, currentControllerState.facingToTargetDirectionWeight) * m_curMoveSpeed) - rb.velocity) * currentControllerState.smoothingFactor
             * (isGrounded ? Mathf.Clamp01(moveAngleSpeedWeightCurve.Evaluate(Mathf.InverseLerp(0, maxWalkAngle, GetCurrentStepAngle()))) : 1);// scale the speed based on the angle we are moving at
        ourMoveForce.y = 0;//we dont want to add force on the y axis

       
        if (signedAngle < maxWalkAngle)
        {
            if (isGrounded)// if we are grouned use artificial gravity
            {
                //lerp back to 0 velocity to stop overing - this is required to still use velocity to naturally beable to move up inclines
                rb.velocity = new Vector3(rb.velocity.x, Mathf.MoveTowards(rb.velocity.y, 0, Time.fixedDeltaTime * GROUNDED_VERTICAL_VELOCITY_LERP_TO_ZERO_RATE), rb.velocity.z);
                // move to the ground position
                curGroundedYPos = Mathf.Lerp(transform.position.y, m_groundedHit.point.y, Time.fixedDeltaTime * groundedGravity);
                rb.MovePosition(new Vector3(transform.position.x, curGroundedYPos, transform.position.z));
            }
            rb.AddForce(ourMoveForce, ForceMode.VelocityChange);//move our character
        }
        else
        {
            rb.AddForce(-rb.velocity * overWalkAngleDamping, ForceMode.VelocityChange);//move our character
        }


        if (m_shouldJumpNextFixedUpdate)//jump
        {
            OnJump();
        }

    }

    #region SUB UPDATES
    private void UpdateInputs()
    {
        if (canMove)
        {
            m_inputs = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;// gets the move input
            m_shouldJumpNextFixedUpdate = (m_shouldJumpNextFixedUpdate || isGrounded && Input.GetKeyDown(KeyCode.Space));// gets the jump input
        }
        else
        {
            m_inputs = Vector3.zero;
            m_shouldJumpNextFixedUpdate = false;
        }
    }

    private void UpdateGrounded()
    {
        //sets grounede to false if: we are actually not grounded, we are jumping or our step angle is above the required
        if (!Physics.SphereCast(transform.position + new Vector3(0, m_groundedCheckRadius * 1.1f + 0.5f, 0), m_groundedCheckRadius * 1.1f, -Vector3.up, out m_groundedHit, groundedStepHeight + 0.5f, GroundedCheckLayerMask, QueryTriggerInteraction.Ignore)
            || m_shouldJumpNextFixedUpdate )//|| GetCurrentStepAngle() > maxUngroundAngle)
        {
            OnUngrounded();
            return;
        }

        RaycastHit hit;
        if(Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, 1 + groundedStepHeight * 2, GroundedCheckLayerMask, QueryTriggerInteraction.Ignore))
        {
            m_groundedHit.normal = hit.normal;
        }

    }

    private void UpdateNotGrounded()
    {
        distToGround = Mathf.Abs(transform.position.y - m_groundedHit.point.y);
        if (rb.velocity.y <= 0.1f)// only check grounede state if we are falling down
        {
            // if our check determines we are grounded this frame
            if (Physics.SphereCast(transform.position + new Vector3(0, m_groundedCheckRadius, 0), m_groundedCheckRadius * 0.9f, -Vector3.up, out m_groundedHit, Mathf.Infinity, GroundedCheckLayerMask, QueryTriggerInteraction.Ignore))
            //if (Physics.SphereCast(transform.position + new Vector3(0, m_groundedCheckRadius, 0), m_groundedCheckRadius * 0.9f, -Vector3.up, out m_groundedHit, m_groundedCheckRadius + Mathf.Abs(rb.velocity.y * Time.fixedDeltaTime), GroundedCheckLayerMask, QueryTriggerInteraction.Ignore))
            {
                distToGround = Mathf.Abs(transform.position.y - m_groundedHit.point.y);
                if (distToGround <= m_groundedCheckRadius + Mathf.Abs(rb.velocity.y * Time.fixedDeltaTime))
                {
                    if (GetCurrentStepAngle() < maxRegroundAngle)// if we are at the correct angle to reground
                    {
                        OnGrounded();
                        return;
                    }
                }
            }
            else
            {
                distToGround = Mathf.Infinity;
            }
        }
    }

    private void UpdatePositionMovement()
    {
        //sets a vectory 2 - this is to use .magnetide without the y 
        Vector2 rbVelocityVec2 = new Vector2(rb.velocity.x, rb.velocity.z);
        //calculates the inverse lerp for to determine if the character is stuck or moving
        float scaledMoveSpeedInverseLerp = Mathf.InverseLerp(0, m_curMoveSpeed, rbVelocityVec2.magnitude);
        //calculates the target velocity depending on if the player is stuck or not
        float scaledMoveSpeed = Mathf.Lerp(rbVelocityVec2.magnitude, m_curMoveSpeed, scaledMoveSpeedInverseLerp);

        m_curMoveSpeed = Mathf.Lerp(scaledMoveSpeed, currentControllerState.moveSpeed * m_inputs.magnitude, //lerps our current speed
            (m_inputs.magnitude > 0 ? currentControllerState.acceleration : currentControllerState.deacceleration) * Time.deltaTime * // determines if we should use acceleration or deacceleration
            (1 + (1 - currentControllerState.smoothingFactor)));// this is important to make sure our smoothing doesnt effect our accleration
    }

    private void UpdateRotationMovement()
    {
        UpdateTurnDamping();//update our turn damping weight
                            // rotate our character
                            //transform.rotation = Quaternion.LookRotation(Vector3.Slerp(transform.forward, m_targetDirection, currentControllerState.turnSpeed * Time.deltaTime * m_inputs.magnitude * m_curTurnDamping));
                            //transform.Rotate(transform.eulerAngles - Vector3.Slerp(transform.forward, m_targetDirection, currentControllerState.turnSpeed * Time.deltaTime * m_inputs.magnitude * m_curTurnDamping));
        float angle = Vector3.SignedAngle(transform.forward, Vector3.Slerp(transform.forward, m_targetDirection, currentControllerState.turnSpeed * Time.deltaTime * m_inputs.magnitude * m_curTurnDamping), Vector3.up);
        transform.RotateAround(transform.position, Vector3.up, angle);
    }

    private void UpdateTurnDamping()
    {
        float delta = GetTargetDirectionDeltaAngle() * m_inputs.magnitude;// the signed difference in our current direction to target direction delta

        if (/*prevTargetDeltaAngle != 0 && */((delta > 0 && m_prevTargetDeltaAngle <= 0) || (delta < 0 && m_prevTargetDeltaAngle >= 0)))
            m_curTurnDamping -= currentControllerState.turnDampingChangeIncrease;// in we change directions increase our damping
        else// if we are traveling in the same direction decrease our damping
            m_curTurnDamping += Time.deltaTime * currentControllerState.turnDampingRecovery;

        m_curTurnDamping = Mathf.Clamp01(m_curTurnDamping);//clamp to a weight
        m_prevTargetDeltaAngle = delta;// keep track of the previous so we can compare next frame
    }
    #endregion

    #region ONCALLS
    void OnJump()// when we jump
    {
        rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
        m_shouldJumpNextFixedUpdate = false;
        if(onJump != null)
        {
            onJump();
        }
        isGrounded = false;
        rb.useGravity = true;

    }

    void OnGrounded()// we when transition from not grounede to grounded
    {
        isGrounded = true;
        rb.useGravity = false;
        m_curTurnDamping = 0;// we want to fully damp our turning
        currentControllerState = groundedControllerState;// set our state
        distToGround = 0;
        rb.AddForce(new Vector3(-rb.velocity.x, 0, -rb.velocity.z) * velocityDampeningOnLanded, ForceMode.VelocityChange);// damp our velocity

        if(onGrounded != null)
        {
            onGrounded();
        }
    }

    void OnUngrounded()// when we transition from grouneded to not grounede
    {
        isGrounded = false;
        rb.useGravity = true;
        currentControllerState = airTimeControllerState;
    }
    #endregion

    #region GETTERS
    public float GetCurrentStepAngle(bool ignoreGroundedState = false)// gets the current angle the player is standing on
    {
        return Vector3.Angle(m_groundedHit.normal, Vector3.up);
    }

    public float GetTargetDirectionDeltaAngle()// gets the signed angle from our direction to the target direction
    {
        return Mathf.Round(Mathf.Atan2(
        Vector3.Dot(Vector3.up, Vector3.Cross(transform.forward, m_targetDirection)),
        Vector3.Dot(transform.forward, m_targetDirection)) * Mathf.Rad2Deg);
    }

    public void AddTurnDamping(float t)
    {
        m_curTurnDamping -= t;
    }
    #endregion

    #region DEBUG
    private void OnDrawGizmos()
    {
        //Debug TARGET direction
        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, transform.position + (m_targetDirection * 3));

        //Debug MOVE direction
        Gizmos.color = Color.red;
        if (rb)
            Gizmos.DrawLine(transform.position, transform.position + new Vector3(rb.velocity.x, 0, rb.velocity.z));

        //Debug OUR direction
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + (transform.forward * 5));

        //Debug GROUNDED CHECKS
        Gizmos.color = Color.yellow;
        if (isGrounded)
        {
            Gizmos.DrawWireSphere(transform.position + new Vector3(0, m_groundedCheckRadius * 1.1f + 0.5f, 0) - new Vector3(0, groundedStepHeight + 0.5f, 0), m_groundedCheckRadius * 1.1f);
            Gizmos.DrawRay(transform.position + Vector3.up, Vector3.down * 10); 
        }
        else
            Gizmos.DrawSphere(transform.position + new Vector3(0, m_groundedCheckRadius, 0) - new Vector3(0, m_groundedCheckRadius + Mathf.Abs(rb.velocity.y * Time.fixedDeltaTime), 0), m_groundedCheckRadius * 0.9f);

        //Debug LAST GROUNDED
        if (isGrounded)
            Gizmos.color = Color.grey;
        else
            Gizmos.color = Color.black;
        //Gizmos.DrawLine(transform.position, groundedHit.point);
        Gizmos.DrawRay(m_groundedHit.point, m_groundedHit.normal * 10);
    }
    #endregion
}
