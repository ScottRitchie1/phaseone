﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharLightsController : MonoBehaviour {

    public Vector2 maxLightAngleVertical;
    public float maxLightAngleHorizontal;

    public Light mainSuitLight;
    public Light[] subSuitLights;

	
	// Update is called once per frame
	void Update () {
        if (DayCycleManager.instance)
        {
            if (CharacterInstanceManager.instance.animatorController.activeRagdoll == null)
            {
                if (DayCycleManager.instance.isNight())
                {


                    if (mainSuitLight)
                    {
                        //Vector3 cameraRelitiveEular = Quaternion.FromToRotation(Vector3.up, CharacterInstanceManager.instance.transform.InverseTransformDirection(CamController.instance.transform.forward)).eulerAngles;
                        //Vector3 cameraClampedForward = new Vector3(Mathf.Clamp(cameraRelitiveEular.x, -maxLightAngleHorizontal/90, maxLightAngleHorizontal/90), Mathf.Clamp(cameraRelitiveEular.y, maxLightAngleVertical.x/90, maxLightAngleVertical.y/90), cameraRelitiveEular.z);
                        mainSuitLight.transform.forward = transform.root.forward;

                        if (mainSuitLight.intensity < 1)
                            mainSuitLight.intensity = Mathf.MoveTowards(mainSuitLight.intensity, 1, Time.deltaTime);
                    }

                }
                else
                {
                    if (mainSuitLight && mainSuitLight.intensity > 0)
                    {
                        mainSuitLight.intensity = Mathf.MoveTowards(mainSuitLight.intensity, 0, Time.deltaTime);
                    }
                }
            }
            else
            {
                mainSuitLight.intensity = 0;
            }
        }
	}
    
}
