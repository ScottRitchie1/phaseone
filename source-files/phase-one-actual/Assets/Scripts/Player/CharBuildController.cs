﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharBuildController : MonoBehaviour {
    [System.Serializable]
    public class hologramColourSettings
    {
        public Color primaryColour;
        public Color secondaryColour;
    }

    public Transform buildPoint;
    public BuildUI buildUI;
    MeshFilter holoGramMeshComponent;
    LineRenderer zoneLineRenderer;

    [SerializeField]
    public bool inBuildMode = true;
    public int maxBluePrints;
    int selectedBluePrint = -1;
    [SerializeField]
    List<StructureBluePrint> activeBluePrints;

    [Header("Camera Settings")]
    public float maxBuildReach = 3;
    public float buildCheckHeight = 2;
    public float rotateSpeed = 25f;

    [Header("Hologram Settings")]
    public hologramColourSettings buildingBlockedColour;
    public hologramColourSettings buildingNotConnectedColour;
    public hologramColourSettings buildingConnectedColour;


    [Header("Zone Settings")]
    public float zoneDisplayHeight = 0.25f;
    public float zoneDisplayWidth = 0.1f;
    public Material zoneMaterial;




    bool canBeBuilt = false;
    float currentRotation;
    Vector3 buildPointPosition;
    Vector3 currentNoBuildPositionLocal;
    Vector3 currentNoBuildSize;

    List<PowerLinkerComponent> availableLinkers;
    PowerLinkerComponent currentLinkerTarget;


    int ignoreLayerMask;
    // Use this for initialization
    private void Awake()
    {
        ignoreLayerMask = ~LayerMask.GetMask("Character", "BlockBuilding");

        zoneLineRenderer = buildPoint.GetComponent<LineRenderer>();
        zoneLineRenderer.material = zoneMaterial;
        holoGramMeshComponent = buildPoint.GetComponent<MeshFilter>();
        zoneLineRenderer.startWidth = zoneDisplayWidth;
        zoneLineRenderer.endWidth = zoneDisplayWidth;
        zoneLineRenderer.loop = true;

        activeBluePrints.Clear();
    }

    void Start () {

        ToggleBuildMode(false);
        OnSelectNewBluePrint(0);

    }

    // Update is called once per frame
    void LateUpdate () {
        if (Input.GetButtonDown("Fire3") && TutorialManager.instance.allowBuild)
        {
            if (inBuildMode || (inBuildMode == false && CharController.instance.isGrounded))
            {
                ToggleBuildMode(!inBuildMode);
            }
        }
        else
        {
            if(inBuildMode && CharController.instance.isGrounded == false)
            {
                ToggleBuildMode(false);
            }
        }

        if (inBuildMode && activeBluePrints.Count > 0)
        {
            canBeBuilt = true;

            if (Input.GetButtonDown("Fire2"))
            {
                OnSelectNewBluePrint(ClampBluePrintIndex(selectedBluePrint+1));
            }
            //if(Input.GetAxis("Mouse ScrollWheel") != 0)
            //{
            //    int newBluePrint = selectedBluePrint + (Input.GetAxis("Mouse ScrollWheel") != 0 ? (Input.GetAxis("Mouse ScrollWheel") > 0 ? 1 : -1) : 0);
            //    OnSelectNewBluePrint(ClampBluePrintIndex(newBluePrint));
            //}
            buildPointPosition = Vector3.Lerp(buildPointPosition, GetBuildPosition(), Time.deltaTime * 10);

            currentRotation += Input.GetAxis("Mouse ScrollWheel") * rotateSpeed;
            buildPoint.eulerAngles = Vector3.up * currentRotation;
            RaycastHit[] zoneHits;
            if(!CheckBuildZone(out zoneHits, buildPointPosition))
            {
                canBeBuilt = false;
            }
            float buildPointY = 0;
            for(int i = 0; i < zoneHits.Length; i++)
            {
                buildPointY += zoneHits[i].point.y;
            }

#if UNITY_EDITOR
            buildUI.UpdateOffset(activeBluePrints[selectedBluePrint].buildUIOffSet);
            Debug.DrawLine(new Vector3(buildPointPosition.x, buildPointY / zoneHits.Length, buildPointPosition.y), Vector3.zero);
            Debug.DrawLine(buildPointPosition, Vector3.zero);
# endif
            buildPoint.position =  new Vector3(buildPointPosition.x, buildPointY / zoneHits.Length, buildPointPosition.z);
            //            buildPoint.position = new Vector3(Mathf.Lerp(buildPoint.position.x, buildPointPosition.x, Time.deltaTime * 10), buildPointY / zoneHits.Length, Mathf.Lerp(buildPoint.position.z, buildPointPosition.z, Time.deltaTime * 10));


            Vector3 dir = EvaluatePlacementAngle(zoneHits);

            if (CheckResources() == false || CheckIncline(dir) == false || CheckClippingVolume() == false || CheckNoBuildZones() == false)
            {
                canBeBuilt = false;

#if UNITY_EDITOR
                zoneLineRenderer.loop = true;
                zoneLineRenderer.startWidth = zoneDisplayWidth;
                zoneLineRenderer.endWidth = zoneDisplayWidth;
                float height = (activeBluePrints[selectedBluePrint].construcutionVolumeSize.y + (activeBluePrints[selectedBluePrint].constructionVolumeOffset.y - activeBluePrints[selectedBluePrint].construcutionVolumeSize.y / 2)) / 2;
                currentNoBuildPositionLocal = new Vector3(activeBluePrints[selectedBluePrint].constructionAreaOffset.x, height, activeBluePrints[selectedBluePrint].constructionAreaOffset.z);
                currentNoBuildSize = new Vector3(activeBluePrints[selectedBluePrint].constructiondAreaSize.x, height, activeBluePrints[selectedBluePrint].constructiondAreaSize.y);
#endif

            }

            if (activeBluePrints[selectedBluePrint].structureSnapsToIncline)
            {
                buildPoint.rotation = Quaternion.LookRotation(Vector3.Cross(dir, buildPoint.right), dir);
            }

            if (canBeBuilt)
            {

                currentLinkerTarget = GetTargetPowerLinker();

                if (currentLinkerTarget && Vector3.Distance(currentLinkerTarget.transform.position, buildPoint.position) < currentLinkerTarget.linkerRange)
                {
                    holoGramMeshComponent.GetComponent<MeshRenderer>().material.SetColor("_Colour", buildingConnectedColour.primaryColour);
                    holoGramMeshComponent.GetComponent<MeshRenderer>().material.SetColor("_GlowFresnelColour", buildingConnectedColour.secondaryColour);
                    buildUI.UpdateBuildStatus(true, true);
                }
                else
                {
                    holoGramMeshComponent.GetComponent<MeshRenderer>().material.SetColor("_Colour", buildingNotConnectedColour.primaryColour);
                    holoGramMeshComponent.GetComponent<MeshRenderer>().material.SetColor("_GlowFresnelColour", buildingNotConnectedColour.secondaryColour);
                    buildUI.UpdateBuildStatus(true, false);
                }

                if (Input.GetButtonDown("Fire1"))
                {
                    BuildSelectedStructure();
                    ToggleBuildMode(false);
                }
            }
            else
            {
                
                holoGramMeshComponent.GetComponent<MeshRenderer>().material.SetColor("_Colour", buildingBlockedColour.primaryColour);
                holoGramMeshComponent.GetComponent<MeshRenderer>().material.SetColor("_GlowFresnelColour", buildingBlockedColour.secondaryColour);
            }

        }
	}

    bool CheckResources()
    {
        for(int i = 0; i < activeBluePrints[selectedBluePrint].constructionRequirements.Count; i++)
        {
            if (ShipInstanceManager.instance.shipResourceContainer == null ||
                !ShipInstanceManager.instance.shipResourceContainer.HasResources(
                    activeBluePrints[selectedBluePrint].constructionRequirements[i].requiredResource, 
                    activeBluePrints[selectedBluePrint].constructionRequirements[i].requiredAmount)
                    ){
                buildUI.UpdateBuildStatus(false, "Insufficient  resources");
                return false;
            }
        }
        return true;
    }

    bool CheckIncline(Vector3 upVector)
    {
        if(Vector3.Angle(upVector, Vector3.up) < activeBluePrints[selectedBluePrint].maxConstructionIncline)
        {
            return true;
        }
        buildUI.UpdateBuildStatus(false, "Blocked by incline");
        return false;
    }

    bool CheckNoBuildZones()
    {
        if(Physics.OverlapBox(buildPoint.position + buildPoint.TransformDirection(currentNoBuildPositionLocal),
                    currentNoBuildSize,
                    buildPoint.rotation,
                    LayerMask.GetMask("BlockBuilding")).Length == 0)
        {
            return true;
        }
        buildUI.UpdateBuildStatus(false, "Unauthorised building location");
        return false;
    }

    bool CheckClippingVolume()
    {
        if(Physics.OverlapBox(buildPoint.position + buildPoint.TransformDirection(activeBluePrints[selectedBluePrint].constructionVolumeOffset), activeBluePrints[selectedBluePrint].construcutionVolumeSize / 2, buildPoint.rotation, ~LayerMask.GetMask("BlockBuilding"), QueryTriggerInteraction.Ignore).Length == 0)
        {
            return true;
        }
        buildUI.UpdateBuildStatus(false, "Too close to another object");
        return false;
    }

    public bool AddBlueprint(StructureBluePrint blueprint, int replacementIndex = -1)
    {
        if (replacementIndex > -1)
        {
            if (replacementIndex < maxBluePrints)
            {
                activeBluePrints[replacementIndex] = blueprint;
                return true;
            }
        }
        else
        {
            if (activeBluePrints.Count < maxBluePrints)
            {
                activeBluePrints.Add(blueprint);
                return true;
            }
        }
        return false;
    }

    public void BuildSelectedStructure()
    {
        if (activeBluePrints[selectedBluePrint].constructionPrefab)
        {
            UnderConstructionComponent newStructure = Instantiate(activeBluePrints[selectedBluePrint].constructionPrefab, buildPoint.position, buildPoint.rotation).AddComponent<UnderConstructionComponent>();
            if (currentLinkerTarget && Vector3.Distance(newStructure.transform.position, currentLinkerTarget.transform.position) < currentLinkerTarget.linkerRange)
            {
                if (newStructure.GetComponent<PowerLinkerComponent>())
                {
                    currentLinkerTarget.AddConnectedPowerLinker(newStructure.GetComponent<PowerLinkerComponent>());
                }
                else if(newStructure.powerComponent)
                {
                    currentLinkerTarget.AddPowerComponent(newStructure.powerComponent);
                }
                //ShipInstanceManager.instance.shipResourceContainer.gameObject.GetComponent<PowerPlantComponent>().AddPowerComponent(newStructure.powerComponent);
            }
            Vector3[] zonePositions = new Vector3[zoneLineRenderer.positionCount];
            zoneLineRenderer.GetPositions(zonePositions);
            newStructure.SetZonePoints(zonePositions, zoneLineRenderer.startWidth, zoneMaterial);
        
            newStructure.SetBlockBuildingArea(currentNoBuildPositionLocal, currentNoBuildSize*2);
            newStructure.SetBuildTime(activeBluePrints[selectedBluePrint].constructionTime, activeBluePrints[selectedBluePrint].constructionTimeType);

            for(int i = 0; i < activeBluePrints[selectedBluePrint].constructionRequirements.Count; i++)
            {
                ShipInstanceManager.instance.shipResourceContainer.RemoveResources(activeBluePrints[selectedBluePrint].constructionRequirements[i].requiredResource, activeBluePrints[selectedBluePrint].constructionRequirements[i].requiredAmount);
            }
            //for(int i = 0; i < activeBluePrints[selectedBluePrint].constructionRequirements.Count; i++)
            //{
            //    resourceDepletionTracker resourceDepletion = new resourceDepletionTracker();
            //    resourceDepletion.resource = activeBluePrints[selectedBluePrint].constructionRequirements[i].requiredResource;
            //    resourceDepletion.amount = activeBluePrints[selectedBluePrint].constructionRequirements[i].requiredAmount;
            //    newStructure.resourceDepletion.Add(resourceDepletion);
            //}
            availableLinkers = new List<PowerLinkerComponent>(FindObjectsOfType<PowerLinkerComponent>());
        }
    }

    void ToggleBuildMode(bool active){
        if(inBuildMode != active)
        {
            buildPoint.gameObject.SetActive(active);
            inBuildMode = active;
            buildPoint.SetParent((active ? null : transform));
            if(inBuildMode)
                buildPointPosition = GetBuildPosition();

        }
    }

    int ClampBluePrintIndex(int index)
    {
        index = index % (activeBluePrints.Count);
        if (index < 0)
        {
            index = activeBluePrints.Count-1;
        }
        return index;
    }

    void OnSelectNewBluePrint(int index = 0)
    {
        if ((index >= 0 && index < activeBluePrints.Count) && selectedBluePrint != index)
        {
            selectedBluePrint = index;
            holoGramMeshComponent.mesh = activeBluePrints[selectedBluePrint].constructionHologram;
            float height = (activeBluePrints[selectedBluePrint].construcutionVolumeSize.y + (activeBluePrints[selectedBluePrint].constructionVolumeOffset.y - activeBluePrints[selectedBluePrint].construcutionVolumeSize.y / 2)) / 2;
            currentNoBuildPositionLocal = new Vector3(activeBluePrints[selectedBluePrint].constructionAreaOffset.x, height, activeBluePrints[selectedBluePrint].constructionAreaOffset.z);
            currentNoBuildSize = new Vector3(activeBluePrints[selectedBluePrint].constructiondAreaSize.x, height, activeBluePrints[selectedBluePrint].constructiondAreaSize.y);
            Debug.Log("BUILDING: " + gameObject.name + " selected a new blue print to build: " + activeBluePrints[selectedBluePrint].structureName);
            availableLinkers = new List<PowerLinkerComponent>(FindObjectsOfType<PowerLinkerComponent>());
            currentLinkerTarget = GetTargetPowerLinker();

            buildUI.UpdateIcon(activeBluePrints[selectedBluePrint].icon);
            buildUI.UpdateOffset(activeBluePrints[selectedBluePrint].buildUIOffSet);
            buildUI.UpdateStructureName(activeBluePrints[selectedBluePrint].structureName);
            buildUI.UpdateBuildTime(WorldTimeManager.TimeString(WorldTimeManager.ConvertTime(activeBluePrints[selectedBluePrint].constructionTimeType, WorldTimeManager._time.seconds, activeBluePrints[selectedBluePrint].constructionTime)));
        }
    }

    Vector3 EvaluatePlacementAngle(RaycastHit[] hits)
    {
        if(hits.Length > 1)
        {
            Vector3 angleV = Vector3.zero;
            for (int i = 0; i < hits.Length; i++)
            {

                angleV += hits[i].normal;

            }
            return (angleV / (hits.Length - 1)).normalized;
        }
        else if(hits.Length > 0)
        {
            return hits[0].normal;
        }
        return Vector3.up;
    }

    PowerLinkerComponent GetTargetPowerLinker()
    {
        if (availableLinkers.Count > 0)
        {
            int closestLinker = 0;
            float closestLinkerDist = Vector3.Distance(buildPoint.position, availableLinkers[0].transform.position);
            for (int i = 1; i < availableLinkers.Count; i++)
            {
                float dist = Vector3.Distance(availableLinkers[i].transform.position, buildPoint.position);
                if (dist < availableLinkers[i].linkerRange && dist < closestLinkerDist)
                {
                    closestLinker = i;
                    closestLinkerDist = dist;
                }
            }
            

            if(closestLinkerDist < availableLinkers[closestLinker].linkerRange)
            {
                return availableLinkers[closestLinker];
            }
        }
        return null;
    }

    Vector3 GetBuildPosition()
    {
        RaycastHit buildHit;
        float TargetDistFromPlayer = (CamController.instance.transform.eulerAngles.x > 90 ? maxBuildReach : CamController.instance.targetOffset / Mathf.Tan(CamController.instance.transform.eulerAngles.x * Mathf.Deg2Rad));
        Vector3 _buildPoint = transform.position + CamController.instance.GetCameraDirection() * (Mathf.Clamp(TargetDistFromPlayer, 1, (Physics.Raycast(transform.position + Vector3.up * CamController.instance.targetOffset, CamController.instance.GetCameraDirection(), out buildHit, maxBuildReach * 2, ignoreLayerMask, QueryTriggerInteraction.Ignore) ? Vector3.Distance(transform.position + Vector3.up * CamController.instance.targetOffset, buildHit.point) - 0.5f : maxBuildReach)));
        if (Physics.Raycast(_buildPoint + Vector3.up * (buildCheckHeight + 0.5f), Vector3.down, out buildHit, buildCheckHeight * 2, ignoreLayerMask, QueryTriggerInteraction.Ignore))
        {
            canBeBuilt = true;
            return buildHit.point;
        }
        else
        {
            Vector3 buildPointDirection = (_buildPoint - transform.position).normalized;
            Vector3 returnPoint = transform.position + Vector3.up + buildPointDirection
                * (Physics.Raycast(transform.position + Vector3.up, buildPointDirection, out buildHit, maxBuildReach, ignoreLayerMask, QueryTriggerInteraction.Ignore)
                ? Mathf.Max(1, Vector3.Distance(buildHit.point, transform.position + Vector3.up))
                : Mathf.Clamp(TargetDistFromPlayer, Mathf.Min(activeBluePrints[selectedBluePrint].constructionAreaOffset.x, activeBluePrints[selectedBluePrint].constructionAreaOffset.z), maxBuildReach));
            return returnPoint;
        }
    }

    bool CheckBuildZone(out RaycastHit[] areaHitPoints, Vector3 fromPoint)
    {
        RaycastHit centerHit;
        bool centerWasHit = CheckBuildAreaPoint(fromPoint + buildPoint.TransformDirection(activeBluePrints[selectedBluePrint].constructionAreaOffset), buildCheckHeight ,out centerHit);
        if (activeBluePrints[selectedBluePrint].constructiondAreaSize != Vector2.zero)
        {
            areaHitPoints = new RaycastHit[5];
            bool[] cornerWasHits = new bool[4];
            cornerWasHits[0] = CheckBuildAreaPoint(fromPoint + buildPoint.TransformDirection(activeBluePrints[selectedBluePrint].constructionAreaOffset + new Vector3(activeBluePrints[selectedBluePrint].constructiondAreaSize.x, 0, activeBluePrints[selectedBluePrint].constructiondAreaSize.y)), buildCheckHeight, out areaHitPoints[0]);
            cornerWasHits[1] = CheckBuildAreaPoint(fromPoint + buildPoint.TransformDirection(activeBluePrints[selectedBluePrint].constructionAreaOffset + new Vector3(activeBluePrints[selectedBluePrint].constructiondAreaSize.x, 0, -activeBluePrints[selectedBluePrint].constructiondAreaSize.y)), buildCheckHeight, out areaHitPoints[1]);
            cornerWasHits[2] = CheckBuildAreaPoint(fromPoint + buildPoint.TransformDirection(activeBluePrints[selectedBluePrint].constructionAreaOffset + new Vector3(-activeBluePrints[selectedBluePrint].constructiondAreaSize.x, 0, -activeBluePrints[selectedBluePrint].constructiondAreaSize.y)), buildCheckHeight, out areaHitPoints[2]);
            cornerWasHits[3] = CheckBuildAreaPoint(fromPoint + buildPoint.TransformDirection(activeBluePrints[selectedBluePrint].constructionAreaOffset + new Vector3(-activeBluePrints[selectedBluePrint].constructiondAreaSize.x, 0, activeBluePrints[selectedBluePrint].constructiondAreaSize.y)), buildCheckHeight, out areaHitPoints[3]);


            zoneLineRenderer.positionCount = 4;
            zoneLineRenderer.SetPosition(0, areaHitPoints[0].point + Vector3.up * zoneDisplayHeight);
            zoneLineRenderer.SetPosition(1, areaHitPoints[1].point + Vector3.up * zoneDisplayHeight);
            zoneLineRenderer.SetPosition(2, areaHitPoints[2].point + Vector3.up * zoneDisplayHeight);
            zoneLineRenderer.SetPosition(3, areaHitPoints[3].point + Vector3.up * zoneDisplayHeight);
            //zoneLineRenderer.SetPosition(4, areaHitPoints[0].point + Vector3.up * zoneDisplayHeight);

            areaHitPoints[4] = centerHit;

            return (centerWasHit && cornerWasHits[0] && cornerWasHits[1] && cornerWasHits[2] && cornerWasHits[3] 
                && !centerHit.transform.root.GetComponent<Rigidbody>()
                && !areaHitPoints[0].transform.GetComponent<Rigidbody>()
                && !areaHitPoints[1].transform.GetComponent<Rigidbody>()
                && !areaHitPoints[2].transform.GetComponent<Rigidbody>()
                && !areaHitPoints[3].transform.GetComponent<Rigidbody>());

        }
        else
        {
            zoneLineRenderer.positionCount = 0;
            areaHitPoints = new RaycastHit[1];
            areaHitPoints[0] = centerHit;

            return centerWasHit;
        }

    }

    bool CheckBuildAreaPoint(Vector3 position, float Lenght, out RaycastHit hit)
    {
        //if(Physics.Raycast(position, Vector3.up, out hit, Lenght, ~0, QueryTriggerInteraction.Ignore))
        //{
        //    if(Physics.Raycast(hit.point, Vector3.down, out hit, (hit.point.y - position.y) + Lenght, ~0, QueryTriggerInteraction.Ignore))
        //    {
        //        return true;
        //    }
        //}
        //else
        //{
            if (Physics.Raycast(position + Vector3.up * Lenght, Vector3.down, out hit, Lenght * 2f, ignoreLayerMask, QueryTriggerInteraction.Ignore))
            {
                return true;
            }
        //}
        hit.point = position;
        hit.normal = Vector3.up;
        return false;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (inBuildMode && selectedBluePrint != -1)
        {
            if (canBeBuilt)
            {
                Gizmos.color = Color.green;
            }
            else
            {
                Gizmos.color = Color.red;
            }

            Gizmos.matrix = Matrix4x4.TRS(buildPoint.position, buildPoint.rotation, Vector3.one);
            Gizmos.DrawCube(-activeBluePrints[selectedBluePrint].constructionVolumeOffset + Vector3.up * (activeBluePrints[selectedBluePrint].constructionVolumeOffset.y * 2), activeBluePrints[selectedBluePrint].construcutionVolumeSize);
        }
    }
#endif
}
