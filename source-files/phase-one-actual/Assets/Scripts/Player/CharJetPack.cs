﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharController))]
public class CharJetPack : MonoBehaviour {
    CharController charController;
    Rigidbody rb;
    [Header("Thrust")]
    public bool isThrusting = false;
    public float force;
    public float maxVelLimit;
    public float inputDelay;
    [Range(0,1)]
    public float turnDampingOnThrust;
    float curInputDelay;

    [Header("Fuel")]
    public float fuelTime;
    [SerializeField]
    float curFuelTime;
    public float recoveryTime;
    
    

    public float recoveryStepAngle;

    bool waitToRepressJump = true;
    bool wasCharacterGrounded = true;

    [Space]
    public CharController.controllerState jetPackState;

	// Use this for initialization
	void Start () {
        charController = GetComponent<CharController>();
        rb = GetComponent<Rigidbody>();
        curFuelTime = fuelTime;
        charController.onJump += OnJump;
    }

    private void OnDestroy()
    {
        charController.onJump -= OnJump;
    }

    // Update is called once per frame
    void Update () {
		if(!wasCharacterGrounded)
        {

            if (Input.GetButtonUp("Jump") || curFuelTime < 0 && isThrusting)
            {
                isThrusting = false;
                charController.currentControllerState = charController.airTimeControllerState;
                waitToRepressJump = false;
            }
            else if(Input.GetButtonDown("Jump") && !wasCharacterGrounded)
            {
                if (!waitToRepressJump)
                {
                    isThrusting = true;
                    charController.AddTurnDamping(turnDampingOnThrust);
                    charController.currentControllerState = jetPackState;
                }
            }

            if (isThrusting)
            {
                curInputDelay += Time.deltaTime;
                if (curInputDelay >= inputDelay)
                {
                    curFuelTime -= Time.deltaTime;
                    if (rb.velocity.y < maxVelLimit)
                    {
                        rb.AddForce(transform.up * force * Time.deltaTime, ForceMode.Force);
                    }
                }
            }
            else
            {
                curInputDelay = 0;
            }
        }else
        {
            isThrusting = false;
            if (charController.GetCurrentStepAngle() < recoveryStepAngle)
            {
                if (curFuelTime < fuelTime)
                    curFuelTime += Time.deltaTime * fuelTime / recoveryTime;
                else
                {
                    curFuelTime = fuelTime;
                }
            }

            curInputDelay = 0;
        }

        wasCharacterGrounded = charController.isGrounded;



    }

    void OnJump()
    {
        waitToRepressJump = true;
    }

    public float GetCurrentFuelPercentage()
    {
        return curFuelTime / fuelTime;
    }
}
