﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharAnimator))]
public class CharIkController : MonoBehaviour {
    public bool ikHeightAdjust = true;

    Animator anim;

    public float footOffset = 0.1f;
    public float footCheckDistance = 2f;
    public float heightAdjustSmoothing = 5;
    Transform m_rightFootBone;
    Transform m_leftFootBone;
    Transform m_hips;

    public LayerMask footIkIgnoreLayers;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        m_hips = anim.GetBoneTransform(HumanBodyBones.Hips);
        m_rightFootBone = anim.GetBoneTransform(HumanBodyBones.RightFoot);
        m_leftFootBone = anim.GetBoneTransform(HumanBodyBones.LeftFoot);

    }

    private void FixedUpdate()
    {

        //if (ikHeightAdjust && CharacterInstanceManager.instance.controller.isGrounded)
            //transform.transform.localPosition += Vector3.up * Mathf.Clamp((CharacterInstanceManager.instance.transform.position.y - CharacterInstanceManager.instance.controller.curGroundedYPos), -footCheckDistance, 0);//this makes sure when we adjust our controllers height we also adjust the root height
    }

    private void OnAnimatorIK(int layerIndex)
    {
        bool grounded = CharacterInstanceManager.instance.controller.isGrounded;

        float curY = transform.localPosition.y;
        transform.localPosition = Vector3.zero;

        float rightFootIkWeight = anim.GetFloat("RFIK");
        float rightFootHipsPosDifference = m_hips.position.y - m_rightFootBone.position.y;
        Vector3 rightFootRayStart = m_rightFootBone.position + Vector3.up* rightFootHipsPosDifference;
        RaycastHit rightFootRayHit;

        float leftFootIkWeight = anim.GetFloat("LFIK");
        float leftFootHipsPosDifference = m_hips.position.y - m_leftFootBone.position.y;
        Vector3 leftFootRayStart = m_leftFootBone.position + Vector3.up* leftFootHipsPosDifference;
        RaycastHit leftFootRayHit;

        Vector3 rightFootLocalPos;
        Vector3 leftFootLocalPos;

#if UNITY_EDITOR
        Debug.DrawRay(rightFootRayStart, Vector3.down * (rightFootHipsPosDifference + footCheckDistance));
        Debug.DrawRay(leftFootRayStart, Vector3.down * (leftFootHipsPosDifference + footCheckDistance));
#endif

        if (grounded && Physics.Raycast(rightFootRayStart, Vector3.down, out rightFootRayHit, rightFootHipsPosDifference + footCheckDistance, ~footIkIgnoreLayers, QueryTriggerInteraction.Ignore))
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, rightFootIkWeight);
            anim.SetIKPosition(AvatarIKGoal.RightFoot, new Vector3(m_rightFootBone.position.x, rightFootRayHit.point.y + footOffset, m_rightFootBone.position.z));
            rightFootLocalPos = transform.InverseTransformPoint(rightFootRayHit.point);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 0);
            rightFootLocalPos = transform.InverseTransformPoint(m_rightFootBone.position);
        }

        if (grounded && Physics.Raycast(leftFootRayStart, Vector3.down, out leftFootRayHit, leftFootHipsPosDifference + footCheckDistance, ~footIkIgnoreLayers, QueryTriggerInteraction.Ignore))
        {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, leftFootIkWeight);
            anim.SetIKPosition(AvatarIKGoal.LeftFoot, new Vector3(m_leftFootBone.position.x, leftFootRayHit.point.y + footOffset, m_leftFootBone.position.z));
            leftFootLocalPos = transform.InverseTransformPoint(leftFootRayHit.point);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 0);
            leftFootLocalPos = transform.InverseTransformPoint(m_leftFootBone.position);
        }


        //transform.GetChild(0).transform.localPosition += Vector3.up * (transform.position.y - curGroundedYPos);


        if (ikHeightAdjust && grounded)
            transform.localPosition = new Vector3(0, 
                Mathf.Lerp(curY, (leftFootLocalPos.y < rightFootLocalPos.y ?
                Mathf.Lerp(0, leftFootLocalPos.y, Mathf.InverseLerp(2,0, new Vector2(CharacterInstanceManager.instance.rb.velocity.x, CharacterInstanceManager.instance.rb.velocity.z).magnitude)) :
                Mathf.Lerp(0, rightFootLocalPos.y, Mathf.InverseLerp(2, 0, new Vector2(CharacterInstanceManager.instance.rb.velocity.x, CharacterInstanceManager.instance.rb.velocity.z).magnitude))),
                Time.deltaTime * heightAdjustSmoothing)
                , 0);
        else
            transform.localPosition = Vector3.zero;
    }
}
