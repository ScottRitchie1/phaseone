﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PowerComponent : MonoBehaviour
{
    PowerLinkerComponent powerLinker;

    public PowerLinkerComponent ConnectedToPowerLinker
    {
        get
        {
            return powerLinker;
        }
        set
        {
            if(powerLinker != value)
            {
                if(value == null)
                {
                    powerLinker = null;
                    isPowered = false;
                    if(ConnectedStatusChanged != null)
                    {
                        ConnectedStatusChanged.Invoke(false);
                    }
                    if (PoweredStatusChanged != null)
                    {
                        PoweredStatusChanged.Invoke(false);
                    }
                }
                else if(value != null)
                {
                    powerLinker = value;
                    if (ConnectedStatusChanged != null)
                    {
                        ConnectedStatusChanged.Invoke(true);
                    }
                }
                GenerateCord();
            }
        }
    }
    LineRenderer cordRenderer;


    public bool isPowered;
    public bool isOn = true;

    public bool IsPowered
    {
        get
        {
            return isPowered;
        }
        set
        {
            if(isPowered != value)
            {
                isPowered = value;
                if (PoweredStatusChanged != null)
                    PoweredStatusChanged.Invoke(isPowered);
            }
            
        }
    }

    public float powerOutputPerDay;
    public float powerConsumptionPerDay;

    public UnityEvent<bool> ConnectedStatusChanged;
    public UnityEvent<bool> PoweredStatusChanged;

    public Transform cordLinkPosition;

    public void GenerateCord()
    {
        cordRenderer = GetComponent<LineRenderer>();
        if(cordRenderer != null)
        {
            cordRenderer.textureMode = LineTextureMode.Tile;
            if (powerLinker)
            {
                cordRenderer.positionCount = 2;
                if (cordLinkPosition){
                    cordRenderer.SetPosition(0, cordLinkPosition.position);
                } else {
                    cordRenderer.SetPosition(0, transform.position);
                }

                if (powerLinker.cordLinkPosition)
                {
                    cordRenderer.SetPosition(1, powerLinker.cordLinkPosition.position);
                }
                else
                {
                    cordRenderer.SetPosition(1, powerLinker.transform.position);
                }
            }
            else
            {
                cordRenderer.positionCount = 0;
            }
        }
    }
}
