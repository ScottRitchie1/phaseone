﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class powerLinkerCord
{
    public LineRenderer cordStart;
    //public PowerLinkerComponent myLinker;
    //public PowerLinkerComponent linkedTo;
    public bool isEnd;
    public bool isStart;
}

public class PowerLinkerComponent : MonoBehaviour {

    static string linkerLineMaterialPath = "Wires_Scifi_A_Cropped_Heavy_Wire_A";

    public float linkerRange;

    public Transform cordLinkPosition;

    public List<PowerComponent> directConnectedPowerComponents;
    public List<PowerLinkerComponent> connectedPowerLinkers;
    public List<powerLinkerCord> cordLinks = new List<powerLinkerCord>();

    public void Start()
    {
        if (!GetComponent<PowerPlantComponent>())
        {
            List<PowerComponent> powerComponents = new List<PowerComponent>(FindObjectsOfType<PowerComponent>());
            List<PowerLinkerComponent> powerLinkerComponents = new List<PowerLinkerComponent>(FindObjectsOfType<PowerLinkerComponent>());
            powerLinkerComponents.Sort(
                delegate (PowerLinkerComponent a, PowerLinkerComponent b) 
                { return Vector3.Distance(this.transform.position, a.transform.position).CompareTo(Vector3.Distance(this.transform.position, b.transform.position)); }
                
                
                );

            for (int i = 0; i < powerComponents.Count; i++)
            {
                if (powerComponents[i].ConnectedToPowerLinker == null)
                {
                    if (Vector3.Distance(powerComponents[i].transform.position, transform.position) <= linkerRange)
                    {
                        AddPowerComponent(powerComponents[i]);
                    }
                }
            }

            List<PowerLinkerComponent> searched = new List<PowerLinkerComponent>();
            for (int i = 0; i < powerLinkerComponents.Count; i++)
            {
                if(searched.Contains(powerLinkerComponents[i]) == false)
                {
                    if (Vector3.Distance(powerLinkerComponents[i].transform.position, transform.position) <= linkerRange)
                    {
                        searched.AddRange(powerLinkerComponents[i].DBS_All(ref searched));
                        AddConnectedPowerLinker(powerLinkerComponents[i]);
                    }
                }
                
            }
        }
    }


    public void AddPowerComponent(PowerComponent componentToAdd)
    {
        if (directConnectedPowerComponents.Contains(componentToAdd) == false)
        {
            directConnectedPowerComponents.Add(componentToAdd);
            componentToAdd.ConnectedToPowerLinker = this;
        }
    }

    public void RemovePowerComponent(PowerComponent componentToRemove)
    {
        if (directConnectedPowerComponents.Contains(componentToRemove))
        {
            directConnectedPowerComponents.Remove(componentToRemove);
            componentToRemove.ConnectedToPowerLinker = null;
        }
    }

    public bool InRangeToLink(Vector3 position)
    {
        return Vector3.Distance(position, transform.position) < linkerRange;
    }

    public void AddConnectedPowerLinker(PowerLinkerComponent linkerToAdd)
    {
        if (GetConnectedLinkerComponents().Contains(linkerToAdd) == false && linkerToAdd != this)
        {
            connectedPowerLinkers.Add(linkerToAdd);
            if(linkerToAdd.connectedPowerLinkers.Contains(this) == false)
            {
                GenerateCord(linkerToAdd);
                linkerToAdd.AddConnectedPowerLinker(this);
            }
            //linkerToAdd.ConnectedToPowerLinker = this;
        }
    }

    public void RemoveConnectedPowerLinker(PowerLinkerComponent linkerToRemove)
    {
        if (connectedPowerLinkers.Contains(linkerToRemove))
        {
            connectedPowerLinkers.Remove(linkerToRemove);
            linkerToRemove.RemoveConnectedPowerLinker(this);
            //linkerToRemove.ConnectedToPowerLinker = null;
        }
    }


    public void GenerateCord(PowerLinkerComponent tryingToLinkToUs = null)
    {
        if (cordLinks.Count > 0)
        {
            foreach (powerLinkerCord ourLinkCord in cordLinks)//adds link to end of line renderer
            {
                if (ourLinkCord.isEnd)
                {
                    ourLinkCord.isEnd = false;
                    ourLinkCord.cordStart.positionCount++;
                    ourLinkCord.cordStart.SetPosition(ourLinkCord.cordStart.positionCount - 1, (tryingToLinkToUs.cordLinkPosition ? tryingToLinkToUs.cordLinkPosition.position : tryingToLinkToUs.transform.position));
                    powerLinkerCord theirNewCord = new powerLinkerCord();
                    theirNewCord.cordStart = ourLinkCord.cordStart;
                    theirNewCord.isEnd = true;
                    theirNewCord.isStart = false;
                    //newCord.myLinker = toLink;
                    //theirNewCord.linkedTo = this;
                    tryingToLinkToUs.cordLinks.Add(theirNewCord);
                    return;
                }
                else if (ourLinkCord.isStart)//adds link to start of line renderer
                {
                    ourLinkCord.isStart = false;

                    Vector3[] pointsArr = new Vector3[ourLinkCord.cordStart.positionCount];
                    ourLinkCord.cordStart.GetPositions(pointsArr);
                    List<Vector3> points = new List<Vector3>(pointsArr);
                    points.Insert(0, (tryingToLinkToUs.cordLinkPosition ? tryingToLinkToUs.cordLinkPosition.position : tryingToLinkToUs.transform.position));
                    ourLinkCord.cordStart.positionCount++;
                    ourLinkCord.cordStart.SetPositions(points.ToArray());
                    powerLinkerCord newCord = new powerLinkerCord();
                    newCord.cordStart = ourLinkCord.cordStart;
                    newCord.isEnd = false;
                    newCord.isStart = true;
                    //newCord.myLinker = toLink;
                    //newCord.linkedTo = this;
                    tryingToLinkToUs.cordLinks.Add(newCord);
                    Debug.Log("found start");
                    return;
                }
            }
        }

        if (GetComponent<LineRenderer>() == false)
        {
            LineRenderer lr = gameObject.AddComponent<LineRenderer>();
            lr.material = Resources.Load<Material>(linkerLineMaterialPath);
            lr.endWidth = 0.2f;
            lr.startWidth = 0.2f;
            powerLinkerCord myLinkCord = new powerLinkerCord();
            powerLinkerCord theirLinkCord = new powerLinkerCord();
            lr.textureMode = LineTextureMode.Tile;

            myLinkCord.isEnd = false;
            myLinkCord.isStart = true;
            myLinkCord.cordStart = lr;
            //myLinkCord.myLinker = this;
            //myLinkCord.linkedTo = tryingToLinkToUs;

            theirLinkCord.isEnd = true;
            theirLinkCord.isStart = false;
            theirLinkCord.cordStart = lr;
            //theirLinkCord.myLinker = toLink;
            //theirLinkCord.linkedTo = this;

            lr.positionCount = 2;
            lr.SetPosition(0, (cordLinkPosition ? cordLinkPosition.position : transform.position));
            lr.SetPosition(1, (tryingToLinkToUs.cordLinkPosition ? tryingToLinkToUs.cordLinkPosition.position : tryingToLinkToUs.transform.position));

            tryingToLinkToUs.cordLinks.Add(theirLinkCord);
            cordLinks.Add(myLinkCord);
            Debug.Log("new link on my end");
            return;
        }

        if (tryingToLinkToUs.GetComponent<LineRenderer>() == false)
        {
            LineRenderer lr = tryingToLinkToUs.gameObject.AddComponent<LineRenderer>();
            lr.material = Resources.Load<Material>(linkerLineMaterialPath);
            lr.endWidth = 0.2f;
            lr.startWidth = 0.2f;
            lr.textureMode = LineTextureMode.Tile;
            powerLinkerCord myLinkCord = new powerLinkerCord();
            powerLinkerCord theirLinkCord = new powerLinkerCord();

            myLinkCord.isEnd = true;
            myLinkCord.isStart = false;
            myLinkCord.cordStart = lr;
            //myLinkCord.myLinker = this;
            //myLinkCord.linkedTo = tryingToLinkToUs;

            theirLinkCord.isEnd = false;
            theirLinkCord.isStart = true;
            theirLinkCord.cordStart = lr;
            //theirLinkCord.myLinker = toLink;
            //theirLinkCord.linkedTo = this;

            lr.positionCount = 2;
            lr.SetPosition(0, (tryingToLinkToUs.cordLinkPosition ? tryingToLinkToUs.cordLinkPosition.position : tryingToLinkToUs.transform.position));
            lr.SetPosition(1, (cordLinkPosition ? cordLinkPosition.position : transform.position));

            tryingToLinkToUs.cordLinks.Add(theirLinkCord);
            cordLinks.Add(myLinkCord);
            Debug.Log("new link on their end");
            return;
        }

        



        Debug.LogError("FAILED TO LINK A LINKER");

    }

    public List<PowerLinkerComponent> DBS_All(ref List<PowerLinkerComponent> alreadySearched)
    {
        List<PowerLinkerComponent> additionToList = new List<PowerLinkerComponent>();
        foreach(PowerLinkerComponent pl in connectedPowerLinkers)
        {
            if(alreadySearched.Contains(pl) == false)
            {
                additionToList.Add(pl);
                alreadySearched.Add(pl);
                additionToList.AddRange(pl.DBS_All(ref alreadySearched));

            }
        }


        return additionToList;
    }

    public List<PowerLinkerComponent> GetConnectedLinkerComponents()
    {
        List<PowerLinkerComponent> searched = new List<PowerLinkerComponent>();
        return DBS_All(ref searched);
    }
}
