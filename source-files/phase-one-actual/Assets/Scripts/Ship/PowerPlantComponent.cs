﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerPlantComponent : MonoBehaviour {

    [SerializeField]
    List<PowerLinkerComponent> connectedPowerLinkers;
    [SerializeField]
    List<PowerComponent> connectedPowerComponents;
    public PowerLinkerComponent shipPowerLinker;

    public float maxCapacity;
    public double currentCapacity = -1;
    [SerializeField]
    double powerDrain;

    [Header("Cords")]
    public Transform cordLinkPosition;
	
	// Update is called once per frame
	void Update () {
        powerDrain = CalculatePowerUsage();
        currentCapacity += (powerDrain * WorldTimeManager.ConvertTime(WorldTimeManager._time.seconds, WorldTimeManager._time.days, WorldTimeManager.deltaTime));

        if (currentCapacity > maxCapacity)
        {
            currentCapacity = maxCapacity;
        }else if(currentCapacity < 0)
        {
            currentCapacity = 0;
        }
        connectedPowerComponents = GetPowerComponents();
    }

    public void SetPower(double amount)
    {
        currentCapacity = amount;
    }

    public void ForceChangePower(int amount)
    {
        currentCapacity -= amount;
    }

    public void AddPowerLinker(PowerLinkerComponent linkerToAdd)
    {
        if (connectedPowerLinkers.Contains(linkerToAdd) == false)
        {
            connectedPowerLinkers.Add(linkerToAdd);
            Debug.Log(linkerToAdd);
        }
    }

    public void RemovePowerLinker(PowerLinkerComponent linkerToRemove)
    {
        if (connectedPowerLinkers.Contains(linkerToRemove))
        {
            connectedPowerLinkers.Remove(linkerToRemove);
        }
    }

    [ContextMenu("RecursiveGetPowerLinkers")]
    public List<PowerLinkerComponent> RecursiveGetPowerLinkers()
    {
        return GetComponent<PowerLinkerComponent>().GetConnectedLinkerComponents();
    }
    [ContextMenu("GetPowerComponents")]

    public List<PowerComponent> GetPowerComponents()
    {
        List<PowerComponent> pcs = new List<PowerComponent>();
        connectedPowerLinkers = shipPowerLinker.GetConnectedLinkerComponents();
        connectedPowerLinkers.Add(shipPowerLinker);

        for (int i = 0; i < connectedPowerLinkers.Count; i++)
        {
            pcs.AddRange(connectedPowerLinkers[i].directConnectedPowerComponents);
        }
        return pcs;
    }

    public double CalculatePowerUsage()
    {
        
        float powerTally = 0;
        for(int i = 0; i < connectedPowerComponents.Count; i++)
        {
            if (connectedPowerComponents[i])
            {
                connectedPowerComponents[i].isPowered = (currentCapacity > 0);
                    if (connectedPowerComponents[i].isOn)
                        powerTally -= connectedPowerComponents[i].powerConsumptionPerDay - connectedPowerComponents[i].powerOutputPerDay;
            }
        }
        return powerTally;
        //float powerTally = 0;
        //for(int i = 0; i < connectedPowerLinkers.Count; i++)
        //{
        //    if (connectedPowerLinkers[i])
        //    {
        //        for (int j = 0; j < connectedPowerLinkers[i].directConnectedPowerComponents.Count; j++)
        //        {
        //            connectedPowerLinkers[i].directConnectedPowerComponents[j].isPowered = (currentCapacity > 0);
        //            if (connectedPowerLinkers[i].directConnectedPowerComponents[j].isOn)
        //                powerTally -= connectedPowerLinkers[i].directConnectedPowerComponents[j].powerConsumptionPerDay - connectedPowerLinkers[i].directConnectedPowerComponents[j].powerOutputPerDay;
        //        }
        //    }
        //}
        //return powerTally;
    }

    public float GetShipPowerPercentage()
    {
        return Mathf.InverseLerp(0, maxCapacity, (float)currentCapacity);
    }

    public double GetDepletionTime(WorldTimeManager._time t = WorldTimeManager._time.seconds)
    {
        if(powerDrain > 0)
        {
            return 0;
        }
        return WorldTimeManager.ConvertTime(WorldTimeManager._time.days, t, (currentCapacity / -powerDrain));
    }

    public double GetChargeTime(WorldTimeManager._time t = WorldTimeManager._time.seconds)
    {
        if(powerDrain < 0)
        {
            return 0;
        }
        return WorldTimeManager.ConvertTime(WorldTimeManager._time.days, t, ((maxCapacity - currentCapacity) / powerDrain));
    }

    public bool IsDepleting()
    {
        return (powerDrain < 0);
    }
}
