﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeShip : MonoBehaviour {
    PowerLinkerComponent m_powerLinker;
    PowerPlantComponent m_powerPlant;
    ResourceContainerComponent m_shipResources;


    [System.Serializable]
    public class _startingResources
    {
        public ResourceItem resource;
        public int startingAmount;
    }

    [Header("POWER")]
    public float maxPowerCapacity;
    public float startingPowerCapcaity;
    public float defaultShipPowerDrainPerDay;
    public List<PowerComponent> startingPowerComponents;
    [Header("RESOURCES")]
    public List<_startingResources> startingResources;

    [Header("BLUEPRINTS")]
    public List<StructureBluePrint> unlockedBluePrints = new List<StructureBluePrint>();
    public List<StructureBluePrint> activeBluePrints = new List<StructureBluePrint>();

    // Use this for initialization
    void Start () {
        m_powerLinker = GetComponent<PowerLinkerComponent>();
        m_powerPlant = GetComponent<PowerPlantComponent>();
        m_shipResources = GetComponent<ResourceContainerComponent>();

        m_powerPlant.maxCapacity = maxPowerCapacity;
        m_powerPlant.currentCapacity = startingPowerCapcaity;
        foreach(PowerComponent pc in startingPowerComponents)
        {
            if(m_powerLinker != null)
                m_powerLinker.AddPowerComponent(pc);
        }

        m_powerPlant.shipPowerLinker = m_powerLinker;

        GetComponent<PowerComponent>().powerConsumptionPerDay = defaultShipPowerDrainPerDay;

        foreach (_startingResources r in startingResources)
        {
            m_shipResources.AddResources(r.resource, r.startingAmount);
        }

        foreach(StructureBluePrint blueprint in activeBluePrints)
        {
            CharacterInstanceManager.instance.buildController.AddBlueprint(blueprint);
        }
        Destroy(this);

    }
}
