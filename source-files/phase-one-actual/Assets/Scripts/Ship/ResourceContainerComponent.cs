﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ResourceContainerComponent : MonoBehaviour {
    
    [System.Serializable]
    public class ResourceSlot
    {
        [SerializeField]
        ResourceItem resource;
        [SerializeField]
        int amount;
        public ResourceItem Resource
        {
            get
            {
                return resource;
            }
            set
            {
                resource = value;
                if (UpdateSlot != null)
                {
                    UpdateSlot();
                }
            }
        }
        public int Amount
        {
            get{
                return amount;
            }
            set
            {
                amount = value;
                if (UpdateSlot != null)
                {
                    UpdateSlot();
                }
            }
        }

        public delegate void slotChanged();
        public slotChanged UpdateSlot;
    }

    public delegate void listChange();
    public listChange resourceListChanged;
    [SerializeField]
    List<ResourceSlot> resourceList = new List<ResourceSlot>();

    public List<ResourceSlot> ResourceList
    {
        get
        {
            return resourceList;
        }
    }

    public void AddResources(ResourceItem resource, int amount)
    {
        ResourceSlot slot = FindExistingResource(resource);

        if(slot != null)
        {
            slot.Amount += amount;
        }
        else
        {
            slot = new ResourceSlot();
            slot.Resource = resource;

            slot.Amount = amount;
            resourceList.Add(slot);
            if(resourceListChanged != null)
            {
                resourceListChanged();
            }
        }
    }

    public bool HasResources(ResourceItem resource, int amount)
    {
        ResourceSlot slot = FindExistingResource(resource);

        if (slot != null && slot.Amount - amount >= 0){
            return true;
        }
        return false;
    }

    public bool RemoveResources(ResourceItem resource, int amount)
    {
        ResourceSlot slot = FindExistingResource(resource);

        if (slot != null)
        {
            if(slot.Amount - amount >= 0)
            {
                slot.Amount -= amount;
                if(slot.Amount == 0)
                {
                    resourceList.Remove(slot);
                    if(resourceListChanged != null)
                    {
                        resourceListChanged();
                    }
                }
                return true;
            }
        }
        return false;
    }

    public int RemoveResourcesOneByOne(ResourceItem resource, int amount)
    {
        ResourceSlot slot = FindExistingResource(resource);

        if (slot != null)
        {
            if (slot.Amount - amount >= 0)
            {
                slot.Amount -= amount;
                amount = 0;
            }
            else
            {
                amount -= slot.Amount;
                slot.Amount = 0;
            }

            if (slot.Amount == 0)
            {
                resourceList.Remove(slot);
                if (resourceListChanged != null)
                {
                    resourceListChanged();
                }
            }
        }
        return amount;
    }

    public ResourceSlot FindExistingResource(ResourceItem toFind)
    {
        for(int i = 0; i < resourceList.Count; i++)
        {
            if(resourceList[i].Resource == toFind)
            {
                return resourceList[i];
            }
        }
        return null;
    }
}
