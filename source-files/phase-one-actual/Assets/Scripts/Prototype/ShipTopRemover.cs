﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipTopRemover : MonoBehaviour {

    public GameObject obj;
    public GameObject comp;
    public GameObject walls;
    public float dist;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //obj.SetActive((Vector3.Distance(obj.transform.position, Prototype_CharacterController.instance.transform.position) > dist));
        //comp.SetActive(!obj.activeSelf);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.root.GetComponent<CharController>())
        {
            obj.SetActive(false);
            comp.SetActive(true);
            walls.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
		if (other.transform.root.GetComponent<CharController>())
        {
            obj.SetActive(true);
            comp.SetActive(false);
            walls.SetActive(false);
        }
    }
}
