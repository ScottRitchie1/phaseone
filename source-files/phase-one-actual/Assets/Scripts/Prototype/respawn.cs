﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class respawn : MonoBehaviour {

    public static respawn instance;
    public Vector2 randomCloneBirthTimeRange = new Vector2(20, 30);
    public double nextCloneBirthTime = 35;
    public double nextCloneStartTime;
    public Transform respawnPoint;

    public float deathvelocity = 14;

    public Material characterDeathMaterial;

    [Header("CAMERA LOOK SETTINGS")]
    public float magnetideThreshhold = 0.5f;
    public float cameraLookTime = 5;
    public float dayCycleSpeed = 1200;
    public float dayCycleSpeedLerp = 5;

    [Header("DayCycle Settings")]
    public float dayCycleSpeedUpDelay;
    public float dayCycleSpeedUpTime;
    public float dayCycleSpeedUpAmount;

    [Header("WorldTime Settings")]
    public float worldTimeSpeedUpDelay;
    public float worldTimeSpeedUpRate;


    // Use this for initialization
    private void Awake()
    {
        instance = this;
    }

    void Start () {
        NewCloneBirthYear();
        CharController.instance.transform.position = respawnPoint.transform.position;
        CharController.instance.transform.rotation = respawnPoint.transform.rotation;
        if(dayCycleSpeedUpAmount < 1)
        {
            dayCycleSpeedUpAmount = 1;
        }

        CharacterInstanceManager.instance.controller.onGrounded += CheckFallDamage;
    }

    private void OnDestroy()
    {
        CharacterInstanceManager.instance.controller.onGrounded -= CheckFallDamage;

    }

    // Update is called once per frame
    void Update () {
        //nextCloneText.text = "NEW CLONE IN: " + WorldTimeManager.TimeString(nextCloneBirthTime - WorldTimeManager.currentTime);
        if(CharacterInstanceManager.instance.transform.position.y < 0)
        {
            CharController.instance.transform.position = respawnPoint.transform.position;
            CharController.instance.transform.rotation = respawnPoint.transform.rotation;
        }
    
    }

    public void CheckFallDamage()
    {
        if (CharacterInstanceManager.instance.rb.velocity.magnitude > deathvelocity)// OLD CODE REMOVE AND MAKE BETTER DIE FUNCTION
        {
            Respawn();
        }
    }

    public void Respawn()
    {
        StartCoroutine("respawnCoroutine");
    }

    void NewCloneBirthYear()
    {
        nextCloneStartTime = WorldTimeManager.currentTime;
           nextCloneBirthTime = WorldTimeManager.currentTime + WorldTimeManager.ConvertTime(WorldTimeManager._time.years, WorldTimeManager._time.seconds, Random.Range(randomCloneBirthTimeRange.x, randomCloneBirthTimeRange.y));

    }

    public float GetCloneCompletePercentage()
    {
        return WorldTimeManager.TimeInverseLerp(nextCloneStartTime, nextCloneBirthTime);
    }

    IEnumerator respawnCoroutine()
    {
        StartCoroutine("dayCycleRespawnCoroutine");
        StartCoroutine("worldTimeRespawnCoroutine");
        CharacterInstanceManager.instance.animatorController.ToRagDoll();
        CharacterInstanceManager.instance.gameObject.SetActive(false);
        Debug.Log("Waiting for character to stop moving");
        Rigidbody ragdollRB = CharacterInstanceManager.instance.animatorController.activeRagdoll.ragDollBones[0].GetComponent<Rigidbody>();
        while (ragdollRB.velocity.magnitude > magnetideThreshhold)
        {
            yield return null;
        }
        yield return new WaitForSeconds(1);
        CamController.instance.trackTarget = false;
        Debug.Log("Waiting for camera to look at the sun");
        Vector3 CurLookPos = CamController.instance.transform.forward;

        Quaternion targetLookRotation = Quaternion.LookRotation(ShipInstanceManager.instance.transform.position - CamController.instance.transform.position, Vector3.up);
        while (CamController.instance.transform.rotation != targetLookRotation)
        {
            CamController.instance.transform.rotation = Quaternion.RotateTowards(CamController.instance.transform.rotation, targetLookRotation, Time.deltaTime * cameraLookTime);
            yield return null;
        }
        Debug.Log("Waiting for seconds");

        yield return new WaitForSeconds(5);
        CamController.instance.trackTarget = true;
        if(CharacterInstanceManager.instance.animatorController.activeRagdoll)
            CharacterInstanceManager.instance.animatorController.activeRagdoll.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material = characterDeathMaterial;
        CharacterInstanceManager.instance.animatorController.ResetRagDoll();
        CharacterInstanceManager.instance.gameObject.SetActive(true);
        CharacterInstanceManager.instance.rb.velocity = Vector3.zero;

        WorldTimeManager.ModifyTime(nextCloneBirthTime - WorldTimeManager.currentTime);
        NewCloneBirthYear();
        StopCoroutine("dayCycleRespawnCoroutine");
        StopCoroutine("worldTimeRespawnCoroutine");
        DayCycleManager.instance.useWorldTime = true;
        WorldTimeManager.timeScale = 1;
        CharController.instance.transform.position = respawnPoint.transform.position;
        CharController.instance.transform.rotation = respawnPoint.transform.rotation;
    }

    IEnumerator dayCycleRespawnCoroutine()
    {
        float cycleTime = (float)WorldTimeManager.ConvertTime(WorldTimeManager._time.hours, WorldTimeManager._time.seconds, WorldTimeManager.hoursInADay);
        DayCycleManager.instance.cycleTime = cycleTime;
        DayCycleManager.instance.useWorldTime = false;
        yield return new WaitForSeconds(dayCycleSpeedUpDelay);
        float vel = 0;
        while(Mathf.Round(DayCycleManager.instance.cycleTime*1000)/1000 > dayCycleSpeedUpAmount)
        {
            DayCycleManager.instance.cycleTime = Mathf.SmoothDamp(DayCycleManager.instance.cycleTime, dayCycleSpeedUpAmount, ref vel, dayCycleSpeedUpTime/10);
            yield return null;
        }
        DayCycleManager.instance.cycleTime = dayCycleSpeedUpAmount;
    }

    IEnumerator worldTimeRespawnCoroutine()
    {

        yield return new WaitForSeconds(worldTimeSpeedUpDelay);

        do
        {
            yield return null;
            WorldTimeManager.timeScale += 100;
            Debug.Log("WorldTimeManager.timeScale: " + WorldTimeManager.timeScale);
        } while (WorldTimeManager.currentTime + WorldTimeManager.timeScale * Time.deltaTime < nextCloneBirthTime);

        WorldTimeManager.ModifyTime(nextCloneBirthTime - WorldTimeManager.currentTime);
    }
}
