﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stage
{
    public int year;
    public string name = "";
    public GameObject stage;
    public Vector2Int organicsRange;
    public Vector2Int h20Range;
    public Vector2Int compoundRange;
    public Vector2Int energyRange;
    public Vector2Int mineralRange;

}

public class Prototype_Interactable : InteractableEntityBase {

    public int initialYear = 0;

    public Stage currentStage;

    public List<Stage> stages = new List<Stage>();

    public bool destroyOnHarvest = true;
    public bool resetStage;

	// Use this for initialization
	void Start () {
        foreach(Stage stg in stages)
        {
            stg.stage.SetActive(false);
        }
        currentStage = stages[0];
        currentStage.stage.SetActive(true);
        if(initialYear == 0 && Random.Range(0f, 1f) > 0.2f)
        {
            initialYear = (int)Random.Range(-stages[stages.Count - 1].year,  0);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (stages.Count > 1)
        {
            foreach (Stage stg in stages)
            {
                if(stg != currentStage && WorldTimeManager.GetCurrentTime(WorldTimeManager._time.years) > initialYear + stg.year)
                {
                    currentStage.stage.SetActive(false);
                    currentStage = stg;
                    currentStage.stage.SetActive(true);
                }
            }
        }
	}

    public override void OnHarvest()
    {
        Debug.LogError("THIS NO LONGER WORKS BECASUE I COULDNT BE FUCKED PDATING IT");
        //ResourceContainerSystem.instance.organics += Random.Range(currentStage.organicsRange.x, currentStage.organicsRange.y);
        //ResourceContainerSystem.instance.h20 += Random.Range(currentStage.h20Range.x, currentStage.h20Range.y);
        //ResourceContainerSystem.instance.compound += Random.Range(currentStage.compoundRange.x, currentStage.compoundRange.y);
        //ResourceContainerSystem.instance.energy += Random.Range(currentStage.energyRange.x, currentStage.energyRange.y);
        //ResourceContainerSystem.instance.mineral += Random.Range(currentStage.mineralRange.x, currentStage.mineralRange.y);

        if (destroyOnHarvest)
        {
            Destroy(gameObject);
        }
        else
        {
            if (resetStage)
            {
                initialYear = Mathf.FloorToInt(WorldTimeManager.GetCurrentTime(WorldTimeManager._time.years));
                currentStage.stage.SetActive(false);
                currentStage = stages[0];
                currentStage.stage.SetActive(true);
            }
        }
    }

    public override void OnScan()
    {
        HarvestableEntityBase._harvestableStage stage = new HarvestableEntityBase._harvestableStage();
        stage.stageName = "NOT SUPPORTED";
        //stage.stageDrops = new List<HarvestableEntityBase._resourceDropQuantity>();
        Prototype_ScanUI.instance.ScanItem(stage);
    }
}
