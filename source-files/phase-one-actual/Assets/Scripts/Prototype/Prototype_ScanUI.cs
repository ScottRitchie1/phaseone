﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Prototype_ScanUI : MonoBehaviour {

    public static Prototype_ScanUI instance;

    public Canvas scanUI;
    public Text scanName;
    public Text scanElement;
    public LayoutGroup scanElementContainer;
    public float displayTime = 1;

    void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("Prototype_ScanUI instance already set."); }
    }

    private void Start()
    {
        scanUI = GetComponentInChildren<Canvas>();
        scanUI.enabled = false;
    }

    public void ScanItem(HarvestableEntityBase._harvestableStage item)
    {
        if(scanUI.enabled == true)
            StopCoroutine("ScanCoroutine");

        if (item.stageName != "")
        {
            scanName.text = item.stageName;
        }
        else
        {
            scanName.text = "unknown";
        }

        foreach (Text child in scanElementContainer.GetComponentsInChildren<Text>())
        {
            Destroy(child.gameObject);
        }

        if (item.stageDrops.Count > 0)
        {
            for (int i = 0; i < item.stageDrops.Count; i++)
            {
                Text tempScanElement = Instantiate(scanElement, scanElementContainer.transform.position, scanElementContainer.transform.rotation, scanElementContainer.transform);
                tempScanElement.text = item.stageDrops[i].resource.itemName + ": " + item.stageDrops[i].dropAmount;
            }
        }
        StartCoroutine("ScanCoroutine", displayTime);
    }

    public void ScanItem(CollectableResourceContainer item)
    {
        if (scanUI.enabled == true)
            StopCoroutine("ScanCoroutine");

        if (item.containerName != "")
        {
            scanName.text = item.containerName;
        }
        else
        {
            scanName.text = "Unknown Resource Container";
        }

        foreach (Text child in scanElementContainer.GetComponentsInChildren<Text>())
        {
            Destroy(child.gameObject);
        }

        if (item.containerDrops.Count > 0)
        {
            for (int i = 0; i < item.containerDrops.Count; i++)
            {
                Text tempScanElement = Instantiate(scanElement, scanElementContainer.transform.position, scanElementContainer.transform.rotation, scanElementContainer.transform);
                tempScanElement.text = item.containerDrops[i].resource.itemName + ": " + item.containerDrops[i].dropAmount;
            }
        }
        StartCoroutine("ScanCoroutine", displayTime);
    }

    IEnumerator ScanCoroutine(float time)
    {
        scanUI.enabled = true;
        yield return new WaitForSeconds(time);
        scanUI.enabled = false;
    }
}
