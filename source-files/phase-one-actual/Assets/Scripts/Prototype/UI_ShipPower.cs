﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_ShipPower : MonoBehaviour {

    public PowerPlantComponent powerPlantComponent;
    public Text text;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (powerPlantComponent)
        {
            text.text = "POWER: " + System.Math.Round(powerPlantComponent.currentCapacity, 3) + " OF " + powerPlantComponent.maxCapacity + " = " + ((Mathf.Round(powerPlantComponent.GetShipPowerPercentage() * 1000)) * 0.1f);
            if (powerPlantComponent.IsDepleting())
            {
                text.text += "% \n DEPLETES IN: " + string.Format(WorldTimeManager.TimeString(powerPlantComponent.GetDepletionTime()), "Y:", "D:", " H:", " M:", " S:");
            }
            else
            {
                text.text += "% \n CHARGED IN: " + string.Format(WorldTimeManager.TimeString(powerPlantComponent.GetChargeTime()), "Y:", "D:", " H:", " M:", " S:");

            }
        }
	}
}
