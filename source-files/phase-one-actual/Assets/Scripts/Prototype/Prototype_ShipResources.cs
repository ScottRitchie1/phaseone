﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Prototype_ShipResources : MonoBehaviour {

    public class Prototype_ResourceSlot{
        public ResourceContainerComponent.ResourceSlot slot;
        public Text slotText;

        public void UpdateSlot()
        {
            slotText.text = slot.Resource.name + " " + slot.Amount;
        }
    }

    

    public Text slotPrefab;
    public RectTransform slotContainer;
    [SerializeField]
    List<Prototype_ResourceSlot> slotList = new List<Prototype_ResourceSlot>();

    private void Start()
    {
        ShipInstanceManager.instance.shipResourceContainer.resourceListChanged += UpdateSlots;
        UpdateSlots();
    }

    private void OnDisable()
    {
        ShipInstanceManager.instance.shipResourceContainer.resourceListChanged -= UpdateSlots;
    }

    void UpdateSlots()
    {
        List<Prototype_ResourceSlot> toSearch = new List<Prototype_ResourceSlot>(slotList);

        foreach (ResourceContainerComponent.ResourceSlot slot in ShipInstanceManager.instance.shipResourceContainer.ResourceList)
        {
            bool foundUISlot = false;
            foreach(Prototype_ResourceSlot uiSlot in toSearch)
            {
                if(uiSlot.slot == slot)
                {
                    foundUISlot = true;
                    toSearch.Remove(uiSlot);
                }

                if(foundUISlot == true)
                {
                    break;
                }
            }

            if(foundUISlot == false)
            {
                Prototype_ResourceSlot newSlot = new Prototype_ResourceSlot();
                newSlot.slot = slot;
                newSlot.slot.UpdateSlot += newSlot.UpdateSlot;
                newSlot.slotText = Instantiate(slotPrefab, slotContainer.position, slotContainer.rotation, slotContainer);
                newSlot.UpdateSlot();
                slotList.Add(newSlot);

            }
        }

        foreach (Prototype_ResourceSlot uiSlot in toSearch)
        {
            Destroy(uiSlot.slotText.gameObject);
            slotList.Remove(uiSlot);
        }
    }
}
