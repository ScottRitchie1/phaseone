﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycleManager : MonoBehaviour {
    public static DayCycleManager instance;


    public Transform rotater;
    public Light dayLight;
    AuraAPI.AuraLight auraLight;

    [Header("Settings")]
    public Gradient colourCurve;
    public AnimationCurve dayLightIntensity;
    public AnimationCurve ambientLightIntensityCurve;
    public AnimationCurve nightFogWeight;
    [Space]
    [Range(0.1f, 0.9f)]
    public float dayPercentage;
    float dayNightSmoothDampVel;
    [Range(-60,60)]
    public float sunArcAngle;
    [Range(-180, 180)]
    public float sunRiseRotation;

    [Range(0, 1)]
    public float nightTimeDarknessCap;

    [Space]
    public bool useWorldTime = true;
    public float cycleTime;
    float cycleTimeWeight;

    [Range(0, 1)]
    public float weight;

    private void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("DayCycleController instance already set."); }
    }
    void Start () {
        if (cycleTime <= 0)
            cycleTime = 1;

        RenderSettings.sun = dayLight;
        auraLight = dayLight.GetComponent<AuraAPI.AuraLight>();
        RenderSettings.fog = false;
        RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;
	}
	
	// Update is called once per frame
	void Update () {
        if (useWorldTime)//only calculate our own weight if we dont want to use the worldTimeCycle
        {
            UpdateDayNightWeight(WorldTimeManager.dayPercentage);
            cycleTimeWeight = WorldTimeManager.dayPercentage;
        }
        else {
            cycleTimeWeight = (cycleTimeWeight + ((Time.deltaTime / cycleTime))) % 1;
            UpdateDayNightWeight(cycleTimeWeight);
        }


        rotater.localEulerAngles = new Vector3(Mathf.Lerp(0, 360, weight)- 90, sunRiseRotation, 0);
        transform.localEulerAngles = Vector3.forward * sunArcAngle;


        dayLight.intensity = dayLightIntensity.Evaluate(weight);
        dayLight.color = colourCurve.Evaluate(weight);
        RenderSettings.ambientIntensity = Mathf.Max(ambientLightIntensityCurve.Evaluate(weight), nightTimeDarknessCap);
        RenderSettings.ambientLight = dayLight.color;
        RenderSettings.reflectionIntensity = RenderSettings.ambientIntensity;
        auraLight.strength = nightFogWeight.Evaluate(weight);
    }

    void UpdateDayNightWeight(float w)
    {
        float dayStartPoint, dayEndPoint;
        dayStartPoint = 0.5f - (dayPercentage / 2);
        dayEndPoint = 0.5f + (dayPercentage / 2);
        if (w > dayStartPoint && w < dayEndPoint)
        {
            weight = Mathf.SmoothStep(0.25f, 0.75f, Mathf.InverseLerp(dayStartPoint, dayEndPoint, w));
        }
        else
        {
            if (w < dayStartPoint)
            {
                weight = Mathf.SmoothStep(0, 0.25f, Mathf.InverseLerp(0, dayStartPoint, w));
            }
            else
            {
                weight = Mathf.SmoothStep(0.75f, 1, Mathf.InverseLerp(dayEndPoint, 1, w));
            }
        }
    }

    public bool isNight()
    {
        return (weight < 0.25f || weight > 0.75f);
    }

}
