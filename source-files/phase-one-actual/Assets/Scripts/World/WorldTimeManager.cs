﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class WorldTimeManager : MonoBehaviour {

    public enum _time { seconds, minutes, hours, days, years};

    public double startTime;
    
    public static float timeScale = 1;
    [SerializeField]
    public static double currentTime;
    public static double deltaTime;
    double prevTime;


    [SerializeField]
    float hoursPerDay = 23.9345f;
    [SerializeField]
    float daysPerYear = 365.2422f;

    public static float hoursInADay;
    public static float daysInAYear;


    [Range(0, 1)]
    public static float minutePercentage;
    [Range(0, 1)]
    public static float hourPercentage;
    [Range(0, 1)]
    public static float dayPercentage;
    [Range(0, 1)]
    public static float yearPercentage;

    private void Awake()
    {
        hoursInADay = hoursPerDay;
        daysInAYear = daysPerYear;
    }

	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
        hoursInADay = hoursPerDay;
        daysInAYear = daysPerYear;
#endif
        currentTime += Time.deltaTime * timeScale;
        deltaTime = currentTime - prevTime;
        prevTime = currentTime;

        yearPercentage = GetTimePercentage(_time.years);
        minutePercentage = GetTimePercentage(_time.minutes);
        hourPercentage = GetTimePercentage(_time.hours);
        dayPercentage = GetTimePercentage(_time.days);
    }

    public static string TimeString(double time, bool timeCumulative = false)
    {
        if (!timeCumulative)
        {
            return "{0} " + System.Math.Floor(time * GetTimeMultiplier_FromSecondsTo(_time.years))
                + " {1} " + System.Math.Floor(time * GetTimeMultiplier_FromSecondsTo(_time.days) % daysInAYear)
                + " {2} " + System.Math.Floor(time * GetTimeMultiplier_FromSecondsTo(_time.hours) % hoursInADay)
                + " {3} " + System.Math.Floor(time * GetTimeMultiplier_FromSecondsTo(_time.minutes) % 60)
                + " {4} " + System.Math.Floor(time % 60);
        }
        else
        {
            return "{0} " + System.Math.Floor(time * GetTimeMultiplier_FromSecondsTo(_time.years))
                + " {1} " + System.Math.Floor(time * GetTimeMultiplier_FromSecondsTo(_time.days))
                + " {2} " + System.Math.Floor(time * GetTimeMultiplier_FromSecondsTo(_time.hours))
                + " {3} " + System.Math.Floor(time * GetTimeMultiplier_FromSecondsTo(_time.minutes))
                + " {4} " + System.Math.Floor(time);

        }
    }

    public static float GetCurrentTime(_time t)
    {
        switch (t)
        {
            case _time.minutes:
                return Mathf.Floor((float)(currentTime / GetTimeMultiplier_ToSecondsFrom(t)) % 60);
            case _time.hours:
                return Mathf.Floor((float)(currentTime / GetTimeMultiplier_ToSecondsFrom(t)) % hoursInADay);
            case _time.days:
                return Mathf.Floor((float)(currentTime / GetTimeMultiplier_ToSecondsFrom(t)) % daysInAYear);
            case _time.years:
                return Mathf.Floor((float)(currentTime / GetTimeMultiplier_ToSecondsFrom(t)));
            default: return (float)(currentTime) % 60;
        }
    }

    public static double GetCurrentTimeCumulative(_time t)
    {
        return currentTime / GetTimeMultiplier_ToSecondsFrom(t);
    }

    public static float GetTimePercentage(_time t)
    {
        switch (t)
        {
            case _time.minutes:
                return Mathf.InverseLerp(0, 60, (float)(GetCurrentTimeCumulative(_time.seconds) % 60));
            case _time.hours:
                return Mathf.InverseLerp(0, 60, (float)(GetCurrentTimeCumulative(_time.minutes) % 60));
            case _time.days:
                return Mathf.InverseLerp(0, hoursInADay, (float)(GetCurrentTimeCumulative(_time.hours) % hoursInADay));
            case _time.years:
                return Mathf.InverseLerp(0, daysInAYear, (float)(GetCurrentTimeCumulative(_time.days) % daysInAYear));

            default:
                {
                    Debug.LogError("Seconds doesnt have a weight.");
                    return 0;
                }
        }
    }

    public static void ModifyTime(double change)
    {
        currentTime += change;
    }

    public static void ModifyTime(float change, _time t)
    {
        currentTime += change * GetTimeMultiplier_ToSecondsFrom(t);
    }

    public static double GetTimeMultiplier_ToSecondsFrom(_time convertFrom)
    {
        switch (convertFrom)
        {
            case _time.minutes:
                return 60d;
            case _time.hours:
                return 3600d;
            case _time.days:
                return 3600d * hoursInADay;
            case _time.years:
                return 3600d * hoursInADay * daysInAYear;
            default:
                return 1d;
        }
    }

    public static double GetTimeMultiplier_FromSecondsTo(_time convertTo)
    {
        switch (convertTo)
        {
            case _time.minutes:
                return 1.0d / 60;
            case _time.hours:
                return 1.0d / 3600;
            case _time.days:
                return 1.0d / 3600 / hoursInADay;
            case _time.years:
                return 1.0d / 3600 / hoursInADay / daysInAYear;
            default:
                return 1.0d;
        }
    }

    public static double ConvertTime(_time convertFrom, _time convertTo, double value)
    {
        return value * GetTimeMultiplier(convertFrom, convertTo);
    }

    public static double GetTimeMultiplier(_time convertFrom, _time convertTo)
    {
        return GetTimeMultiplier_ToSecondsFrom(convertFrom) * GetTimeMultiplier_FromSecondsTo(convertTo);
    }

    public static float TimeInverseLerp(double a, double b, int places = 3)
    {
        return Mathf.Clamp01((float)Math.Round((currentTime - a) / (b - a), places));
    }
}
