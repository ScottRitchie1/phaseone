﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Debug_UI_Handler : MonoBehaviour {

    public InputField changeTimeInput;
    public Text timeScaleText;
    public Text timeText;
    bool timeDisplayType;
    public Toggle timeDisplayTypeToggle;
    public Text timeCycleDisplay;
    bool isActive;
	// Use this for initialization
	void Start () {
#if !UNITY_EDITOR
        //Destroy(gameObject);
#endif

        WorldTimeManager.ModifyTime(12, WorldTimeManager._time.hours);
    }
	
	// Update is called once per frame
	void Update () {
        transform.GetChild(0).gameObject.SetActive(isActive);
        if (Input.GetKeyDown(KeyCode.Escape)) isActive = !isActive;


        if (timeScaleText) timeScaleText.text = "     TimeScale: " + WorldTimeManager.timeScale.ToString();
        if (timeText) timeText.text = string.Format(WorldTimeManager.TimeString(WorldTimeManager.currentTime, timeDisplayType), "YEARS:", "\nDAYS:", "\nHOURS:", "\nMINUTES:", "\nSECONDS:");
        if(timeDisplayTypeToggle) timeDisplayType = timeDisplayTypeToggle.isOn;
        if (timeCycleDisplay) timeCycleDisplay.text = "HOURS IN DAY: " + WorldTimeManager.hoursInADay.ToString() + "\nDAYS IN YEAR: " + WorldTimeManager.daysInAYear.ToString();

        if (Input.GetKeyDown(KeyCode.P) && CharacterInstanceManager.instance.gameObject.activeInHierarchy)
        {
            CharacterInstanceManager.instance.animatorController.ResetRagDoll();
            CharacterInstanceManager.instance.animatorController.ToRagDoll();
        }
        else if (Input.GetKeyDown(KeyCode.O) && CharacterInstanceManager.instance.gameObject.activeInHierarchy)
        {
            CharacterInstanceManager.instance.animatorController.ResetRagDoll();
        }
    }

    public void ChangeTimeYears()
    {
        float result;
        if(changeTimeInput)WorldTimeManager.ModifyTime( (float.TryParse(changeTimeInput.text, out result) ? result : 0.0f), WorldTimeManager._time.years);
    }


    public void ChangeTimeDays()
    {
        float result;
        if (changeTimeInput) WorldTimeManager.ModifyTime((float.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.days);
    }

    public void ChangeTimeHours()
    {
        float result;
        if (changeTimeInput) WorldTimeManager.ModifyTime((float.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.hours);
    }

    public void ChangeTimeMinutes()
    {
        float result;
        if (changeTimeInput) WorldTimeManager.ModifyTime((float.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.minutes);
    }

    public void ChangeTimeSeconds()
    {
        float result;
        if (changeTimeInput) WorldTimeManager.ModifyTime((float.TryParse(changeTimeInput.text, out result) ? result : 0), WorldTimeManager._time.seconds);
    }

    public void ChangeTimeScale(float t)
    {
        float newScale = t * WorldTimeManager.hoursInADay;
        if (newScale < 1 && newScale > -1)
        {
            if (newScale > 0)
            {
                WorldTimeManager.timeScale = 1;
            }
            else
            {
                WorldTimeManager.timeScale = -1;
            }
        }
        else
        {
            WorldTimeManager.timeScale = t * WorldTimeManager.hoursInADay;
        }
    }

    public void Respawn()
    {
        respawn.instance.Respawn();
    }
    public void ResetGame()
    {
        SceneManager.LoadScene(0);
    }

    public void AddResource()
    {
        List<ResourceItem> toAdd = new List<ResourceItem>(); ;
        foreach(ResourceContainerComponent.ResourceSlot slot in ShipInstanceManager.instance.shipResourceContainer.ResourceList)
        {
            toAdd.Add(slot.Resource);
        }

        foreach (ResourceItem item in toAdd)
        {
            ShipInstanceManager.instance.shipResourceContainer.AddResources(item, 5);
        }

    }

    public void RemoveResource()
    {
        List<ResourceItem> toRemove = new List<ResourceItem>(); ;
        foreach (ResourceContainerComponent.ResourceSlot slot in ShipInstanceManager.instance.shipResourceContainer.ResourceList)
        {
            toRemove.Add(slot.Resource);
        }

        foreach (ResourceItem item in toRemove)
        {
            ShipInstanceManager.instance.shipResourceContainer.RemoveResources(item, 5);
        }
    }

}
