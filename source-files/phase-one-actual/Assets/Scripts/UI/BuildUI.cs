﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class BuildUI : MonoBehaviour {

    public Image iconImage;
    public Text structureNameText;
    public RectTransform canBuildElement;
    public RectTransform notConnectedElement;
    public RectTransform cannotBuildElement;
    public Text cannonBuildReasonText;
    public RectTransform buildTimeElement;
    public Text buildTimeDaysText;
    public Text buildTimeHoursText;
    public Text buildTimeMinutesText;

    public Transform buildPont;

    public Vector2 uiOffset;

    public void UpdateOffset(Vector2 offset) {
        uiOffset = offset;
    }

    private void LateUpdate()
    {
        if (buildPont)
        {
            transform.rotation = Quaternion.LookRotation(transform.position - CamController.instance.transform.position, Vector3.up);
            transform.position = buildPont.position + (Vector3.up * uiOffset.y) + (transform.right * uiOffset.x);
        }
    }

    public void UpdateIcon(Sprite icon = null){
        if (iconImage){
            if (icon){
                iconImage.rectTransform.gameObject.SetActive(true);
                iconImage.sprite = icon;
            }
            else{
                iconImage.rectTransform.gameObject.SetActive(false);
            }
        }
    }

    public void UpdateStructureName(string name = "Unknown Structure")
    {
        if (structureNameText)
        {
            structureNameText.text = name;
        }
    }

    public void UpdateBuildTime(string buildTime = "")
    {
        if (buildTimeElement)
        {
            if(buildTime != "")
            {
                buildTimeElement.gameObject.SetActive(true);
                string[] seperators = new string[] { "{*}" };
                string[] stringArr = Regex.Split(buildTime, @"\{.*?\}");
                if (buildTimeDaysText && stringArr.Length > 0) buildTimeDaysText.text = stringArr[1];
                if (buildTimeHoursText && stringArr.Length > 1) buildTimeHoursText.text = stringArr[2];
                if (buildTimeMinutesText && stringArr.Length > 2) buildTimeMinutesText.text = stringArr[3];
            }
            else
            {
                buildTimeElement.gameObject.SetActive(false);
            }
        }
    }

    public void UpdateBuildStatus(bool canBuild, bool connectionStatus)
    {
        if (canBuild)
        {
            canBuildElement.gameObject.SetActive(connectionStatus);
            notConnectedElement.gameObject.SetActive(!connectionStatus);
            cannotBuildElement.gameObject.SetActive(false);
        }
        else
        {
            UpdateBuildStatus(false, "Error");
        }
    }

    public void UpdateBuildStatus(bool canBuild, string cantBuildReason)
    {
        if (!canBuild)
        {
            canBuildElement.gameObject.SetActive(false);
            notConnectedElement.gameObject.SetActive(false);
            cannotBuildElement.gameObject.SetActive(true);
            cannonBuildReasonText.text = cantBuildReason;
        }
        else
        {
            UpdateBuildStatus(true, false);
        }
    }
}
