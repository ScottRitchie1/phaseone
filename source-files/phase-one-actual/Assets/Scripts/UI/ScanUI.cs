﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ScanUI : MonoBehaviour {
    [System.Serializable]
    public class ResourceElementArgs
    {
        public string resourceName = "name";
        public string resourceAmount = "amount";
        public string resourceOutOf = "";
    }

    public static ScanUI instance;
    public float displayTime = 1;

    public ParticleSystem scanParticles;
    public ParticleSystem scanSmoke;

    Transform target;
    Canvas scanUI;
    public Vector2 uiOffset;
    public RectTransform resourceListElement;
    [Header("MainPanels")]
    public RectTransform scanInfoPanel;
    public RectTransform scanStatusPanel;
    [Space]
    public Text ScanStatusText;
    public Image iconImage;
    public Text nameTitleText;
    [Header("Connection Status")]
    public RectTransform connectionStatusElement;
    public RectTransform connectionStatusConnectedElement;
    public RectTransform connectionStatusNotConnectedElement;
    public RectTransform connectionStatusPoweredElement;
    public RectTransform connectionStatusNotPoweredElement;
    [Header("Error Message")]
    public RectTransform errorMessageElement;
    public Text errorMessageText;
    [Header("Resource Display")]
    public RectTransform resourceDisplayElement;
    public Text resourceDisplayHeadingText;
    public RectTransform resourceDisplayContainer;
    [Header("Output")]
    public RectTransform outputElement;
    public RectTransform outputContainer;
    [Header("Progress Panel")]
    public RectTransform progressPanelElement;
    RectTransform slideElement;
    public Slider progressBarSlider;
    public Text progressBarText;
    public Text progressBarValueText;
    public RectTransform progressTimeElement;
    public Text progressTimeDaysText;
    public Text progressTimeHoursText;
    public Text progressTimeMinutesText;

    [Header("ScanTextSolver")]
    public string hiddenRandomCharacters = ".-;&6#2";
    public float scanCompleteDisplayTime = 1f;
    public bool scanFailed = false;
    public bool stopScanFacingCamera = false;
    public bool isScanning = false;
    public bool isScanFacingCamera = false;
    public bool requiresSolve = false;


    public bool CanScan = true;
    public delegate void OnScanComplete(bool successful);
    public OnScanComplete onScanComplete;
    void Awake()
    {
        if (instance == null) { instance = this; }// set instance
        else { Debug.LogError("Prototype_ScanUI instance already set."); }
    }

    private void Start()
    {
        scanUI = GetComponentInChildren<Canvas>();
        scanUI.enabled = false;
        scanParticles.Stop();
        scanSmoke.Stop();
    }

    public void ScanObject(Transform obj, bool _requiresSolve, float solveTime = 0)
    {
        if(target == null && CanScan)
        {
            scanFailed = false;
            stopScanFacingCamera = false;
            isScanning = false;
            isScanFacingCamera = false;
            requiresSolve = false;

            target = obj;
            requiresSolve = _requiresSolve;
            StartCoroutine("ScanTargetCamera");

            if (requiresSolve)
            {
                StartCoroutine("scanTextElementSolver", solveTime);
            }

            StartCoroutine("coolDown");
        }
        else if (obj != null && obj != target)
        {
            stopScanFacingCamera = true;
            if (isScanning)
            {
                scanFailed = true;
            }
        }
                

        
    }

    public void UpdateScanEffect(MeshFilter scanMesh = null)
    {
        if (scanMesh != null)
        {

            scanParticles.transform.SetParent(null);
            scanSmoke.transform.SetParent(null);
            scanParticles.transform.position = scanMesh.transform.position;
            scanParticles.transform.rotation = scanMesh.transform.rotation;
            scanSmoke.transform.position = scanMesh.transform.position;
            scanSmoke.transform.rotation = scanMesh.transform.rotation;
            //scanParticles.transform.SetParent(scanMesh.transform);
            //scanSmoke.transform.SetParent(scanMesh.transform);

            ParticleSystem.ShapeModule shape = scanParticles.shape;
            shape.mesh = scanMesh.mesh;
            shape.scale = scanMesh.transform.lossyScale;
            shape = scanSmoke.shape;
            shape.mesh = scanMesh.mesh;
            shape.scale = scanMesh.transform.lossyScale;

        }
        else
        {
            scanParticles.transform.SetParent(transform);
            scanSmoke.transform.SetParent(transform);
        }
    }

    public void UpdateIcon(Sprite icon = null)
    {
        if (iconImage)
        {
            if (icon)
            {
                iconImage.gameObject.SetActive(true);
                iconImage.sprite = icon;
            }
            else
            {
                iconImage.gameObject.SetActive(false);
            }
        }
    }

    public void UpdateDisplayNameTitle(string name = "")
    {
        if (nameTitleText)
        {
            if(name != "")
            {
                nameTitleText.text = name;
            }
            else
            {
                nameTitleText.text = "Unknown Scanned Object";
            }
        }
    }

    public void UpdateConnectionStatus(bool display = false, bool isConnected = false, bool isPowered = false)
    {
        if (display)
        {
            connectionStatusElement.gameObject.SetActive(true);
            connectionStatusConnectedElement.gameObject.SetActive(isConnected);
            connectionStatusNotConnectedElement.gameObject.SetActive(!isConnected);
            connectionStatusPoweredElement.gameObject.SetActive(isPowered);
            connectionStatusNotPoweredElement.gameObject.SetActive(!isPowered);
        }
        else
        {
            connectionStatusElement.gameObject.SetActive(false);
        }
    }

    public void UpdateErrorMessage() {
        errorMessageElement.gameObject.SetActive(false);
    }

    public void UpdateErrorMessage(string message)
    {
        errorMessageElement.gameObject.SetActive(true);
        errorMessageText.text = message;
    }

    public void UpdateResourceDisplay()
    {
        resourceDisplayElement.gameObject.SetActive(false);
    }

    public void UpdateResourceDisplay(string heading, ResourceElementArgs[] resourceList)
    {
        resourceDisplayElement.gameObject.SetActive(true);
        resourceDisplayHeadingText.text = heading;
        foreach (Transform t in resourceDisplayContainer.transform)
        {
            Destroy(t.gameObject);
        }

        foreach(ResourceElementArgs resource in resourceList)
        {
            RectTransform resourceElement = Instantiate(resourceListElement, resourceDisplayContainer.transform.position, resourceDisplayContainer.transform.rotation, resourceDisplayContainer.transform);
            resourceElement.gameObject.SetActive(true);
            resourceElement.GetChild(0).GetComponent<Text>().text = resource.resourceName;
            resourceElement.GetChild(1).GetComponent<Text>().text = resource.resourceAmount;
            resourceElement.GetChild(2).GetComponent<Text>().text = resource.resourceOutOf;
        }
    }

    public void UpdateOutput()
    {
        outputElement.gameObject.SetActive(false);
    }

    public void UpdateOutput(ResourceElementArgs[] resourceList)
    {
        outputElement.gameObject.SetActive(true);
        foreach (Transform t in outputContainer.transform)
        {
            Destroy(t.gameObject);
        }

        foreach (ResourceElementArgs resource in resourceList)
        {
            RectTransform resourceElement = Instantiate(resourceListElement, outputContainer.transform.position, outputContainer.transform.rotation, outputContainer.transform);
            resourceElement.gameObject.SetActive(true);
            resourceElement.GetChild(0).GetComponent<Text>().text = resource.resourceName;
            resourceElement.GetChild(1).GetComponent<Text>().text = resource.resourceAmount;
            resourceElement.GetChild(2).GetComponent<Text>().text = resource.resourceOutOf;
        }
    }

    public void UpdateProgress()
    {
        progressPanelElement.gameObject.SetActive(false);
    }

    public void UpdateProgress(string title, float precentage, bool displayTime = false, string timeString = "")
    {
        progressPanelElement.gameObject.SetActive(true);
        progressBarSlider.value = precentage;
        progressBarText.text = title;
        progressBarValueText.text = (precentage*100).ToString() + "%";

        if (displayTime && timeString != "")
        {
            progressTimeElement.gameObject.SetActive(true);
            string[] seperators = new string[] { "{*}" };
            string[] stringArr = Regex.Split(timeString, @"\{.*?\}");
            //TO FIX REGEX POPULATES AN EMPTY FIRST ENTRY INTO THE LST SO ICREAMENTED EVERTHING BY 1
            if (progressTimeDaysText && stringArr.Length > 1) progressTimeDaysText.text = stringArr[2];
            if (progressTimeHoursText && stringArr.Length > 2) progressTimeHoursText.text = stringArr[3];
            if (progressTimeMinutesText && stringArr.Length > 3) progressTimeMinutesText.text = stringArr[4];
        }
        else
        {  
            progressTimeElement.gameObject.SetActive(false);
        }
    }

    IEnumerator ScanTargetCamera()
    {
        CharacterInstanceManager.instance.interactionController.harvestAudio.clip = CharacterInstanceManager.instance.interactionController.scanSound;
        CharacterInstanceManager.instance.interactionController.harvestAudio.Play();
        CharacterInstanceManager.instance.animatorController.anim.SetBool("Scan", true);
        transform.rotation = Quaternion.LookRotation(transform.position - CamController.instance.transform.position, CamController.instance.transform.up);
        transform.position = target.position + (Vector3.up * uiOffset.y) + (transform.right * uiOffset.x);
        scanUI.enabled = true;

        float uiUnitLenght = GetComponent<RectTransform>().rect.width * transform.localScale.x;
        LayerMask layer = ~LayerMask.GetMask("Character", "InteractableEntity");
        while (stopScanFacingCamera == false)
        {
            if(Vector3.Distance(target.transform.position, CharacterInstanceManager.instance.transform.position) > CharacterInstanceManager.instance.interactionController.interactionRange * 2f
                || (Input.GetKey(KeyCode.E) || Input.GetMouseButton(1)) == false || CharacterInstanceManager.instance.buildController.inBuildMode)
            {
                stopScanFacingCamera = true;
                scanFailed = true;
                if(Vector3.Distance(target.transform.position, CharacterInstanceManager.instance.transform.position) > CharacterInstanceManager.instance.interactionController.interactionRange * 2f)
                Debug.Log("Character Moved Away from Camera");
                else

                break;
            }


            if (target == null)
                break;
            transform.rotation = Quaternion.LookRotation(transform.position - CamController.instance.transform.position, CamController.instance.transform.up);
            transform.position = target.position + (Vector3.up * uiOffset.y) + (transform.right * uiOffset.x);
            yield return null;
        }
        stopScanFacingCamera = false;
        scanUI.enabled = false;
        CharacterInstanceManager.instance.interactionController.harvestAudio.Stop();
        target = null;
        CharacterInstanceManager.instance.animatorController.anim.SetBool("Scan", false);
    }

    //IEnumerator ScanCoroutine(float time)
    //{
    //    transform.rotation = Quaternion.LookRotation(transform.position - CamController.instance.transform.position, Vector3.up);
    //    transform.position = target.position + (Vector3.up * uiOffset.y) + (transform.right * uiOffset.x);
    //    float endTime = Time.time + time;
    //    scanUI.enabled = true;
    //
    //    float uiUnitLenght = GetComponent<RectTransform>().rect.width * transform.localScale.x;
    //    LayerMask layer = ~LayerMask.GetMask("Character", "InteractableEntity");
    //
    //    while (Time.time < endTime)
    //    {
    //        if (target == null)
    //            break;
    //        transform.rotation = Quaternion.LookRotation(transform.position - CamController.instance.transform.position, Vector3.up);
    //        transform.position = target.position + (Vector3.up * uiOffset.y) + (transform.right * uiOffset.x);
    //        yield return null;
    //    }
    //    scanUI.enabled = false;
    //    target = null;
    //}

    IEnumerator scanTextElementSolver(float time)
    {
        isScanning = true;
        scanFailed = false;
        scanStatusPanel.gameObject.SetActive(true);
        ScanStatusText.text = "Scan Started";

        yield return new WaitForEndOfFrame();
        scanParticles.Play();
        scanSmoke.Play();
        List<Text> activeText = new List<Text>(scanInfoPanel.GetComponentsInChildren<Text>());
        List<string> textStrings = new List<string>();
        int charTally = 0;
        //Debug.Log("START WITH: text: " + activeText.Count + "      strngs: " + textStrings.Length);
        for (int i = 0; i < activeText.Count; i++) {
            if (activeText[i].text != "")
            {
                charTally += activeText[i].text.Length;
                textStrings.Add(activeText[i].text);
                activeText[i].text = RandomString(activeText[i].text.Length, activeText[i].text);
            }
            else
            {
                activeText.RemoveAt(i);
                i--;
            }
        }
        float finishTime = Time.time + time;

        int stringIndex = 0;
        int charIndex = 0;
        int currentSolved = 0;
        float targetSolved = 0;
        float solvePerSecond = Mathf.Floor(charTally / time);

        while (stringIndex < activeText.Count)
        {

            if (scanFailed)
                break;
            targetSolved += solvePerSecond * Time.deltaTime;
            while(currentSolved < targetSolved)
            {
                if (charIndex > textStrings[stringIndex].Length - 1)
                {
                    stringIndex++;
                    charIndex = 0;
                    if(stringIndex > activeText.Count-1)
                    {
                        break;
                    }
                }

                string newString = textStrings[stringIndex].Substring(0, charIndex + 1) + activeText[stringIndex].text.Substring(charIndex + 1);
                activeText[stringIndex].text = newString;

                charIndex++;
                currentSolved++;
                
            }

            ScanStatusText.text = "Scan " + Mathf.Floor(Mathf.InverseLerp(finishTime - time, finishTime, Time.time) * 100).ToString() + "% Complete";
            yield return null;

        }

        for(int i = 0; i < textStrings.Count; i++)
        {
            activeText[i].text = textStrings[i];
        }
        scanParticles.Stop();
        scanSmoke.Stop();
        if (onScanComplete != null)
        {
            onScanComplete(!scanFailed);
            onScanComplete = null;
        }
        if (scanFailed)
        {
            ScanStatusText.text = "Scan Failed";
        }
        else
        {
            ScanStatusText.text = "Scan Completed";
            yield return new WaitForSeconds(scanCompleteDisplayTime);
        }

        scanStatusPanel.gameObject.SetActive(false);
        scanFailed = false;
        isScanning = false;
        //Debug.Log("FINISHED - Expected: " + finishTime + "   Actual: " + Time.time);
    }

    IEnumerator coolDown()
    {
        CanScan = false;
        yield return new WaitForSeconds(1);
        CanScan = true;
    }




    string RandomString(int Lenght, string sample = "")
    {
        string returnStr = "";
        for(int i = 0; i < Lenght; i++)
        {
            if (sample != "" && i < sample.Length && sample[i] == ' ')
            {
                returnStr += " ";
            }
            else
            {
                returnStr += hiddenRandomCharacters[Random.Range(0, hiddenRandomCharacters.Length - 1)];
            }
        }

        return returnStr;
    }
}
