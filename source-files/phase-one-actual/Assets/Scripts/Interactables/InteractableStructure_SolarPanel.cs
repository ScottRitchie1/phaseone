﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(PowerComponent))]
public class InteractableStructure_SolarPanel : InteractableEntityBase
{

    public string displayName;
    PowerComponent pc;
    public Transform solarPanelPivot;
    public float angle;
    float maxPowerOutput;
    private void Start()
    {
        pc = GetComponent<PowerComponent>();
        maxPowerOutput = pc.powerOutputPerDay;
    }

    private void Update()
    {
        if(DayCycleManager.instance.isNight() == false && (pc.isOn))
        {
            Vector3 lookRot = transform.InverseTransformDirection(-DayCycleManager.instance.rotater.transform.forward);
            solarPanelPivot.localEulerAngles = Mathf.Clamp(Mathf.Atan2(lookRot.y, -lookRot.z) * Mathf.Rad2Deg, 45.0f, 135.0f) * Vector3.right;
            pc.powerOutputPerDay = maxPowerOutput * Vector3.Dot(DayCycleManager.instance.rotater.forward, solarPanelPivot.forward);
        }
        else
        {
            pc.powerOutputPerDay = 0;
        }
    }

    public override void OnScan()
    {
        if (ScanUI.instance)
        {
            UnderConstructionComponent underConstruction = GetComponent<UnderConstructionComponent>();
            
            ScanUI.instance.UpdateDisplayNameTitle(displayName + (underConstruction && underConstruction.buildCompletionPercentage < 1 ? " --Under Construction--" : ""));

            ScanUI.instance.UpdateConnectionStatus(true, (pc.ConnectedToPowerLinker != null), pc.isPowered);
            ScanUI.instance.UpdateErrorMessage();
            ScanUI.instance.UpdateIcon();
            ScanUI.ResourceElementArgs[] resourceList = new ScanUI.ResourceElementArgs[1];
            ScanUI.ResourceElementArgs powerResource = new ScanUI.ResourceElementArgs();
            powerResource.resourceName = "Power";
            powerResource.resourceAmount = pc.powerOutputPerDay.ToString();
            powerResource.resourceOutOf = " / Day";
            resourceList[0] = powerResource;
            ScanUI.instance.UpdateOutput(resourceList);
            ScanUI.instance.UpdateResourceDisplay();
            if (underConstruction && underConstruction.buildCompletionPercentage < 1)
            {
                ScanUI.instance.UpdateProgress("Build Completed", underConstruction.buildCompletionPercentage, true, WorldTimeManager.TimeString(underConstruction.finishTime - WorldTimeManager.currentTime));
            }
            else
            {
                ScanUI.instance.UpdateProgress();
            }

            ScanUI.instance.ScanObject(transform, false);
        }
        else
        {
            Debug.LogError("No scanning ui in scene");
        }
    }

    public override bool UseDefaultHarvest()
    {
        return false;
    }
}
