﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableResourceContainer : InteractableEntityBase
{
    public string containerName = "Unknown Resource Container";
    public List<_resourceDropQuantity> containerDrops;
    bool requiresScan = true;
    public MeshFilter scanMesh;
	// Use this for initialization
	void Start () {
        for (int i = 0; i < containerDrops.Count; i++)
        { 
            containerDrops[i].CalculateDropAmount();
        }
    }

    public override void OnScan()
    {
        if (ScanUI.instance)
        {
            ScanUI.instance.UpdateDisplayNameTitle(containerName);
            ScanUI.instance.onScanComplete = OnScanComplete;
            ScanUI.instance.UpdateConnectionStatus();
            ScanUI.instance.UpdateErrorMessage();
            ScanUI.instance.UpdateIcon();
            ScanUI.instance.UpdateOutput();
            ScanUI.instance.UpdateProgress();
            ScanUI.instance.UpdateScanEffect(scanMesh);
            ScanUI.ResourceElementArgs[] resources = new ScanUI.ResourceElementArgs[containerDrops.Count];
            for (int i = 0; i < resources.Length; i++)
            {
                ScanUI.ResourceElementArgs newResource = new ScanUI.ResourceElementArgs();
                newResource.resourceName = containerDrops[i].resource.itemName;
                newResource.resourceAmount = containerDrops[i].dropAmount.ToString();
                resources[i] = newResource;
            }

            ScanUI.instance.UpdateResourceDisplay("Contents", resources);
            ScanUI.instance.ScanObject(transform, requiresScan, 5);

        }
        else
        {
            Debug.LogError("No scanning ui in scene");
        }
    }

    public override void OnScanComplete(bool successful)
    {
        if (successful)
            requiresScan = false;
    }

    public override void OnHarvest()
    {
        for (int i = 0; i < containerDrops.Count; i++)
        {
            ShipInstanceManager.instance.shipResourceContainer.AddResources(containerDrops[i].resource, containerDrops[i].dropAmount);
        }
        Destroy(gameObject);
    }
}
