﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonHarvestableEntity : InteractableEntityBase {

    public string displayName = "Unknown";
    public string cantHarvestReason = "Requires appropriate tools.";
    public MeshFilter scanMesh;
    bool requiresScan = true;
    // Use this for initialization
    public override void OnScan()
    {
        if (ScanUI.instance)
        {
            ScanUI.instance.onScanComplete = OnScanComplete;
            ScanUI.instance.UpdateIcon();
            ScanUI.instance.UpdateDisplayNameTitle(displayName);
            ScanUI.instance.UpdateErrorMessage(cantHarvestReason);
            ScanUI.instance.UpdateConnectionStatus();
            ScanUI.instance.UpdateOutput();
            ScanUI.instance.UpdateResourceDisplay();
            ScanUI.instance.UpdateScanEffect(scanMesh);
            EvolutionEntityBase evolutionEntity = GetComponent<EvolutionEntityBase>();
            if (evolutionEntity)
            {
                ScanUI.instance.UpdateProgress("Growth Cycle Complete", evolutionEntity.currentLifeSpan);
            }
            else
            {
                ScanUI.instance.UpdateProgress();
            }
            ScanUI.instance.ScanObject(transform, requiresScan, 2);

        }
    }

    public override void OnScanComplete(bool successful)
    {
        if (successful)
        {
            Debug.Log("COMPLETED");
            requiresScan = false;
        }
    }
}
