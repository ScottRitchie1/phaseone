﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animation))]
public class EvolutionEntityBase : MonoBehaviour {

    Animation anim;
    AnimationState animState;
    public AnimationClip evolutionAnimation;
    [Range(0,1)]
    public float currentLifeSpan;

    [Range(0, 1)]
    public float growthCap = 1;

    public double birtTime;
    public double lifeSpan;
    double deathTime;

    private void Awake()
    {
#if UNITY_EDITOR
        if (evolutionAnimation.wrapMode != WrapMode.Default)
        {
            Debug.LogError(evolutionAnimation.name + " isnt set to default warpmode. This will cause enity to dissapear when its fully evolved.");
            evolutionAnimation.wrapMode = WrapMode.Default;
        }
#endif
        anim = GetComponent<Animation>();
        anim.AddClip(evolutionAnimation, evolutionAnimation.name);
        anim.Play(evolutionAnimation.name);
    }
    // Use this for initialization
    void Start () {
		if(birtTime == 0)
        {
            birtTime = WorldTimeManager.currentTime;
        }

        deathTime = birtTime + lifeSpan;
        anim[evolutionAnimation.name].speed = 0;
        animState = anim[evolutionAnimation.name];
    }
	
	// Update is called once per frame
	void Update () {
        currentLifeSpan = WorldTimeManager.TimeInverseLerp(birtTime, deathTime);
        animState.normalizedTime =  Mathf.Clamp(currentLifeSpan, 0, growthCap);
    }

    public void OffsetBirthTime(double offset)
    {
        birtTime -= offset;
        deathTime -= offset;
    }
}
