﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PowerLinkerComponent))]
public class InteractableStructure_Linker : InteractableEntityBase {

    public string displayName;
    PowerLinkerComponent lc;
    private void Start()
    {
        lc = GetComponent<PowerLinkerComponent>();
    }

    public override void OnScan()
    {
        if (ScanUI.instance)
        {
            UnderConstructionComponent underConstruction = GetComponent<UnderConstructionComponent>();
            ScanUI.instance.UpdateDisplayNameTitle(displayName + (underConstruction && underConstruction.buildCompletionPercentage < 1 ? " --Under Construction--" : ""));
            bool foundPowerPlant = false;
            List<PowerLinkerComponent> searched = new List<PowerLinkerComponent>();
            foreach(PowerLinkerComponent l in lc.DBS_All(ref searched))
            {
                if (l.GetComponent<PowerPlantComponent>())
                {
                    foundPowerPlant = true;
                    break;
                }
            }
            ScanUI.instance.UpdateConnectionStatus(true, (lc.connectedPowerLinkers.Count > 0), foundPowerPlant);
            ScanUI.instance.UpdateErrorMessage();
            ScanUI.instance.UpdateIcon();
            ScanUI.instance.UpdateOutput();
            ScanUI.instance.UpdateResourceDisplay();
            if (underConstruction && underConstruction.buildCompletionPercentage < 1)
            {
                ScanUI.instance.UpdateProgress("Build Completed", underConstruction.buildCompletionPercentage, true, WorldTimeManager.TimeString(underConstruction.finishTime - WorldTimeManager.currentTime));
                    }
            else
            {
                ScanUI.instance.UpdateProgress();
            }
            ScanUI.instance.ScanObject(transform, false);

        }
        else
        {
            Debug.LogError("No scanning ui in scene");
        }
    }

    public override bool UseDefaultHarvest()
    {
        return false;
    }
}
