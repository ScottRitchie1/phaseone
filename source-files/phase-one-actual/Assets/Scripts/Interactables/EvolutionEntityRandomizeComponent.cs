﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EvolutionEntityBase))]
public class EvolutionEntityRandomizeComponent : MonoBehaviour {
    EvolutionEntityBase entityBase;
    [Header("LifeSpan")]
    public WorldTimeManager._time rangeTimeType;
    public Vector2 lifeSpanRange;

    [Header("BirthTime")]
    public WorldTimeManager._time birthTimeType;
    public Vector2 birthTimeRange;


    [Header("GrowthCap")]
    public Vector2 growthCapRange;
    private void Awake()
    {
#if UNITY_EDITOR
        if(lifeSpanRange.x > lifeSpanRange.y)
        {
            Debug.LogError(gameObject.name + " has an invalid random life span.");
            Destroy(this);
        }

        if (birthTimeRange.x > birthTimeRange.y)
        {
            Debug.LogError(gameObject.name + " has an invalid random birth time.");
            Destroy(this);
        }

        if(growthCapRange == Vector2.zero)
        {
            Debug.LogWarning(gameObject.name + " has an invalid growth cap, defaulting to 1 but this wont work in a build.");
            growthCapRange = new Vector2(1, 1);
        }
#endif
        entityBase = GetComponent<EvolutionEntityBase>();
        entityBase.lifeSpan = WorldTimeManager.ConvertTime(rangeTimeType, WorldTimeManager._time.seconds, Random.Range(lifeSpanRange.x, lifeSpanRange.y));
        entityBase.birtTime = WorldTimeManager.ConvertTime(birthTimeType, WorldTimeManager._time.seconds, Random.Range(birthTimeRange.x, birthTimeRange.y));
        entityBase.growthCap = Random.Range(growthCapRange.x, growthCapRange.y) / 100;
        Destroy(this);
    }

}
