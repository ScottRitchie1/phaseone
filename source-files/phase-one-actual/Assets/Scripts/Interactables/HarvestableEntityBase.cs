﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EResourceTypes { organics, h20, compound, energy, mineral};
[System.Serializable]
public class _resourceDropQuantity
{
    public ResourceItem resource;
    public Vector2Int randomDropAmountRange;
    public int dropAmount;

    public void CalculateDropAmount()
    {
        dropAmount = Random.Range(randomDropAmountRange.x, randomDropAmountRange.y);
    }
}
[RequireComponent(typeof(EvolutionEntityBase))]
public class HarvestableEntityBase : InteractableEntityBase {
    
    [System.Serializable]
    public class _harvestableStage
    {
        public string stageName;
        public bool useDefaultHarvestEffect = true;
        public float stageLifeCyclePercentage;
        [Header("When Harvested")]
        public List<_resourceDropQuantity> stageDrops;
        [Header("Actions Harvested")]
        public bool DestroyOnHarvest = false;
        public int SetStageWhenHarvested = -1;
        [HideInInspector]
        public bool requiresScan = true;
        public MeshFilter mesh;
    }

    EvolutionEntityBase eEntity;

    public List<_harvestableStage> entityStages = new List<_harvestableStage>();
    // Use this for initialization
    private void Awake()
    {
        eEntity = GetComponent<EvolutionEntityBase>();
#if UNITY_EDITOR
        if (!eEntity)
        {
            Debug.Log(gameObject.name + " Harvestable doesnt have an evolution component.");
            this.enabled = false;
        }

        foreach(_harvestableStage stage in entityStages)
        {
            foreach (_resourceDropQuantity drops in stage.stageDrops)
            {
                if(drops.resource == null)
                {
                    Debug.LogError(gameObject.name + "'s " + stage.stageName + " stage's drop amount isnt set up");
                    this.enabled = false;
                }
            }
        }
#endif
    }

    public override bool UseDefaultHarvest()
    {
        return GetStage(eEntity.currentLifeSpan).useDefaultHarvestEffect;
    }
    private void Start()
    {
        for(int i = 0; i < entityStages.Count; i++)
        {
            for(int j = 0; j < entityStages[i].stageDrops.Count; j++)
            {
                entityStages[i].stageDrops[j].CalculateDropAmount();
            }
            
        }
    }

    public override void OnScan()
    {
        if (ScanUI.instance)
        {
            _harvestableStage curStage = GetStage(eEntity.currentLifeSpan);
            ScanUI.instance.UpdateScanEffect(curStage.mesh);
            ScanUI.instance.onScanComplete = OnScanComplete;
            ScanUI.instance.UpdateConnectionStatus();
            ScanUI.instance.UpdateDisplayNameTitle(curStage.stageName);
            ScanUI.instance.UpdateErrorMessage();
            ScanUI.instance.UpdateIcon();
            ScanUI.instance.UpdateOutput();
            ScanUI.ResourceElementArgs[] resources = new ScanUI.ResourceElementArgs[curStage.stageDrops.Count];
            for(int i = 0; i < curStage.stageDrops.Count;i++)
            {
                ScanUI.ResourceElementArgs newResource = new ScanUI.ResourceElementArgs();
                newResource.resourceName = curStage.stageDrops[i].resource.itemName;
                newResource.resourceAmount = curStage.stageDrops[i].dropAmount.ToString();
                resources[i] = newResource;
            }
            ScanUI.instance.UpdateResourceDisplay("Resources avaliable", resources);
            ScanUI.instance.UpdateProgress("Growth Cycle Complete", eEntity.currentLifeSpan);
            ScanUI.instance.ScanObject(transform, curStage.requiresScan, 5);

        }
    }

    public override void OnScanComplete(bool successful)
    {
        if (successful)
        {
            GetStage(eEntity.currentLifeSpan).requiresScan = false;
        }
    }

    public override void OnHarvest()
    {
        _harvestableStage stage = GetStage(eEntity.currentLifeSpan);
        Debug.Log("Harvested: " + gameObject.name + " at stage: " + stage.stageName);

        for (int i = 0; i < stage.stageDrops.Count; i++)
        {
            ShipInstanceManager.instance.shipResourceContainer.AddResources(stage.stageDrops[i].resource, stage.stageDrops[i].dropAmount);
        }

        if (stage.DestroyOnHarvest)
        {
            Destroy(gameObject);
        }

        if(stage.SetStageWhenHarvested >= 0 && stage.SetStageWhenHarvested < entityStages.Count)
        {
            eEntity.OffsetBirthTime((eEntity.currentLifeSpan - (eEntity.lifeSpan * (1 - entityStages[stage.SetStageWhenHarvested].stageLifeCyclePercentage))));
        }
    }

    public virtual _harvestableStage GetStage(float lifePercentage = -1)
    {
        if (entityStages.Count > 0) {
            if (lifePercentage >= 0 && lifePercentage <= 1)
            {
                return stageAtPoint(lifePercentage);
            }
            else
            {
                return entityStages[0];
            }
        }
        Debug.LogError(gameObject.name + " doesnt have any evolution stages.");
        return null;
    }


    _harvestableStage stageAtPoint(float t)
    {
        if (entityStages.Count > 0)
        {

            int stage = 0;
            for (int i = 1; i < entityStages.Count; i++)
            {
                if (t > entityStages[i].stageLifeCyclePercentage)
                {
                    stage = i;
                }
            }

            return entityStages[stage];
        }
        Debug.LogError(gameObject.name + " doesnt have any evolution stages.");
        return null;
    }
}
