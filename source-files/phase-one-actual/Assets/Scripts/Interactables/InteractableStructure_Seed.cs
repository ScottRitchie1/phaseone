﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(PowerComponent))]
public class InteractableStructure_Seed : InteractableEntityBase
{

    public string displayName;
    PowerComponent pc;
    private void Start()
    {
        pc = GetComponent<PowerComponent>();
        if (TutorialManager.instance)
        {
            TutorialManager.instance.seed = this;
        }
    }

    public override void OnScan()
    {
        if (ScanUI.instance)
        {
            UnderConstructionComponent underConstruction = GetComponent<UnderConstructionComponent>();
            
            ScanUI.instance.UpdateDisplayNameTitle(displayName + (underConstruction && underConstruction.buildCompletionPercentage < 1 ? " --Under Construction--" : ""));

            ScanUI.instance.UpdateConnectionStatus(true, (pc.ConnectedToPowerLinker != null), pc.isPowered);
            ScanUI.instance.UpdateErrorMessage();
            ScanUI.instance.UpdateIcon();

            ScanUI.instance.UpdateOutput();
            if (underConstruction && underConstruction.buildCompletionPercentage < 1)
            {
                ScanUI.instance.UpdateProgress("Build Completed", underConstruction.buildCompletionPercentage, true, WorldTimeManager.TimeString(underConstruction.finishTime - WorldTimeManager.currentTime));
                ScanUI.ResourceElementArgs[] resourceList = new ScanUI.ResourceElementArgs[1];
                ScanUI.ResourceElementArgs powerResource = new ScanUI.ResourceElementArgs();
                powerResource.resourceName = "Power";
                powerResource.resourceAmount = pc.powerConsumptionPerDay.ToString();
                powerResource.resourceOutOf = " / Day";
                resourceList[0] = powerResource;
                ScanUI.instance.UpdateResourceDisplay("Operational Costs", resourceList);

            }
            else
            {
                ScanUI.instance.UpdateProgress();
                ScanUI.instance.UpdateResourceDisplay();

            }

            ScanUI.instance.ScanObject(transform, false);
        }
        else
        {
            Debug.LogError("No scanning ui in scene");
        }
    }

    public override bool UseDefaultHarvest()
    {
        return false;
    }
}
