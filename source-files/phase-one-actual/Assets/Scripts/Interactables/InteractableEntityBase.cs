﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class InteractableEntityBase : MonoBehaviour {
    private void Awake()
    {
#if UNITY_EDITOR
        if (gameObject.layer != LayerMask.NameToLayer("InteractableEntity"))
            Debug.LogError(gameObject.name + " is an entity but doesnt have the 'InteractableEntity' Layer Set.");
#endif
    }

    public virtual void OnScan()
    {
        Debug.Log(gameObject.name + " was scanned.");
    }

    public virtual void OnScanComplete(bool successful)
    {
        Debug.Log(gameObject.name + "'s scan was successful: " + successful);
    }

    public virtual void OnHarvest()
    {
        Debug.Log(gameObject.name + " was harvested.");
    }

    public virtual bool UseDefaultHarvest()
    {
        return true;
    } 
}
