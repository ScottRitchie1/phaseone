commit 0a3fa9f5cb0b43551333be2f9b5959f92b7366dd
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 28 01:34:57 2018 +1100

    harvestables reset stage on harvest

commit 11990968a2d334821a7e54f517db96f87d9b330e
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Nov 27 20:47:35 2018 +1100

    Fixed issue with character teleporting to the ground when landing right on an edge. Potential fix for character getting stuck in airState while grounded.

commit 6733c3d1e4b5e945b49b06a47e4f0a674f4e3d32
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Nov 27 03:32:27 2018 +1100

    Scan Effect

commit 6bb70a718fb199aa5c50737006fa0a932f9d39fb
Merge: 01383b98 4cd26d38
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 23 15:25:58 2018 +1100

    Merge branch 'master' of https://gitlab.com/ScottRitchie1/phaseone

commit 01383b98effc360a59d5b7676700d7487ef2711b
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 23 15:25:42 2018 +1100

    dunno

commit 4cd26d38db2321c5a63ee2b8976fbb5f74321efa
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Nov 23 14:26:22 2018 +1100

    level additions and terrain changes

commit 544b07303ffc673f7296d345f50b58ac9b606af4
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 23 12:40:36 2018 +1100

    landing scene 2

commit 5b66e27e59ba3361b4d7af16cb86055ba128a31f
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 23 12:19:21 2018 +1100

    Ship Land Scene

commit 2f4c175bc3015d58f097b5b129f489056381b9cb
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 23 10:50:28 2018 +1100

    Faked Scan fix

commit 127ddbf7a062cf172b9178aeda0209a809a99c81
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 23 10:45:16 2018 +1100

    Added Faked Scan Rock and plant

commit 9a6203feafcf786e647b799fe5f5ee0c7b6c46ee
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 23 10:03:22 2018 +1100

    Harvest Scene Prefab

commit 3613fee2326879c2954ff289f4db954808f18fc6
Author: Luke Boon <luke.boon@hotmail.com>
Date:   Fri Nov 23 07:24:38 2018 +1100

    changes made to scene and environment for trailer shots

commit 08f6b5adedd66b01c9729029482b74c04c44d44b
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 20:28:25 2018 +1100

    Updated and Optimised Ship, Updated Seed prefab

commit 2027b72a86e9f63287afd4427fad659400fb9530
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 16:58:26 2018 +1100

    stuff

commit 18b0043dabc7a914c9e861739907f3517275d2c5
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 16:16:50 2018 +1100

    planet, ship textures, grasses and updated terrain

commit bf51493e74aa73cfd5b06a78c01d7d0578526065
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 15:54:07 2018 +1100

    scene

commit a9927413363801e0896ce3c9cd1d10c4d4390e07
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 15:52:04 2018 +1100

    scens

commit ec7321bbe7f23707af92d3a617a13f2b0e9a25ca
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Nov 22 15:22:40 2018 +1100

    Water

commit ca4eb29f5f3c8de58afad701340c15c3a42dfe01
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Nov 22 15:16:04 2018 +1100

    Configures Seed prefab and blueprint

commit 962a813892b8ca7474f75f2cf41a50ae2e0bfeae
Author: Twinkle Star Taylor <twinklestartaylor@gmail.com>
Date:   Thu Nov 22 14:37:54 2018 +1100

    seed blueprint

commit 3d32e83e3a64fd7e5e45d3b22637484a8e9d9929
Author: Twinkle Star Taylor <twinklestartaylor@gmail.com>
Date:   Thu Nov 22 13:42:51 2018 +1100

    Tuna Mushroom and new scene

commit c202e76d2d56dcb02ed106b911c36e90c37124c8
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 13:37:39 2018 +1100

    Updated Curve sTrees & Crystal. Added Fern

commit a701f37a17e8d5d2df5b781cc7c71a94eb0cb10e
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 13:22:35 2018 +1100

    Fixed FireFlys

commit 1e8fedaab5c0d9a92fb3475ba7c037dffdfe3b5f
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 13:17:02 2018 +1100

    Updated Drooper Tree, Added Harvestable Crystal

commit 2ff876289f1193199372c484391412fbae58421a
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Nov 22 12:28:06 2018 +1100

    new crystal models, textures, maps added - needs prefab made
    also added new fern asset, needs prefab added

commit d15759e16289b7f1cc591031300a2adbd22ca0bf
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 12:03:14 2018 +1100

    Change to mushrooms

commit 1a2e78ee6d8444ee8c6ca60ef7ae9b7550096083
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 05:00:39 2018 +1100

    Added Scan UI Text Solver feature, Hard coded solver & scan speeds for now. gg im going to bed

commit a2c583e23b30f71fdcc96cb47b676098c4bb45bf
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 22 03:39:01 2018 +1100

    Added Animation Curves to character animations

commit 8913430cf03ae6433a56f08700994f1622be83ab
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 21 18:59:42 2018 +1100

    Added NEW CHARACTER PREFAB with new charactcer moel and textures, animation curves yet to be applied

commit eb577fa4ebb0b0b3428b8494fc1b5c7bfbb2f48f
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 21 15:39:25 2018 +1100

    Converted Character to fully use single asset import prior to updaing to new rig/model - Commited for backup

commit 734b5df5535dd0774c6bed51ec1cff3e80301072
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 21 15:33:49 2018 +1100

    Updated Scanui positioning and fixed clipping

commit 23b3e0182bfc0b89b96c9e9cbb1b97462593a47d
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 21 01:57:55 2018 +1100

    Continued Working on Scanning system.

commit 6c11a0691c714f46cd6f15ca11ac9f395c98fa0f
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Nov 20 19:24:34 2018 +1100

    Added building UI Functionality, Started on Scan UI Functionality, Fixed issue with underconstruction no build zones not been positioned properly

commit f88227f4fac3228d837a06cd6b3c9cae4e5f6fc5
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Nov 19 22:47:13 2018 +1100

    Created Scan and build UI Elements started on UI Functionality

commit f7505b880cf3e7b4ae291ff07b84f7ea87b0130b
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Nov 19 02:44:27 2018 +1100

    Updated Buildables Effects - Shader, Fixed Character step height Related issues

commit b865f81351c87687be860589963973b243a6f43a
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sun Nov 18 20:46:40 2018 +1100

    Added Jetpack lights, Worked on Character heaed torch light, fixed falling animation transition

commit 0a846e9d78c75b097680298f28d158779af8c207
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 16 15:51:31 2018 +1100

    Imported new ship model, Fixed Ship lighting issue

commit 876357852d7a5205f6557ee26c2463ad7634f2ca
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 16 15:38:23 2018 +1100

    Changed character sounds

commit b2c7952442f54975e551b795804ff38b5e05100a
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 16 15:34:01 2018 +1100

    Added Fire Fly Effect, Fixed Ship Glass, Fixed Debug Time Scaling

commit 52433ab553620ee4b2f883135ad8712c57da35aa
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Nov 16 15:15:15 2018 +1100

    models and normal maps for harvestable crystal, ground textures and scene and terrain updates

commit a6f6bd6a4d241be3b89926f858dab8a29b5d6474
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 16 14:30:03 2018 +1100

    Added Mushroom emmision Script

commit 95d6845bb39e6c026139f2769a79fec323d4e45d
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Nov 16 14:19:48 2018 +1100

    files and prefab for new monolith

commit 15497d5c625c2ee79fb153cd91b502eb8f363de7
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Nov 16 13:54:53 2018 +1100

    updated sun, big, med, small plants prefabs. Added grasses, updated terrain

commit 94b424ec9ef05cb98e3f15eaacc28961a9bbfc15
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Nov 16 11:53:17 2018 +1100

    xelha prefab update

commit 9dd1eb9b0ee6ae630a5a7833789b4de52ee68b31
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Nov 16 11:21:06 2018 +1100

    Droopy tree + prefab

commit 323d14efe125bf378e878ea4d44f459ad52d6069
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 16 11:14:18 2018 +1100

    Updated Big Plant - Added Small plant

commit d1dd62ff73476c14df6ca6cf7aadb09478d2867f
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Nov 16 10:59:51 2018 +1100

    bent mushroom prefab + ground textures

commit b86f68b78a516f47850202a51a22bc839d391869
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Nov 16 09:59:17 2018 +1100

    updated level, xelha prefab

commit 878da84aef03d39e3f1b278270ef14e01d5bbcb3
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 16 09:54:08 2018 +1100

    Added Large Plant

commit fad41b13b90bec4ad6281ef6f276cf48dc541229
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Nov 15 15:50:59 2018 +1100

    fIXED CLIPPING PLANE

commit f8679a1734a4f32fa57605c0c4f2c57fc5e0cb14
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 15 15:42:00 2018 +1100

    Updated Ship

commit 33da3a4babbca956ed027639eb183ab6c359ca54
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 15 15:01:07 2018 +1100

    Added Mid Plant Prefab

commit 0569e2a7f0d9308b33db59c0758d45ecd8259642
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 15 14:36:33 2018 +1100

    Added Growing large mushroom prefabs

commit 17bb970bf551abf3f05eedba64efaab073363e1a
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 15 13:40:29 2018 +1100

    Created Rock Prefabs

commit cadc1dda5c0d8b9065923aaeec0e8760ca01d656
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Nov 15 13:36:48 2018 +1100

    Updated Sound Effects

commit eccad659c88e9edf03ce69d22cd589b50c9c1b9b
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 15 11:35:09 2018 +1100

    Changed fog to work with Aura not legacy fog

commit 968ba33c27822fffa6501b782168aa9235c3a492
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 15 11:27:10 2018 +1100

    Configured lighting in Final Scene

commit a07fdeeee652b9a28d3199aeb38a7eac8be486b1
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Nov 15 11:04:07 2018 +1100

    Updated Scene ready for prefab harvestables and scene adjustments

commit 8c9b51885bf8a6f699b67010ced2089d188ec6b1
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Nov 15 10:29:38 2018 +1100

    Final Scene - added some finalised art assets

commit efbe6c4808f5d727fd6355805572837fa360c8e7
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 14 19:40:00 2018 +1100

    Fixed Volumetric Lighting

commit 4becb18b2b2f6e7ae7d644daeaaff9cc4050f804
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 14 18:36:14 2018 +1100

    Removed all unessesary files and meta data to fix repository.

commit bb00b8f54ab40d55d6fb5031b15692f49eaf5bef
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 14 18:30:37 2018 +1100

    Fixed .gitIgnoreFile

commit c2824b5467bcb88b3ef5d196b14bb3decd8ddab9
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 14 07:25:33 2018 +0000

    Delete Assembly-CSharp.csproj

commit 5af40caf58586ed0fd5234e7b0b4e743292b4030
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 14 18:05:49 2018 +1100

    Changed .gitignore

commit aa0b7461bb9389307f5c39d8b7722c62aa652f97
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 14 15:49:35 2018 +1100

    Added jetpack effects, Added WIP ship mdoel. Fixed Building issue. Lighting is broken apparently?

commit 8918de537f9ba68ef5ce482a63a6c2929f234bbf
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Nov 13 16:46:48 2018 +1100

    Added WIP Sip models and configured it in the testing scene

commit 8edfdce192b9984f4bc5f40697b9ced4a38b6699
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Nov 13 15:51:49 2018 +1100

    Updated to Unity 2018.2.14f1 - previous was wrong version/ Fixed issue with building

commit 49ce4523b9d8c7ab6870db7af5fbad96ef3f37e6
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Nov 13 01:51:08 2018 +1100

    UPDATED TO UNITY 2018.2.12f1. Fixed terrain lighting issue. Imported Gaia, Aura, Shader Forge & 2.5 particles packs. Reimported Post Processing & ProBuilder.

commit 70a89bed6156f54710a28cb72368658d10a2e17c
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Nov 12 02:40:36 2018 +1100

    Fixed serious issue with time display not been accurate, Fixed issue where ship didnt detech its own power linker. Worked on Respawn coroutine event

commit bf93d1455f90adad2e0ce9b9aca57525115d7fa9
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sun Nov 11 23:57:57 2018 +1100

    Added Initial Pass on FootStep, SuitRubbing, Jetpack sounds. Fixed issue where sometimes character wouldnt jump.

commit 429543d6f0933d6da5cf830524260d3ab7ccc8d2
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sun Nov 11 20:19:04 2018 +1100

    Fixed all powerlinker system to work properly now

commit e6682cb35cf86a26971fc4e60b955be262b9e0fc
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sun Nov 11 03:16:18 2018 +1100

    Started to rewrite Link system, functional but backend isnt working as intended FIX IF HAVE TIME. Altered character animation controller & jetpack animations

commit a6ff4849fb85e134a8522ac5aebd44c11b4e05ce
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 9 21:14:10 2018 +1100

    Fixed building broken in a build. Fixed landing damage, Fixed isue with unconnected power linkers not been able to be reconnected. but broke PowerCords and powerconsuption - will fix tomorrow

commit 3356713b7e35c61ffd94cbc1e925f3cd725533d5
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 9 17:43:21 2018 +1100

    Fixed foot ik issue

commit c5d2572bac48dab68c175e8a0dcb685baf283003
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 9 17:07:22 2018 +1100

    Worked on respawn, added new animations

commit 25d1cdf11fb34df68fe17ea7808cde165f239363
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 8 17:01:33 2018 +1100

    Imported Wip

commit 3c8fb6bff9bfffc4072af953a3b4b15a83013b2e
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Nov 8 15:51:26 2018 +1100

    updated terrain - new level layout with sectioned areas

commit 403c03a7310185f46787dc8d229f752586904884
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 8 14:04:48 2018 +1100

    Fixed Root ik jittering.

commit a083d951f38cbfeadf5aa7f6401113c16af25d8b
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 8 10:14:47 2018 +1100

    Fixed issue where linkers now connect power components that where unconnected

commit fb787d24d8cae15deb46b7f74fee1d497474fef9
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 8 00:58:01 2018 +1100

    Smoothed Root IK positioning, Added Character Collider controller to manage heights and offsets of colliders based off root height. Removed apparently unneeded dynamic friction code from CharacterController - CharController - PhysicsMaterial

commit 810adcfdd5a4cbbb12180934ae895ab5746ff525
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Nov 7 20:56:07 2018 +1100

    Fixed Root Tilting, Adjusted Character Controller values. Initial pass on foot ik & root Ik

commit 5690723c359e22b3eaa7efcb374fd228a812dd6a
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Nov 6 23:37:06 2018 +1100

    Imported and configured Temp animations and character rig/model. Added initial pass on character animation controller & root tilting.

commit c2c961612d9b2a22cfae003097ff22b219751261
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Nov 5 19:59:27 2018 +1100

    Reworked power system to support powerlinkers

commit 6c46f44038687f5415ce762286a9de50183dd28e
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Nov 5 03:16:13 2018 +1100

    Completed Building system - EXCEPT resources depleting over time - currently instantly deplete. Large block of commented wip code will eb revisited if i have time.

commit 4960c80a08f451004009144600fc135143ca3955
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Nov 2 16:57:16 2018 +1100

    Implemented Building system

commit 5de06bb8cbde59a96ffc1dbabd1c64e10f5a4578
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Nov 1 16:58:56 2018 +1100

    Worked on Building system even more

commit f85954a0526c4cb7fcbc1917281e76089599491a
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 31 21:52:41 2018 +1100

    Added build area system

commit 8d2eee9c814f403608df67bbad3b3d2886d50824
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 31 17:00:08 2018 +1100

    Worked on building system

commit 27735403f729bf64ad7fe367f6549d162d59b6e7
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Oct 30 22:04:53 2018 +1100

    Implemented Initial Power System. Added Resource and blueprints system. Added Resource Containers. Updated old systems to work with new resource management. Updated Debug & scan ui to work with new systems. Script folder rearrangement

commit 9f01dace9e984d3adfb27ca860b4dd41fd903baf
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Oct 29 00:44:05 2018 +1100

    Rewrote Majority of Math Functions for the WorldTimeManager and added new functions. Changed Prototype scan to show the amount given not range

commit e95d400b556f0accbbdfe866da878ca6d1d68cda
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 26 16:14:30 2018 +1100

    Created more tree's and grass tests. Added wip ShipPowerPlant Manager

commit feff55c4434a47e6dc7d1234642d3881c952232f
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Oct 26 15:31:51 2018 +1100

    prototype build - new scene, new level built with terrain builder, implemented lastest models and textures, ground textures

commit 1d8ed390f54a30aa72de89cfce66257048460443
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 26 14:10:02 2018 +1100

    Double Sided Standard Shader

commit a9f679c66c21b3961abd9044019e2f0987c6d5df
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 26 12:36:19 2018 +1100

    Added Seperate state for jetpack and airTime, Fixed null obj error on character light script if no world time manager is present.

commit 4182b5fd81875666a50258119d6006de40d17df9
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 26 11:00:15 2018 +1100

    Removed Months from WorldTimeManager

commit b7e06838cdbafa05c693f0ce6ed263f77096a243
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Oct 23 22:03:00 2018 +1100

    Started Working on HarvestableEntities system

commit 30ef18f258a252e1668dd1b537713700b1602c25
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Oct 22 02:01:32 2018 +1100

    Potential Fix for charater max walkable angle. Added variable day light lenghts

commit 9e026ac443d8a4ab18071975e44b8c4c6f030f76
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sun Oct 21 01:40:10 2018 +1100

    Added Initial Pass of Evolution Object. Added couple texture and created temp large mushroom. Imported ProBuilder

commit 6537ba24dec8b9499dd36e19c633eab297431a4d
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 19 17:02:05 2018 +1100

    Minor changes to WorldTime Script

commit c967486cbdfd544a061d011f5a3286d5373d99c6
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Oct 19 16:34:27 2018 +1100

    updated scene: level2

commit 23d2481e0b07c107ed3c79831c949aaa023c5d08
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 19 15:15:35 2018 +1100

    Updated Prototype systems to work fully with the new WorldTime system. Removed old time manager

commit 40dfc28f4c26d8552a36bfe34b99445368f8ddd2
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 19 14:52:34 2018 +1100

    Game starts at day time now

commit 1d55efd423c5ca80a66f27f57120efe182862ddd
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 19 12:17:16 2018 +1100

    Intergrated main branch changes into prototype. Added new character and set up initial pass on plants.

commit 6d8ebda00227eeee56208728eec5bc1fde1e0a5b
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 19 01:44:10 2018 +1100

    Fixed DayCycle to work with realtime system. Implemented first pass on core day time system tracking. Added Debug ui. Removed old debug ui.

commit 6a3bb9a0b0718b670c8324b91c3005bd609c4f40
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Thu Oct 18 17:02:40 2018 +1100

    Worked on WorldTimeManager & reworked DayCycle Manager to work with real time system.

commit 804fef7a5c83cdefdbcfb21d9f0a215ec5ec351e
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Oct 18 15:37:58 2018 +1100

    added all new environment assets and new ship interior+exterior

commit 0a9febd630d6594e045d5778147f4c25254a35f2
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 17 20:14:44 2018 +1100

    Completed Initial Pass on Day Cycle Manager

commit c1a654a208fe58f47753970f5f904e4d5661f7ef
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 17 17:01:12 2018 +1100

    Worked on Day Cycle Manager and trees

commit 2bc32f659c18fa9ae1d59ea6474446266c0fb9a2
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 17 11:53:53 2018 +1100

    Added Build mechanic to prototype

commit d55ca7bca39d58829f241dee4f532173b9567da3
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Tue Oct 16 20:03:40 2018 +1100

    Messed around with more trees

commit 5d66dc098f24a824e5a6784d693a236a7ee5ad0c
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Oct 15 21:02:34 2018 +1100

    Rewrote Interaction Controller. Fixed issues where camera and character grouneded gravity would detect triggers

commit 8833e7f18eeea709fcfc5e35534105888fd07efd
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Mon Oct 15 00:57:00 2018 +1100

    Rewrote Camera Script. Fixed Camera Clipping. Removed Old Camera Script

commit 8f4796f7b7518e4a6b01f51a50010e1488a209a3
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sun Oct 14 19:22:41 2018 +1100

    Fixed Character Gravity Issue. Added more Step and Angular controls when grounded. Char Controller commenting and code cleanup. Removed Old Character Controller as its no longer needed. Messed with trees some more.

commit 3f3f8c86c5704cfd6b2aa4deb2f7a9369f2c992e
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sun Oct 14 01:31:16 2018 +1100

    Added Character Directional Change Damping.

commit 172e8a74a2808c7f572e3262f15a173c7e1ba466
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sat Oct 13 19:51:16 2018 +1100

    Updated Graphics Settings

commit 6c46e116f7d391bb50aba5088867a695c277c13b
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sat Oct 13 19:02:51 2018 +1100

    Added default unity Procedural Tree Tests

commit a6e301989ab07f580cd69e4c1487dd03fe2634af
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Sat Oct 13 19:00:39 2018 +1100

    Added .gitignore for Actual and Prototype Prpjects

commit 3e6364e8eef23256de164c60ef016b5e6322936a
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 12 16:58:41 2018 +1100

    Updated Actual Game Char Controller

commit 020f3a0bc8594b1b53ed646b157e9a2c14826021
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Fri Oct 12 16:05:48 2018 +1100

    Updated and Configured Changes to CharController. Implemented new WIP Ship and WIP Plant.

commit 9fd996ab89682924c0d50f69484fe5b69f2c4cfe
Author: Luke <luke.boon@hotmail.com>
Date:   Fri Oct 12 15:13:27 2018 +1100

    Updated second level, added new assets - plant, ship, rescaled character

commit a6345ebcf60ab5def5522d6db0fafa120ecfabac
Author: Luke <luke.boon@hotmail.com>
Date:   Thu Oct 11 16:05:28 2018 +1100

    added updated version, new levels, prototype

commit de8c9e5f609cfa11b055e6f2bd068d190d486527
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 21:36:10 2018 +1100

    fixed more shit

commit 878919685adea486624a82f3248f696b090bd632
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 21:28:26 2018 +1100

    Fixed the shit i broke

commit 8f41f35992ffe6c703b546ad038d150fa556b2a4
Merge: 41ee3fd5 0b957d11
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 21:13:04 2018 +1100

    Merge

commit 0b957d115fc6cf298f71d6fecbfdca06734d6f5a
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 21:07:40 2018 +1100

    Build Change Test

commit 41ee3fd50efd27746b56aec9157bdd25cec1488d
Merge: 50cfac6a 254b0a6e
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 10:01:35 2018 +0000

    Merge branch 'development' into 'master'
    
    removed other branch files from development branch
    
    See merge request ScottRitchie1/phaseone!2

commit 254b0a6ef4cd0b897eabb32e513c00005581e40c
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 20:58:00 2018 +1100

    removed other branch files from development branch

commit 50cfac6a47233ce55669ba620b1eac26764ca954
Merge: 86f767c2 b9613e98
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 09:50:45 2018 +0000

    Merge branch 'builds' into 'master'
    
    Configured Builds Branch
    
    See merge request ScottRitchie1/phaseone!1

commit b9613e98711b83fd67699c3fd8fedb7872546b46
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 20:42:23 2018 +1100

    Configured Builds Branch

commit 86f767c29a1fe6c3784b02274be9f6fb507134cd
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 19:43:23 2018 +1100

    Revert "Add new file"
    
    This reverts commit 205e1491f868cb9528a4bedfb6ad1e5e40d331b1.

commit 205e1491f868cb9528a4bedfb6ad1e5e40d331b1
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 08:34:45 2018 +0000

    Add new file

commit e945502bbaad472f78ec060c199466da43b57177
Merge: 66cc737d 65ea3c31
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 19:20:51 2018 +1100

    Removed builds from master

commit 65ea3c318a58d4c0590fcc4c4a1ef4038ab59593
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 19:17:27 2018 +1100

    adding test asset to development branch

commit 66cc737de97aac8a53fe80612874d192ff814ac0
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 19:12:16 2018 +1100

    just a test

commit df808f4e47c5520f8484af3c2618b31fdaf58aca
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 08:09:55 2018 +0000

    Update test.txt

commit c3319686891c8753556b059dfde6ddabc4f2bc8a
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 08:05:37 2018 +0000

    Update test.txt

commit dde8ad5d53ad95ddb747837e4db0c4c68156b26b
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 18:53:33 2018 +1100

    yet another test

commit 22a401b4a9fe581c384167e211b9af8b986176b7
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 18:13:44 2018 +1100

    Initial Commit of Actual project + gitignore

commit e5be25733551c7369b8b330f63e7f890279a3a3d
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 18:09:31 2018 +1100

    Renamed folders

commit fd2d4eea2327cfe93dfa326b530ffae2dbc31181
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 18:05:29 2018 +1100

    Initial Push of Prototype + gitignore

commit fcbb72f546a22653b1b9d97f9ca607138369622a
Author: Scott <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 17:31:01 2018 +1100

    another test

commit 2d449c408298850d710cb407615d4423de92afb1
Author: Scott <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 17:29:30 2018 +1100

    Test

commit 3174d90a1e2bc3c97d8f32c1ce92eb2398158730
Author: Scott Ritchie <s.c.ritchie1@gmail.com>
Date:   Wed Oct 10 06:23:02 2018 +0000

    Initial commit
